## Módulo centralizador de andresgotta.com.ar

Gotta center es el sistema encargado de gestionar todos los usuarios y servicios de todas las plataformas asociadas a andresgotta.com.ar, permitiendo así poder tener de manera centralizada la información de los mismos para ser usada por todas y cada una de estas plataformas.

## Requerimientos

Para el correcto funcionamiento del sistema se necesitan satisfacer los siguientes requisitos mínimos

  - php >= 5.6.*
  - Extensió de php OpenSSL
  - Extensión de php PDO 
  - Extensión e php Mbstring
  - Extensiónde php Tokenizer
  - Extensión de php XML
  - Mysql >= 5.6 

## Instalación del proyecto

Luego de clonar el repositorio se debe ubicar en la ra�z del proyecto y seguir los siguientes pasos:

  - composer install
  - creación de archivo .envcon las variables de configuración,en el arvhivo .env_dist encuentras un ejemplo con los nombres de todas las variables
  - php artisan key:generate
  - php artisan storage:link
  - php artisan migrate --seed
  - php artisan passport:install, este comando creará los clientes de OAuth2 los cuales se deben configurar en las apicaciones que hagan uso del centralizador. El tipo de autenticaci�(grant_type) que debe usarse es el password
  - chmod 755 -Rf public
