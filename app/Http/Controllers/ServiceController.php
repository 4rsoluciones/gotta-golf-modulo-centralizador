<?php

namespace App\Http\Controllers;

use App\Service;
use App\Platform;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\servicesPurchase;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Config;
use SantiGraviano\LaravelMercadoPago\MP;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class ServiceController extends Controller
{
    /**
     * MercadoPago SDK handle
     *
     * @var MP
     */
    protected $mp;

    public function __construct()
    {
        //New instance of MercadoPago SDK
        $this->mp = new MP (Config::get('mercadopago.access_token'));
    }

    protected $scope;

    public function index()
    {
        return Response()->json(Service::whereHas('platforms', function ($query) {
            $query->where('platform_id', Request()->get('platform'));
        })
            ->join('areas', 'area_id', '=', 'areas.id')
            ->selectRaw("services.id, CONCAT(areas.name,' - ',services.name) as name")
            ->orderBy('name', 'ASC')
            ->pluck('name', 'id'), 200);
    }

    /**
     * Show all services by area
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function area($id)
    {
        try {
            $services = \App\Area::with('services')->findOrFail($id);

            $services = $services->services;

            $response = (!empty(session('response')) ? session('response') : null);

            $error = (!empty(session('error')) ? session('error') : null);

            session()->forget(['response', 'error']);

            return view('services.list', compact('services', 'response', 'error'));
        } catch (ModelNotFoundException $e) {
            return redirect('/')->with('error', 'Ocurrió un error, por favor intente nuevamente.');
        }
    }

    /**
     * Show Service detail
     *
     * @param $id
     * @param $platform
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id, $platform = null)
    {
        try {
            if (!Request()->headers->get('referer') && !$platform) {
                throw new ModelNotFoundException();
            } else {
                $url = explode('platform/', Request()->headers->get('referer'));

                $platform = count($url) > 1 ?
                    substr($url[1], 0, 1) :
                    Platform::whereName(Config::get('constants.platformInternalNames.'.$platform))->firstOrFail()->id;
            }

            $service = \App\Service::whereVisible(true)
                ->with(['type', 'area', 'users', 'platforms' => function ($query) use ($platform) {
                    $query->where('id', $platform);
                }])
                ->whereHas('platforms', function ($query) use ($platform) {
                    $query->where('id', $platform);
                })
                ->findOrFail($id);

            return view('services.detail', compact('service', 'platform'));
        } catch (ModelNotFoundException $e) {
            return redirect('/')->with('error', 'Ocurrió un error, por favor intente nuevamente.');
        }
    }

    public function pay($id)
    {
        try {
            $service = \App\Service::with('type', 'area', 'platforms', 'users')->findOrFail($id);

            $user = Auth::user();

            $platform = Platform::findOrFail(Request()->get('platform'));

            $userServices = $user->services()->where('platform_id', $platform->id)->pluck('id')->toArray();

            if (in_array($id, $userServices)) {
                return back()->with('error', 'Usted ya possee este servicio activo.');
            }

            $mpCustomer = $this->getMercadoPagoCustomer($user);

            $this->mp->post (
                "/v1/customers/".$mpCustomer['id']."/cards",
                array("token" => Request()->get('token')));

            if (Request()->has('promoCode')) {
                $promoCode = \App\PromoCode::where('code', Request()->get('promoCode'))->first();

                $discount = (!empty($promoCode) ? $promoCode->discount : 0);
            }

            $subscription = $this->mp->post(
                '/v1/subscriptions',
                [
                    'plan_id' => $service->mp_plan_id,
                    'payer' => [
                        'id' => $mpCustomer['id'],
                    ],
                    'description' => $service->platforms()->first()->name.' - '.$service->name.' - '. $user->email,
                ]
            );

            if ($subscription['status'] == 201) {

                $data = [
                    'order_status_id' => \App\OrderStatus::where(
                        'name',
                        $this->getOrderStatusName($subscription['response']['status'])
                    )->first()->id,
                    'pay_cash' => false
                ];

                $order = \App\Order::create($data);

                $data = [
                    'amount' => $service->price,
                    'payment_type_id' => \App\PaymentType::where(
                        'name',
                        $this->getPaymentTypeName('credit_card')
                    )->first()->id,
                    'order_id' => $order->id,
                    'mp_payment_id' => null,
                ];

                \App\Payment::create($data);

                $order->services()->attach($id);

                if (count($userServices)) {
                    $this->checkForUpdateOrDowngrade(
                        $user->services()->where('platform_id', $platform->id)->get(),
                        $service,
                        $platform->name
                    );
                }

                $servicePlatforms = $service->platforms()->get(['name'])->toArray();

                $names = [];

                foreach ($servicePlatforms as $item) {
                    $names[] = $item['name'];
                }

                if (in_array(Config::get('constants.platformNames.SPORT_IN_TOUCH_NAME'), $names)) {
                    $userRoles = unserialize($user->profile->roles);

                    if (!in_array(Config::get('constants.sportInTouchRolesNames.ACADEMY'), $userRoles)) {
                        $userRoles[] = 'ROLE_ACADEMY';

                        $profile = $user->profile;

                        $profile->update(['roles' => serialize($userRoles)]);

                        $profile->save();
                    }
                }

                $user->services()->attach(
                    $id,
                    [
                        'active' => true,
                        'expired_at' => Carbon::now()->addDays(30)->toDateString(),
                        'mp_subscription_id' => $subscription['response']['id'],
                        'platform_id' => $platform->id,
                    ]
                );

                // Pertenece a SportInTouch
                $linkAcademia = false;
                if (strstr($order->services[0]->name, 'Sports InTouch')) {
                    $linkAcademia = true;
                }

                //Send order details email to user
                Mail::to($user->email)->send(new servicesPurchase($order, $user, $linkAcademia));

                $response = [
                    'status' => Config::get('mercadopago.MP_PAYMENT_APPROVED'),
                    'message' => 'El pago fue procesado satisfactoriamente. Gracias por su compra!'
                ];
            }

            if ($platform->name == Config::get('constants.platformNames.PUBLICIST_NAME')) {
                return redirect(str_replace(URL::to('/'), '', URL::previous()))->with(compact('response'));
            } else {
                $platformUrlName = $this->getPlatformUrlName($platform->name);

                return redirect('/services/'.$service->id.'/'.$platformUrlName)->with(['response' => $response]);
            }

        } catch (ModelNotFoundException $e) {
            return redirect('/')->with('error', 'Ocurrió un error, por favor intente nuevamente.');
        }
    }

    public function unsubscribe($id)
    {
        try {
            $service = \App\Service::findOrFail($id);

            $service->users()->updateExistingPivot(
                Auth::user()->id,
                [
                    'active' => false,
                    'expired_at' => null,
                ]
            );

            $this->mp->put(
                '/v1/subscriptions/'.$service->users()->where('id', Auth::user()->id)->first()->pivot->mp_subscription_id,
                ['status' => 'paused']
            );

            return redirect('/profile/edit/'.Auth::user()->id)->with('success', 'Registro editado satisfactoriamente');

        } catch (ModelNotFoundException $e) {
            return redirect('/profile/edit/'.Auth::user()->id)
                ->with(['error' => 'Registro no encontrado.']);
        }
    }

    public function resubscribe($id)
    {
        try {
            $service = \App\Service::findOrFail($id);

            $service->users()->updateExistingPivot(
                Auth::user()->id,
                [
                    'active' => true,
                    'expired_at' => Carbon::now()->addDays(30)->toDateString(),
                ]
            );

            $this->mp->put(
                '/v1/subscriptions/'.$service->users()->where('id', Auth::user()->id)->first()->pivot->mp_subscription_id,
                ['status' => 'authorized']
            );

            return redirect('/profile/edit/'.Auth::user()->id)->with('success', 'Registro editado satisfactoriamente');

        } catch (ModelNotFoundException $e) {
            return redirect('/profile/edit/'.Auth::user()->id)
                ->with(['error' => 'Registro no encontrado.']);
        }
    }

    public function listServices(Request $request, $email)
    {
        $user = \App\User::where(['email' => $email])->orWhere(['username' => $email])->first();

        if (empty($user)) {
            return $this->sendFailedLoginResponse($request);
        }

        $this->guard()->login($user);

        return redirect('/')->with($request->all());
    }

    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * Return the Order status name
     *
     * @param $status
     * @return string
     */
    private function getOrderStatusName($status)
    {
        switch ($status) {
            case Config::get('mercadopago.MP_SUBSCRIPTION_AUTHORIZED'):
                    $name = 'Pagado';
                break;

            case Config::get('mercadopago.MP_SUBSCRIPTION_PAUSED'):
                    $name = 'Pendiente';
                break;
        }

        return $name;
    }

    /**
     * Return the Payment type name
     *
     * @param $type
     * @return mixed
     */
    private function getPaymentTypeName($type)
    {
        switch ($type) {
            case 'credit_card':
                    $name = Config::get('mercadopago.MP_PAYMENT_CREDIT_CARD');
                break;
            case 'debit_card':
                    $name = Config::get('mercadopago.MP_PAYMENT_DEBIT_CARD');
                break;
            case 'ticket':
                    $name = Config::get('mercadopago.MP_PAYMENT_TICKET');
                break;
            case 'atm':
                    $name = Config::get('mercadopago.MP_PAYMENT_ATM');
                break;
        }

        return $name;
    }

    /**
     * Return the user customer object from MercadoPago
     *
     * @param $user
     * @return mixed
     */
    private function getMercadoPagoCustomer($user)
    {
        $profile = $user->load('profile');

        $customer = [
            "first_name" => $profile->first_name,
            "last_name" => $profile->last_name
        ];

        $mpCustomer = $this->mp->post ("/v1/customers", $customer);

        return $mpCustomer['response'];
    }

    private function parsePaymentResponse($paymentResponse)
    {
        switch ($paymentResponse['response']['status']) {
            case Config::get('mercadopago.MP_PAYMENT_APPROVED'):
                $response = [
                    'status' => $paymentResponse['response']['status'],
                    'message' => 'El pago fue procesado satisfactoriamente. Gracias por su compra!',
                ];
                break;
            case Config::get('mercadopago.MP_PAYMENT_PENDING'):
                $response = [
                    'status' => $paymentResponse['response']['status'],
                    'message' => 'El pago fue registrado correctamente. Siga los pasos indicados para completar su pago.',
                    'redirect' => $paymentResponse['response']['transaction_details']['external_resource_url'],
                ];
                break;
            default:
                $response = [
                    'status' => $paymentResponse['response']['status'],
                    'message' => 'Ocurrió un error con su pago. Por favor comuniquese con su banco.',
                ];
                break;
        }

        return $response;
    }

    public function platform($id) {
        if (!empty($id)) {
            $platfom = Platform::where('id',$id)->first();
            $mService = new Service;
            $areas = $mService->getAreas($platfom->services);
            return view('services.platforms', compact('areas','platfom'));
        }

        return redirect('/');
    }

    public function platformsArea($idPlatform,$idArea) {
        if(!empty($idPlatform) && !empty($idArea)) {
            $mService = new Service;
            $services = $mService->getByAreaPlatfom($idPlatform,$idArea);

            return view('services.list', compact('services','idPlatform'));
        }
    }

    private function checkForUpdateOrDowngrade($services, $newService, $platform)
    {
        $detach = true;

        foreach ($services as $service) {
            if ($service->id != $newService->id) {
                if ($platform == Config::get('constants.platformNames.PUBLICIST_NAME')) {
                    $detach = $service->area->id != $newService->area->id ? false : true;
                }

                if ($detach) {
                    $this->mp->put(
                        '/v1/subscriptions/'.$service->users()->where('id', Auth::user()->id)->first()->pivot->mp_subscription_id,
                        ['status' => 'cancelled']
                    );

                    $service->users()->detach(Auth::user()->id);
                }
            }
        }
    }

    /**
     * Get platform url name
     *
     * @param $platform
     * @return null|string
     */
    private function getPlatformUrlName($platform)
    {
        switch ($platform) {
            case 'GP Stats':
                    $name = 'gpstats';
                break;

            case 'Sport in Touch':
                    $name = 'sportsintouch';
                break;

            case 'Anunciantes':
                    $name = 'anunciantes';
                break;

            default:
                $name = null;
        }

        return $name;
    }
}
