<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $user = \App\User::where(['email' => request('email')])->orWhere(['username' => request('email')])->first();

        if (empty($user)) {
            return $this->sendFailedLoginResponse($request);
        }

        // "GP Stats"

        $platform = \App\Platform::with(['users' => function ($q) use ($user){
            $q->where('user_id', $user->id);
        }])->get();
        $platforms = $user->platforms;
        $esta = true;
        foreach ($platforms as $platform) {
            if($platform->id == 1){
                $esta = false;
            }
        }

        $user = $user->salt;
        Hash::setSalt($user);

        $field = filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)
            ? 'email'
            : 'username';

        if (Auth::attempt([$field => request('email'), 'password' => request('password')])) {
            if ($request->get('is')) {
                session(['pw' => request('password')]);

                return redirect()->intended('admin');
            }
            if($esta){
                $user = \App\User::where(['email' => request('email')])->orWhere(['username' => request('email')])->first();
                $platform = \App\Platform::find(1);
                $user->platforms()->attach([$platform->id]);
                $user->services()->attach(
                    $user->id,
                    [
                        'active' => true,
                        'expired_at' => null,
                        'mp_subscription_id' => null,
                        'platform_id' => $platform->id,
                    ]
                );
            }
            return redirect('/');
        }

        return redirect()->back()->withInput();
    }
}
