<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Profile;
use App\Sha512Hasher;
use App\Mail\newUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $scopeData = null;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('getStates');
    }

    public function showRegistrationForm()
    {
        $countries = \App\Country::pluck('name', 'id');
        $genders = ['male' => 'Masculino', 'female' => 'Femenino'];

        return view ('auth.register', compact('countries', 'genders'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'username' => 'required|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'country' => 'required',
            'state' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $hasher = new Sha512Hasher();

        $options = [
            'salt' => base_convert(sha1(uniqid(mt_rand(), true)), 16, 36)
        ];

        $hash = $hasher->make($data['password'], $options);

        $data['locked'] = false;
        $data['expired'] = false;
        $data['credentials_expired'] = false;
        $data['enabled'] = true;
        $data['roles'] = 'a:1:{i:0;s:11:"ROLE_PLAYER";}';
        $data['state_id'] = $data['state'];
        $data['birth_date'] = date("Y-m-d", strtotime(str_replace('/','-', $data['birth_date'])));

        $profile = Profile::create($data)->id;

        $user = User::create([
            'email' => $data['email'],
            'username' => $data['username'],
            'password' => $hash,
            'salt' => $options['salt'],
            'profile_id' => $profile
        ]);

        $profile = Profile::find($profile);

        $profile->update(['user_id' => $user->id]);
        $profile->save();

        //Send account details email to new user
        Mail::to($user->email)->send(new newUser($user));

        return $user;
    }

    public function getStates(Request $request)
    {
        $this->scopeData = $request->input('country');

        $states = \App\State::whereHas('country', function ($query) {
            $query->where('id', $this->scopeData);
        })->pluck('name', 'id');

        //$states = \App\State::get()->where('country_id', $data);

        return response()->json($states, 200);
    }
}
