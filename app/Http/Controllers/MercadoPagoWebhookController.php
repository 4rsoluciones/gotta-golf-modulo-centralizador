<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use SantiGraviano\LaravelMercadoPago\MP;
use SantiGraviano\LaravelMercadoPago\MercadoPagoException;

class MercadoPagoWebhookController
{
    /**
     * MercadoPago SDK handle
     *
     * @var MP
     */
    protected $mp;

    public function __construct()
    {
        //New instance of MercadoPago SDK
        $this->mp = new MP (Config::get('mercadopago.access_token'));
    }

    public function index()
    {
        $event = \GuzzleHttp\json_decode(Request()->getContent());

        if (!isset($event->type, $event->data) || !ctype_digit($event->data->id)) {
            return http_response_code(400);
        }

        if ($event->type == 'payment'){
            try {
                $payment_info = $this->mp->get('/v1/payments/'.$event->data->id);

                if ($payment_info['status'] == 200 && $payment_info['response']['status'] == 'approved') {
                    $payment = \App\Payment::
                        with('order')
                        ->where('mp_payment_id', $payment_info['response']['id'])
                        ->first();

                    $user = \App\User::where('email', $payment_info['response']['payer']['email'])->first();

                    if (!empty($payment)) {
                        $order = $payment->order;

                        $orderStatus = $order->status;

                        if ($orderStatus->name == Config::get('constants.payments.PAYMENT_PENDING')) {
                            $status = \App\OrderStatus::
                                where('name', Config::get('constants.payments.PAYMENT_APPROVED'))
                                ->first()->id;

                            $order->update(['order_status_id' => $status]);
                            $order->save();

                            $service = $order->services->first();

                            $service->users()->updateExistingPivot(
                                $user->id,
                                [
                                    'active' => true,
                                    'expired_at' => Carbon::now()->addDays($service->days)->toDateString()
                                ]
                            );
                        }
                    }

                    return Response('', $payment_info['status']);
                }
            } catch (MercadoPagoException $e) {
                return Response($e->getMessage(), $e->getCode());
            }
        }

        return Response('', 200);
    }
}