<?php

namespace App\Http\Controllers\API;

use App\User;
use Validator;
use App\Profile;
use App\Mail\newUser;
use App\Sha512Hasher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends Controller
{

    public $successStatus = 200;

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        $user = User::where(['username' => request('username')])->orWhere(['email' => request('username')])->first();

        if (!count($user)) {
            return response()->json(['error' => 'User not found'], 401);
        }

        $platforms = $user->platforms;

        $esta = true;
        foreach ($platforms as $platform) {
            if($platform->id == 1){
                $esta = false;
            }
        }

        Hash::setSalt($user->salt);

        $field = filter_var(request('username'), FILTER_VALIDATE_EMAIL)
            ? 'email'
            : 'username';

        if(Auth::attempt([$field => request('username'), 'password' => request('password')])) {
            $user = Auth::user();

            $success['token'] =  $user->createToken('gotta-modulo-centralizador')->accessToken;
            $success['user']    = $user->with('profile', 'platforms')->find($user->id);

            if($esta){
                // Crear platform y relacionarlo al usuario
                // Crear el servicio y relacionarlo al usuario
                $platform = \App\Platform::find(1);
                $user->services()->attach(
                    $user->id,
                    [
                        'active' => true,
                        'expired_at' => null,
                        'mp_subscription_id' => null,
                        'platform_id' => $platform->id,
                    ]
                );
                $user->platforms()->attach([$platform->id]);
            }

            return response()->json(['success' => $success], $this->successStatus);
        } else {
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    public function registerMobile(Request $request)
    {
        $rules = [
            'first_name' => 'nullable|string|max:255',
            'last_name' => 'nullable|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'username' => 'required|unique:users',
            'country' => 'nullable',
            'state_id' => 'nullable',
            'city' => 'nullable|string|max:255',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response(['status' => 'error', 'messages' => $validator->errors()], '400');
        }

        $hasher = new Sha512Hasher();
        $options = [
            'salt' => base_convert(sha1(uniqid(mt_rand(), true)), 16, 36)
        ];
        $hash = $hasher->make($request->input('password'), $options);
        $input['password'] = $hash;
        $input['salt'] = $options['salt'];


        $inputs['locked'] = false;
        $inputs['expired'] = false;
        $inputs['credentials_expired'] = false;
        $inputs['enabled'] = true;
        $inputs['birth_date'] = null;
        $inputs['roles'] = serialize(['ROLE_PLAYER']);
        $inputs['first_name'] = $request->input('first_name');
        $inputs['last_name'] = $request->input('last_name');
        $inputs['state_id'] = $request->input('state_id');
        $inputs['city'] = $request->input('city');

        $profile = \App\Profile::create($inputs)->id;


        // Create new user and assign the profile
        $user = User::create([
            'email' => $request->input('email'),
            'username' => $request->input('username'),
            'password' => $hash,
            'salt' => $options['salt'],
            'profile_id' => $profile
        ]);

        $user->platforms()->attach([1]);//GPSTAT
        $user->save();


      //  Mail::to($user->email)->send(new newUser($user));

        $profile = \App\Profile::find($profile);

        $profile->update(['user_id' => $user->id]);
        $profile->save();

        $user =  User::with('profile')->find($user->id);

        $user->services()->attach(
            10,
            [
                'active' => true,
                'platform_id' =>1,
            ]
        );
        return response()->json(['success' => $user],200);

        //return redirect('admin/users')->with('success', 'Usuario creado correctamente');

    }


    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'username' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'c_password' => 'required|same:password',
            'locked' => 'required|integer',
            'expired' => 'required|integer',
            'roles' => 'required',
            'credentials_expired' => 'required|integer',
            'enabled' => 'required|integer',
            'gender' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $input = $request->all();

        $hasher = new Sha512Hasher();

        $options = [
            'salt' => base_convert(sha1(uniqid(mt_rand(), true)), 16, 36)
        ];

        $hash = $hasher->make($input['password'], $options);

        $input['password'] = $hash;
        $input['salt'] = $options['salt'];

        //If has no state id set Santa Fe as default
        if (!isset($input['state_id']) || empty($input['state_id'])) {
            $input['state_id'] = 188;
        }

        $profile = Profile::create($input)->id;

        $input['profile_id'] = $profile;

        $user = User::create($input);

        $profile = Profile::find($profile);

        $profile->update(['user_id' => $user->id]);
        $profile->save();

        $user =  $user->with('profile', 'platforms')->find($user->id);

        return response()->json(['success' => $user], $this->successStatus);
    }

    /**
     * Details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        try {
            $user = Auth::user();

            if (!count($user)) {
                throw new ModelNotFoundException();
            }

            $user = $user->with('profile', 'platforms')->find($user->id);

            return response()->json(['success' => $user], $this->successStatus);
        } catch (ModelNotFoundException $e) {
            return Response()->json('User not found', 404);
        }

    }

    /**
     * List api
     *
     * @return \Illuminate\Http\Response
     */
    public function lists()
    {
        $users = User::all();

        return response()->json(['success' => $users], $this->successStatus);
    }

    /**
     * Edit api
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        try {
            $user = Auth::user();

            if (!count($user)) {
                throw new ModelNotFoundException();
            }

            $validator = Validator::make($request->input(), [
                'email' => 'nullable|email|unique:users',
                'locked' => 'nullable|boolean',
                'expired' => 'nullable|boolean',
                'credentials_expired' => 'nullable|boolean',
                'enabled' => 'nullable|boolean'
            ]);

            if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()], 401);
            }

            $input = $request->input();

            if (isset($input['password'])) {
                // Get new instance of custom Hasher to encode password with SHA512
                $hasher = new Sha512Hasher();

                // Create new random salt for encode the password
                $options = [
                    'salt' => base_convert(sha1(uniqid(mt_rand(), true)), 16, 36)
                ];

                //Create new password encoded with SHA512 algorithm
                $hash = $hasher->make($input['password'], $options);

                $input['password'] = $hash;
            }

            $profile = Profile::find($user->profile_id);

            $profile->update($input);
            $profile->save();

            $user->update($input);
            $user->save();

            $user = $user
                ->with(
                    'profile:id,first_name,last_name,enabled,roles,state_id,city,company_name,birth_date,gender,mobile_phonenumber',
                    'platforms'
                )
                ->select('id', 'email', 'username', 'profile_id')
                ->find($user->id);

            return response()->json(['success' => $user], $this->successStatus);
        } catch (ModelNotFoundException $e) {
            return Response()->json('User not found', 404);
        }
    }

    public function changePassword(Request $request, $email)
    {
        try {
            $user = \App\User::where('email', $email)->firstOrFail();

            $inputs = $request->input();

            $validator = Validator::make($inputs, [
                'encoded_password' => 'required'
            ]);

            if ($validator->fails()) {
                return Response()->json(['error' => $validator->errors()], 400);
            }

            $user->update([
                'password' => $inputs['encoded_password']
            ]);

            $user->save();

            return Response()->json('success', 200);

        } catch (ModelNotFoundException $e) {
            return Response()->json(['error' => 'Ususario no encontrado'], 404);
        }
    }

    public function editUser(Request $request, $email)
    {
        try {
            $user = \App\User::where('email', $email)->firstOrFail();

            $inputs = $request->input();

            if (isset($inputs['email'])) {
                unset($inputs['email']);
            }

            $validator = Validator::make($request->all(), [
                'locked' => 'boolean',
                'expired' => 'boolean',
                'credentials_expired' => 'boolean',
                'enabled' => 'boolean'
            ]);

            if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()], 400);
            }

            $profile = Profile::find($user->profile_id);

            if (isset($inputs['roles'])) {

                $userRoles = unserialize($user->profile->roles);
                $newRoles = [];

                foreach ($inputs['roles'] as $role) {
                    if (!in_array($role, $userRoles)) {
                        $newRoles[] = $role;
                    }
                }

                $inputs['roles'] = serialize(array_unique(array_merge($userRoles,$newRoles)));
            } else {
                unset($inputs['roles']);
            }

            if (empty($inputs['password'])) {
                unset($inputs['password']);
            }

            $profile->update($inputs);
            $profile->save();

            $user->update($inputs);
            $user->save();

            $user = $user->with('profile', 'platforms')->find($user->id);

            return response()->json(['success' => $user], $this->successStatus);

        } catch (ModelNotFoundException $e) {
            return Response()->json(['error' => 'Ususario no encontrado'], 404);
        }
    }

    /**
     * Remove api
     *
     * @return \Illuminate\Http\Response
     */
    public function remove()
    {
        $user = Auth::user();

        $user->token()->revoke();
        $user->token()->delete();

        $user->delete();

        return response()->json(['success' => 'Deleted'], $this->successStatus);
    }

    /**
     * Get user by email
     *
     * @param $email
     * @return \Illuminate\Http\Response
     */
    public function getUser($email)
    {
        try {
            $user = User::with('profile')->where(['email' => $email])->firstOrFail();

            return response()->json(['success' => $user], $this->successStatus);
        } catch (ModelNotFoundException $e) {
            return Response()->json('User not found', 404);
        }
    }

    /**
     * Create new user
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        $inputs = $request->input();

        $validator = Validator::make($inputs, [
            'username' => 'required|unique:users',
            'email' => 'required|email|unique:users',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()], 400);
        }

        // Get new instance of custom Hasher to encode password with SHA512
        $hasher = new Sha512Hasher();

        // Create new random salt for encode the password
        if (!isset($inputs['salt'])) {
            $options = [
                'salt' => base_convert(sha1(uniqid(mt_rand(), true)), 16, 36)
            ];
        } else {
            $options = [ 'salt' => $inputs['salt']];
        }

        //Generate random password or get received
        if (!isset($inputs['password'])) {
            $plainPassword = str_random(8);
        } else {
            $plainPassword = $inputs['password'];
        }

        $inputs['password'] = $plainPassword;

        //Create new password encoded with SHA512 algorithm
        $hash = $hasher->make($inputs['password'], $options);

        if (isset($inputs['roles'])) {
            $inputs['roles'] = serialize($inputs['roles']);
        } else {
            $inputs['roles'] = serialize(['ROLE_PLAYER']);
        }

        if (isset($inputs['birth_date'])) {
            $inputs['birth_date'] = date("Y-m-d", strtotime(str_replace('/','-', $inputs['birth_date'])));
        } else {
            $inputs['birth_date'] = null;
        }

        //If has no state id set Santa Fe as default
        if (!isset($inputs['state_id']) || empty($inputs['state_id'])) {
            $inputs['state_id'] = 188;
        }

        // Create new profile
        $profile = \App\Profile::create($inputs)->id;

        // Create new user and assign the profile
        $user = User::create([
            'email' => $inputs['email'],
            'username' => $inputs['username'],
            'password' => $hash,
            'salt' => $options['salt'],
            'profile_id' => $profile
        ]);

        // Add the Platforms to the User
        if (isset($inputs['platforms'])) {
            $user->platforms()->attach($inputs['platforms']);
            $user->save();

            $onlyPlayer = false;
        } else {
            $onlyPlayer = true;
        }

        $user->plainPassword = $plainPassword;

        //Send account details email to new user
        Mail::to($user->email)->send(new newUser($user));

        $profile = \App\Profile::find($profile);

        $profile->update(['user_id' => $user->id]);
        $profile->save();

        return response()->json(['success' => User::with('profile', 'platforms')->find($user->id)], 200);
    }
}