<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Service;
use App\Platform;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ServiceController extends Controller
{
    public $successStatus = 200;
    public $scopeVar = null;

    /**
     * List api
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $services = Service::get();

        return response()->json(['success' => $services], $this->successStatus);
    }

    /**
     * Create api
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:services',
            'description' => 'required',
            'long_description' => 'required',
            'price' => 'required|numeric',
            'highlighted' => 'required|integer',
            'important' => 'required|integer',
            'visible' => 'required|integer',
            'type' => 'required',
            'days' => 'required|integer',
            'trial_days' => 'required|integer',
            'is_lifetime' => 'required|integer',
            'debitDues' => 'required|integer',
            'service_type_id' => 'required|integer',
            'area_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $input = $request->all();

        $service = Service::create($input);

        return response()->json(['success' => $service], $this->successStatus);
    }

    /**
     * Details api
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function details($id)
    {
        $service = Service::with('platforms', 'orders')->find($id);

        if (count($service) > 0) {
            return response()->json(['success' => $service], $this->successStatus);
        }

        return response()->json(['error' => 'Service not found'], 404);
    }

    /**
     * Edit api
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request, $id)
    {
        $service = Service::find($id);

        if (count($service) > 0) {
            $validator = Validator::make($request->all(), [
                'name' => 'unique:services',
                'price' => 'numeric',
                'highlighted' => 'integer',
                'important' => 'integer',
                'visible' => 'integer',
                'days' => 'integer',
                'trial_days' => 'integer',
                'is_lifetime' => 'integer',
                'debitDues' => 'integer',
                'service_type_id' => 'integer',
                'area_id' => 'integer'
            ]);

            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 401);
            }

            $input = $request->all();

            $service->update($input);
            $service->save();

            return response()->json(
                ['success' => $service->with('platforms', 'orders')->find($service->id)],
                $this->successStatus
            );
        }

        return response()->json(['error' => 'Service not found'], 404);
    }

    /**
     * By platform api
     *
     * @param $platform
     * @return \Illuminate\Http\JsonResponse
     */
    public function servicesByPlatform($platform)
    {
        $this->scopeVar = $platform;

        $services = Service::whereHas('platforms', function ($query) {
            $query->where('id', $this->scopeVar);
        })->with('platforms', 'orders')->get();


        return response()->json(['success' => $services], $this->successStatus);

    }

    /**
     * User services for specified platform name
     *
     * @param $platform
     * @return \Illuminate\Http\JsonResponse
     */
    public function servicesByPlatformName($platform)
    {
        try {
            $platform = Platform::whereName(Config::get('constants.platformInternalNames.'.$platform))->firstOrFail();

            $services = Auth::user()
                ->services()
                ->whereActive(true)
                ->whereHas('platforms', function ($query) use ($platform) {
                    $query->where('id', $platform->id);
                })->get(['id', 'name'])->map(function ($item, $key) {
                    return $item->toArray() + ['pack' => $item->pack()->first()->fields->pluck('value', 'name')->toArray()];
                });

            return Response()->json($services->toArray(), 200);
        } catch (ModelNotFoundException $e) {
            return Response()->json('Model not found', 404);
        }
    }

    /**
     * By user api
     *
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function servicesByUser($user)
    {
        try {
            $user = \App\User::where('email', $user)->firstOrFail();

            return Response()->json(array_filter($user->services->map(
                function($item, $key) {
                    if ($item->pivot->active) {
                        $subscription = collect($item->pivot);

                        $subscription['status'] = 'active';
                        return $subscription;
                    }
                }
            )->toArray()), $this->successStatus);
        } catch (ModelNotFoundException $e) {
            return Response()->json('User Not found', 404);
        }
    }

    /**
     * By user without token api
     *
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function servicesByUserWithoutToken($user)
    {
        try {
            $user = \App\User::where('email', $user)->firstOrFail();

            return Response()->json(array_filter($user->services->map(
                function($item, $key) {
                    if ($item->pivot->active) {
                        $subscription = collect($item->pivot);

                        $subscription['status'] = 'active';
                        return $subscription;
                    }
                }
            )->toArray()), $this->successStatus);
        } catch (ModelNotFoundException $e) {
            return Response()->json('User Not found', 404);
        }
    }

    /**
     * Remove api
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove($id)
    {
        $service = Service::find($id);

        $service->delete();

        return response()->json(['success' => 'Service Deleted'], $this->successStatus);
    }

    /**
     * List services group by platforms
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function servicesGroupByPlatforms()
    {
        $services = Service::select('id','name', 'area_id')->where('visible',1)->get();
        $result = array();
        foreach($services as $service) {
            $platform = $service->platforms->toArray();
            $platform = $platform[0];

            $service->name = $service->area->name . ' - ' . $service->name;

            $service = $service->toArray();

            unset($service['area']);

            $result[$platform['name']][] = $service;

        }
        return response()->json(['success' => $result], $this->successStatus);
    }

    public function servicesUserByPlatform($user, $platform)
    {
        try {
            $user = \App\User::whereEmail($user)->firstOrFail();

            $platform = Platform::whereName(Config::get('constants.platformInternalNames.'.$platform))->firstOrFail();

            $services = $user->services()->whereActive(true)->whereHas('platforms', function ($query) use ($platform) {
                $query->where('id', $platform->id);
            })->get(['id', 'name'])->map(function ($item, $key) {
                return $item->toArray() + ['pack' => $item->pack()->first()->fields->pluck('value', 'name')->toArray()];
            });

            return Response()->json($services, $this->successStatus);
        } catch (ModelNotFoundException $e) {
            return Response()->json('Model not found', 404);
        }
    }
}
