<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\BusinessCategory;
use App\Business;

use Illuminate\Support\Facades\DB;

class BusinessController extends Controller {

    public function getCategories() {
        $categories = BusinessCategory::all()->toArray();
        return Response()->json(array_filter($categories));
    }
}
