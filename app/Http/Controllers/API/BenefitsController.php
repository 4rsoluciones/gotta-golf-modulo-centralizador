<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\BusinessCategory;
use App\Business;
use App\Coupons;

use Illuminate\Support\Facades\DB;

class BenefitsController extends Controller {

    public function getAll(Request $request) {

        $user = Auth::user();
        $fields = $request->all();
        $start = 0;
        $limit = 10;
        $dateNow = date("Y-m-d");
        if(isset($fields['start']) && is_numeric($fields['start']))
            $start = $fields['start'];

        if(isset($fields['limit']) && is_numeric($fields['limit']))
            $limit = $fields['limit'];

        $sqlIdsServicios = 'SELECT id FROM services WHERE name like "%Beneficios%" and visible = 1';
        $sqlIdsUsers = 'SELECT user_id FROM users_services WHERE active = 1 and service_id IN ('.$sqlIdsServicios.')';

        $sqlIdsBusiness = 'SELECT id FROM businesses WHERE status = "Aprobada" ';
        $data = array();
        /*$count = DB::select( DB::raw('
        SELECT count(x.count) count
        FROM (
            SELECT
                count(coupons.id) count,
                coupons.stock,
                count(coupons_user.id) as exchanged
            FROM coupons
            JOIN businesses
                ON businesses.id = coupons.business_id
            LEFT JOIN coupons_user
                ON coupons_user.coupons_id = coupons.id
            WHERE
                coupons.date_active > CURRENT_TIMESTAMP
                AND coupons.status = 1
                AND coupons.user_id IN('.$sqlIdsUsers.')
                AND coupons.business_id IN('.$sqlIdsBusiness.')
            GROUP BY coupons.id
            HAVING
                coupons.stock > exchanged
        ) AS x'
    ));*/

        $results = DB::select( DB::raw('
            SELECT
                coupons.*,
                businesses.logo logo,
                count(coupons_user.id) as exchanged
            FROM coupons
            JOIN businesses
                ON businesses.id = coupons.business_id
            LEFT JOIN coupons_user
                ON coupons_user.coupons_id = coupons.id
            WHERE
                coupons.date_active > CURRENT_TIMESTAMP
                AND coupons.status = 1
                AND coupons.user_id IN('.$sqlIdsUsers.')
                AND coupons.business_id IN('.$sqlIdsBusiness.')
            GROUP BY coupons.id
            HAVING
                coupons.stock > exchanged
            LIMIT '.$start.', '.$limit
        ));
        //$data['count'] = $count[0]->count;
        if(!empty($results) && count($results)) {
            foreach($results as $key => $result) {
                $changed = DB::select( DB::raw('
                    SELECT id FROM `coupons_user` WHERE `user_id` = '.$user->id.' AND '.$result->id.' = coupons_id
                '));
                if(!empty($changed)) {
                    $results[$key]->changed = 1;
                } else {
                    $results[$key]->changed = 0;
                }
            }
        } else {
            $results = array();
        }

        $data['results'] = $results;
        return Response()->json($data);
    }

    public function exchangeCoupon($id) {
        $user = Auth::user();
        $coupon = Coupons::findOrFail($id);
        $exits = $coupon->users()->where('user_id',$user->id)->get()->toArray();

        if(!empty($coupon)) {
            if($coupon->status == 1) {
                if($coupon->date_active > date('Y-m-d')) {
                    if(count($coupon->users) < $coupon->stock) {
                        if(empty($exits)) {
                            DB::table('coupons_user')->insert(
                                ['coupons_id' => $id,
                                'user_id' => $user->id]
                            );
                            return response()->json(['status'=>'ok']);
                        }
                        return response()->json(['status'=> '5','error' => 'El cupón ya fue canjeado.'],403);
                    }
                    return response()->json(['status'=> '4','error' => 'No hay stock'],403);
                }
                return response()->json(['status'=> '3','error' => 'El cupón esta vencido.'],403);
            }
            return response()->json(['status'=> '2','error' => 'El cupón no esta activado'],403);
        }
        return response()->json(['status'=> '1','error' => 'Not authorized.'],403);
    }
}
