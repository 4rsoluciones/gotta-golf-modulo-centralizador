<?php

namespace App\Http\Controllers\API;

use App\User;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Psr\Http\Message\ServerRequestInterface;
use Laravel\Passport\Bridge\PersonalAccessGrant;
use League\OAuth2\Server\Grant\RefreshTokenGrant;
use Laravel\Passport\Bridge\AccessTokenRepository;
use Laravel\Passport\Bridge\RefreshTokenRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\ResponseTypes\BearerTokenResponse;

class Sha512Grant extends PersonalAccessGrant
{
    public function doValidateClient($request)
    {
        return $this->validateClient($request);
    }

    public function doIssueAccessToken($accessTokenTTL, $client, $userIdentifier)
    {
        return $this->issueAccessToken($accessTokenTTL, $client, $userIdentifier);
    }

    public function doIssueRefreshToken($accessToken)
    {
        return $this->issueRefreshToken($accessToken);
    }

    /**
     * {@inheritdoc}
     */
    public function getIdentifier()
    {
        return 'password';
    }
}

class Sha512RefreshGrant extends RefreshTokenGrant
{
    public function doValidateClient($request)
    {
        return $this->validateClient($request);
    }

    public function doValidateOldRefreshToken($request, $clientId)
    {
        $this->setEncryptionKey('EFRE$');

        return $this->validateOldRefreshToken($request, $clientId);
    }

    /**
     * {@inheritdoc}
     */
    public function getIdentifier()
    {
        return 'refresh_token';
    }
}

class AccessTokenController extends \Laravel\Passport\Http\Controllers\AccessTokenController
{
    public function issueToken(ServerRequestInterface $request)
    {
        try {
            if ($request->getParsedBody()['grant_type'] == 'refresh_token') {
                $accessTokenTTL = new \DateInterval('P1Y');

                $app = Container::getInstance('app');

                $grant = new Sha512RefreshGrant($app->make(RefreshTokenRepository::class));
                $grant->setClientRepository(app('Laravel\Passport\Bridge\ClientRepository'));

                $grant->setAccessTokenRepository($app->make(AccessTokenRepository::class));
                $grant->setRefreshTokenTTL($accessTokenTTL);

                $client = $grant->doValidateClient($request);

                try {
                    $refreshTokenData = $grant->doValidateOldRefreshToken($request, $client->getIdentifier());

                    $user = \App\User::find($refreshTokenData['user_id']);

                    return $this->createAccessToken($request, $user);
                } catch (\Exception $e) {
                    return Response()->json(["message" => "Invalid Refesh token"], 400);
                }
            }

            $username = $request->getParsedBody()['username'];
            $password = $request->getParsedBody()['password'];

            $user = User::where('username', '=', $username)->orWhere(['email' => $username])->first();

            if (!count($user)) {
                return response()->json(["message" => "User not found"], 401);
            }

            $platforms = $user->platforms;

            $esta = true;
            foreach ($platforms as $platform) {
                if($platform->id == 1){
                    $esta = false;
                }
            }

            if($esta){
                // Crear platform y relacionarlo al usuario
                // Crear el servicio y relacionarlo al usuario
                $platform = \App\Platform::find(1);
                $user->services()->attach(
                    $user->id,
                    [
                        'service_id' => 10,
                        'active' => true,
                        'expired_at' => null,
                        'mp_subscription_id' => null,
                        'platform_id' => $platform->id,
                    ]
                );
                    
                $user->platforms()->attach([$platform->id]);

            }

            Hash::setSalt($user->salt);

            $field = filter_var($username, FILTER_VALIDATE_EMAIL)
                ? 'email'
                : 'username';

            if(Auth::attempt([$field => $username, 'password' => $password])) {
                $user = Auth::user();

                return $this->createAccessToken($request, $user);
            } else {
                return response()->json(['error'=>'Unauthorised'], 401);
            }
        }
        catch (ModelNotFoundException $e) { // user or email not found
            //return error message
            return response()->json(["message" => "User not found"], 401);
        }
        catch (OAuthServerException $e) { //password not correct..token not granted
            //return error message
            return response()->json(["message" => "The user credentials were incorrect.', 6, 'invalid_credentials"], 500);
        }
        catch (Exception $e) {
            ////return error message
            return response()->json(["message" => "Internal server error"], 500);
        }
    }

    private function createAccessToken($request, $user)
    {
        $accessTokenTTL = new \DateInterval('P1Y');

        $grant = new Sha512Grant;
        $grant->setClientRepository(app('Laravel\Passport\Bridge\ClientRepository'));

        $app = Container::getInstance('app');

        $grant->setAccessTokenRepository($app->make(AccessTokenRepository::class));
        $grant->setRefreshTokenRepository($app->make(RefreshTokenRepository::class));
        $grant->setRefreshTokenTTL($accessTokenTTL);

        $client = $grant->doValidateClient($request);

        $accessToken = $grant->doIssueAccessToken($accessTokenTTL, $client, $user->id);

        $refreshToken = $grant->doIssueRefreshToken($accessToken);

        $response = new BearerTokenResponse();
        $response->setAccessToken($accessToken);
        $response->setRefreshToken($refreshToken);

        $x = new \ReflectionObject($this->server);
        $p = $x->getProperty('privateKey');
        $p->setAccessible(true);
        $privateKey = $p->getValue($this->server);
        $response->setPrivateKey(($privateKey));

        $response->setEncryptionKey('EFRE$');

        $response = $response->generateHttpResponse(new \Zend\Diactoros\Response());


        return $this->withErrorHandling(function () use ($request, $response) {
            return new Response(
                $response->getBody(),
                $response->getStatusCode(),
                $response->getHeaders()
            );
        });
    }

    protected function validateClient(ServerRequestInterface $request)
    {
        list($basicAuthUser, $basicAuthPassword) = $this->getBasicAuthCredentials($request);

        $clientId = $this->getRequestParameter('client_id', $request, $basicAuthUser);
        if (is_null($clientId)) {
            throw OAuthServerException::invalidRequest('client_id');
        }

        // If the client is confidential require the client secret
        $clientSecret = $this->getRequestParameter('client_secret', $request, $basicAuthPassword);

        $client = $this->clientRepository->getClientEntity(
            $clientId,
            $this->getIdentifier(),
            $clientSecret,
            true
        );

        if ($client instanceof ClientEntityInterface === false) {
            $this->getEmitter()->emit(new RequestEvent(RequestEvent::CLIENT_AUTHENTICATION_FAILED, $request));
            throw OAuthServerException::invalidClient();
        }

        // If a redirect URI is provided ensure it matches what is pre-registered
        $redirectUri = $this->getRequestParameter('redirect_uri', $request, null);
        if ($redirectUri !== null) {
            if (
                is_string($client->getRedirectUri())
                && (strcmp($client->getRedirectUri(), $redirectUri) !== 0)
            ) {
                $this->getEmitter()->emit(new RequestEvent(RequestEvent::CLIENT_AUTHENTICATION_FAILED, $request));
                throw OAuthServerException::invalidClient();
            } elseif (
                is_array($client->getRedirectUri())
                && in_array($redirectUri, $client->getRedirectUri()) === false
            ) {
                $this->getEmitter()->emit(new RequestEvent(RequestEvent::CLIENT_AUTHENTICATION_FAILED, $request));
                throw OAuthServerException::invalidClient();
            }
        }

        return $client;
    }

}