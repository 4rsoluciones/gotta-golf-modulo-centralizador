<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Near;
use App\Business;
use App\Service;
use Illuminate\Support\Facades\DB;

class NearToMeController extends Controller {
    public function getNearMe(Request $request) {
        $postFields = $request->all();
        if(!empty($postFields)) {
            if(isset($postFields['longitude']) && isset($postFields['latitude'])) {

                $latitude = $postFields['latitude'];
                $longitude = $postFields['longitude'];
                $categoryId = isset($postFields['category_id'])?$postFields['category_id']:'';

                $sqlIdsServicios = 'SELECT id FROM services WHERE name like "%Cerca mío%" and visible = 1';
                $sqlIdsUsers = 'SELECT user_id FROM users_services WHERE active = 1 and service_id IN ('.$sqlIdsServicios.')';

                $sqlIdsBusiness = 'SELECT id FROM businesses WHERE status = "Aprobada" ';

                if(!empty($categoryId)) {
                    $categoryFilter = ' AND business_category_id IN ('.$categoryId.')';
                    $sqlIdsBusiness = $sqlIdsBusiness.$categoryFilter;
                }

                if(!isset($postFields['range']) || (isset($postFields['range']) && !is_numeric($postFields['range']))) {
                    $range = 30;
                } else {
                    $range = $postFields['range'];
                }
                $results = DB::select( DB::raw('
                    SELECT
                        nears.id,
                        nears.name,
                        nears.latitude,
                        nears.longitude,
                        nears.image,
                        nears.description,
                        nears.direction,
                        business_categories.name category,
                        businesses.logo logo,
                        (
                            3959 * acos (
                                cos ( radians('.$latitude.') )
                                * cos( radians( latitude ) )
                                * cos( radians( longitude ) - radians('.$longitude.') )
                                + sin ( radians('.$latitude.') )
                                * sin( radians( latitude ) )
                                )
                                ) AS distance
                                FROM nears
                                JOIN businesses
                                    ON businesses.id = nears.business_id
                                JOIN business_categories
                                    ON business_categories.id = businesses.business_category_id
                                WHERE nears.business_id IN ('.$sqlIdsBusiness.')
                                AND nears.user_id IN ('.$sqlIdsUsers.')
                                AND nears.status = 1

                                HAVING distance < '.$range.'
                                ORDER BY distance
                                LIMIT 0 , 20;
                            ')
                );

                return Response()->json(array_filter($results));
            }
        }
        return 'sin valores';
    }
}
