<?php

namespace App\Http\Controllers\Publicist;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BusinessController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('publicist');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $businesses = Auth::user()->businesses;

        return view('publicist.business.list', compact('businesses'));
    }

    public function create()
    {
        $categories = \App\BusinessCategory::pluck('name', 'id');

        return view('publicist.business.new', compact('categories'));
    }

    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        //Validate form input values
        if ($validator->fails()) {
            return redirect('/anunciantes/establecimientos/registro')
                ->withErrors($validator)
                ->withInput();
        }

        $data = $request->except('logo');

        $data['status'] = Config::get('constants.businessStatus.EVALUATION');
        $data['user_id'] = Auth::user()->id;

        try {
            $business = \App\Business::create($data);

            if ($request->hasFile('logo')) {
                //Upload new logo
                $path = Storage::putFileAs(
                    'business/'.$business->id.'/logo',
                    $request->file('logo'),
                    $request->file('logo')->getClientOriginalName()
                );

                $business->update(['logo' => $path]);
                $business->save();
            }

            return redirect('/anunciantes/establecimientos')->with(
                'success',
                'Establecimiento registrado correctamente. Pronto recibirás un email con información sobre tu registro.'
            );
        } catch (\Exception $e) {
            return redirect('/anunciantes/establecimientos')->with(
                'error',
                'Ocurrió un error al realizar el registro. Intente nuevamente.'
            );
        }
    }

    public function edit($id)
    {
        try {
            $business = \App\Business::findOrFail($id);

            $categories = \App\BusinessCategory::pluck('name', 'id');

            return view('publicist.business.edit', compact('business', 'categories'));
        } catch (ModelNotFoundException $e) {
            return redirect('/anunciantes/establecimientos')->with('error', 'Registro no encontrado');
        }
    }

    public function update($id)
    {
        try {
            $business = \App\Business::findOrFail($id);

            $validator = $this->validator(Request()->all(), $business->id);

            if ($validator->fails()) {
                return redirect('/anunciantes/establecimientos/edit/'.$business->id)
                    ->withErrors($validator)
                    ->withInput();
            }

            if (Request()->hasFile('logo')) {
                //Delete old logo
                Storage::delete($business->logo);

                //Upload new logo
                $path = Storage::putFileAs(
                    'business/'.$business->id.'/logo',
                    Request()->file('logo'),
                    Request()->file('logo')->getClientOriginalName()
                );

                $business->update(['logo' => $path]);
            }

            $business->update(Request()->except('logo'));

            $business->save();

            return redirect('/anunciantes/establecimientos')->with('success', 'Su establecimiento fue modificado correctamente.');
        } catch (ModelNotFoundException $e) {
            return redirect('/anunciantes/establecimientos')->with('error', 'Registro no encontrado');
        }
    }

    /**
     * Validate form
     *
     * @param array $data
     * @param $businessId
     * @return mixed
     */
    private function validator(array $data, $businessId = null)
    {
        $rules = [
            'name' => (!empty($businessId) ? 'nullable' : 'required').'|string|max:255',
            'business_name' => (!empty($businessId) ? 'nullable' : 'required').'|string|max:255',
            'description' => 'required|string|max:255',
            'cuit' => (!empty($businessId) ? 'nullable' : 'required').'|numeric|digits_between:10,11|unique:businesses,cuit'.(!empty($businessId) ? ','.$businessId : ''),
            'web' => 'nullable|string|max:150',
            'logo' => 'nullable|image',
            'business_category_id' => 'required',
        ];

        return Validator::make($data, $rules);
    }
}
