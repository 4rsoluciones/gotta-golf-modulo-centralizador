<?php

namespace App\Http\Controllers\Publicist;

use App\Area;
use App\Coupons;
use App\Service;
use App\Business;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class BenefitsController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('publicist');
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $area = Area::where('name', 'like', '%Beneficios%')->first();

//        $services = Auth::user()->services()
//        ->where('name', 'like', '%Beneficios%')
//        ->where('active', 1)
//        ->pluck('id')->toArray();

        $services = Auth::user()->services()
            ->where('area_id', $area->id)
            ->whereActive(true)
            ->whereHas('platforms', function ($query) {
                $query->whereName(Config::get('constants.platformNames.PUBLICIST_NAME'));
            })->pluck('id')->toArray();

        if(!empty($services)) {
            $service = Service::where('id',$services[0])->get();
            if(!empty($service[0]->pack[0]->fields)) {
                $packFields = $service[0]->pack[0]->fields->where('name', 'like', 'Negocios')->pluck('value')->toArray();
                $limitBusiness = $packFields[0];
                $packFields = $service[0]->pack[0]->fields->where('name', 'like', 'Cupones')->pluck('value')->toArray();
                $limitCupones = $packFields[0];
            }
            $user = Auth::user();
            $businesssIds = DB::table('business_service')
            ->where('service_id', $services[0])
            ->pluck('business_id')->toArray();
            $businesss = Business::whereIn('id', $businesssIds)->where('user_id',$user->id)->get();
        } else {
            $limitBusiness = array();
            $limitCupones = array();
            $businesss = array();
//            $businessServices = Service::where('name', 'like', '%Beneficios%')->get();

            $area = Area::where('name', 'like', '%Beneficios%')->first();

            $businessServices = Service::where('area_id', $area->id)->whereHas('platforms', function ($query) {
                $query->whereName(Config::get('constants.platformNames.PUBLICIST_NAME'));
            })->get();
        }
        return view('publicist.benefits.home', compact('services','limitBusiness','limitCupones','businesss','businessServices'));
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function add($id) {
        if(!empty($id) && is_numeric($id)) {
            $area = Area::where('name', 'like', '%Beneficios%')->first();

            $services = Auth::user()->services()
                ->where('area_id', $area->id)
                ->whereActive(true)
                ->whereHas('platforms', function ($query) {
                    $query->whereName(Config::get('constants.platformNames.PUBLICIST_NAME'));
                })->pluck('id')->toArray();

            if(!empty($services)) {
                $service = Service::where('id',$services[0])->get();
                if(!empty($service[0]->pack[0]->fields)) {
                    $packFields = $service[0]->pack[0]->fields->where('name', 'like', 'Cupones')->pluck('value')->toArray();
                    $limitCupones = $packFields[0];
                    $business = Business::findOrFail($id);
                    if(!empty($business) && (count($business->coupons) < $limitCupones)) {
                        $user = Auth::user();
                        if($business->user_id == $user->id) {
                            return view('publicist.benefits.add', compact('services','business'));
                        }
                    }
                }
            }
        }
        return redirect('anunciantes/beneficios')->with('error', 'No tiene permisos para ingresar a esta pagina');
    }

    /**
    * [add description]
    */
    public function create($id,Request $request) {
        $fields = $request->all();
        $validator = $this->validator($fields);

        if ($validator->fails()) {
            return redirect('anunciantes/beneficios/nuevo/'.$id)
            ->withErrors($validator)
            ->withInput();
        }
        $user = Auth::user();
        $business = Business::where('status','like','%Aprobada%')
        ->where('id',$id)
        ->where('user_id',$user->id)->get()->toArray();
        if(!empty($business) && isset($business[0])) {
            $fields['business_id'] = $id;
            $fields['user_id'] = $user->id;
            $fields['status'] = 0;
            $coupon = Coupons::create($fields);
            if ($request->hasFile('image')) {
                //Upload new logo
                $path = Storage::putFileAs(
                    'coupons/'.$coupon->id,
                    $request->file('image'),
                    $request->file('image')->getClientOriginalName()
                );
                $coupon->update(['image' => $path]);
                $coupon->save();
            }
            return redirect('anunciantes/beneficios')->with('success', 'El cupón fue creado correctamente.');
        }
        return redirect('anunciantes/beneficios/nuevo/'.$id)->withInput();
    }

    /**
    * [public description]
    * @var [type]
    */
    public function edit($id) {
        if(!empty($id) && is_numeric($id)) {
            $coupon = Coupons::findOrFail($id);
            $user = Auth::user();
            if(!empty($coupon) && !empty($user)) {
                $cUser = $coupon->user;
                if($user->id === $cUser->id) {
                    return view('publicist.benefits.edit',compact('coupon'));
                }
            }
        }
        return redirect('anunciantes/beneficios')->with('error', 'La sucursal no existe.');
    }

    /**
    * Validate form
    *
    * @param array $data
    * @param null $serviceId
    * @return mixed
    */

    /**
    * [public description]
    * @var [type]
    */
    public function update($id,Request $request) {
        $fields = $request->all();
        $validator = $this->validator($fields);

        if ($validator->fails()) {
            return redirect('anunciantes/beneficios/editar/'.$id)
            ->withErrors($validator)
            ->withInput();
        }
        $coupon = Coupons::findOrFail($id);
        $user = Auth::user();
        if(!empty($coupon) && !empty($user)) {
            $cUser = $coupon->user;
            if($user->id === $cUser->id) {
                if ($request->hasFile('image')) {
                    //Delete old logo
                    if(!empty($coupon->image)) {
                        Storage::delete($coupon->image);
                    }

                    //Upload new logo
                    $path = Storage::putFileAs(
                        'benefits/'.$coupon->id,
                        $request->file('image'),
                        $request->file('image')->getClientOriginalName()
                    );
                    $fields['image'] = $path;

                }

                $updateNow = $coupon->update($fields);
                return redirect('anunciantes/beneficios')->with('success', 'El cupón fue actualizado exitosamente');
            }
        }
    }

    /**
     * [public description]
     * @var [type]
     */
    public function delete($id) {
        if(!empty($id) && is_numeric($id)) {
            $coupon = Coupons::findOrFail($id);
            $user = Auth::user();
            if(!empty($coupon) && !empty($user)) {
                $cUser = $coupon->user;
                if($user->id === $cUser->id) {
                    if(!empty($coupon->image)) {
                        Storage::delete($coupon->image);
                    }
                    $coupon->delete();
                    return redirect('anunciantes/beneficios')->with('error', 'El cupón fue eliminado exitosamente.');
                }
            }
        }
        return redirect('anunciantes/beneficios');
    }

    private function validator(array $data) {
        return Validator::make($data, [
            'name' => 'required|string',
            'description' => 'required',
            'discount' => 'required',
            'date_active' => 'required',
            'stock' => 'required',
        ]);
    }

    /**
    *
    * @return [type] [description]
    */
    public function assign() {
        $user = Auth::user();

        $area = Area::where('name', 'like', '%Beneficios%')->first();

        $services = $user->services()->where('area_id', $area->id)->pluck('id')->toArray();

        if(!empty($services)) {
            $businesssIds = DB::table('business_service')->where('service_id', $services[0])->pluck('business_id')->toArray();
            $businesses[null] = 'Seleccione un establecimiento';
            $businesses = $businesses + Business::where('status','like','%Aprobada%')->where('user_id',$user->id)->whereNotIn('id', $businesssIds)->pluck('name','id')->toArray();

            return view('publicist.benefits.assign', compact('businesses'));
        }
        return redirect('anunciantes/cerca-mio')->with('error', 'No tiene permisos para ingresar a esta pagina');
    }

    /**
    *
    * @return [type] [description]
    */
    public function assignSave(Request $request) {
        $fields = $request->all();
        $user = Auth::user();
        $area = Area::where('name', 'like', '%Beneficios%')->first();

        $services = $user->services()->where('area_id', $area->id)->pluck('id')->toArray();
        if(!empty($services)) {
            DB::table('business_service')->insert([
                'business_id' => $fields['business_id'],
                'service_id' => $services[0]
            ]);
            return redirect('anunciantes/beneficios')->with('success', 'La sucursal fue actualizada exitosamente');
        }
    }

    /**
     * [actives description]
     * @return [type] [description]
     */
    public function actives($id) {
        if(!empty($id) && is_numeric($id)) {
            $user = Auth::user();
            $coupons = Coupons::where('business_id',$id)->where('user_id', $user->id)->get();
            $area = Area::where('name', 'like', '%Beneficios%')->first();

            $services = $user->services()->where('area_id', $area->id)->pluck('id')->toArray();
            $service = Service::where('id',$services[0])->get();
            $packFields = $service[0]->pack[0]->fields->where('name', 'like', 'Cupones')->pluck('value')->toArray();
            $limitCupones = $packFields[0];

            return view('publicist.benefits.actives', compact('coupons','limitCupones','id'));
        }
        return redirect('anunciantes/beneficios')->with('error', 'No tiene permisos para ingresar a esta pagina');
    }
    /**
     * [actives description]
     * @return [type] [description]
     */
    public function activesSave($id,Request $request) {
        if(!empty($id) && is_numeric($id)) {

            $user = Auth::user();
            $area = Area::where('name', 'like', '%Beneficios%')->first();

            $services = $user->services()->where('area_id', $area->id)->pluck('id')->toArray();
            $service = Service::where('id',$services[0])->get();
            $packFields = $service[0]->pack[0]->fields->where('name', 'like', 'Cupones')->pluck('value')->toArray();
            $limitCupones = $packFields[0];
            $fields = $request->all();
            $count = (!empty($fields['cuponsActive'])?count($fields['cuponsActive']):0);
            if ($count < $limitCupones) {
                DB::table('coupons')->where('business_id', $id)
                                  ->update(['status' => 0]);

                if(isset($fields['cuponsActive'])) {
                    DB::table('coupons')->whereIn('id', $fields['cuponsActive'])
                                      ->update(['status' => 1]);
                }
                return redirect('anunciantes/beneficios')->with('success', 'Las sucursales fueron actualizada exitosamente');
            }
        }
        return redirect('anunciantes/beneficios')->with('error', 'No tiene permisos para ingresar a esta pagina');
    }
}
