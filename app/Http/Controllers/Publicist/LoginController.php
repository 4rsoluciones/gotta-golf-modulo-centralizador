<?php

namespace App\Http\Controllers\Publicist;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/anunciantes';

    /**
     * Show the admin panel application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('publicist.auth.login');
    }

    public function login(Request $request)
    {
        $user = \App\User::where(['email' => request('email')])->orWhere(['username' => request('email')])->first();

        if (empty($user)) {
            return redirect('/anunciantes/login')->with(['error' => 'Usuario o contraseña incorrectos.']);
        }

        $salt = $user->salt;

        Hash::setSalt($salt);

        $field = filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)
            ? 'email'
            : 'username';

        if (Auth::attempt([$field => request('email'), 'password' => request('password')])) {
            $roles = (array) unserialize($user->profile->roles);

            if (!in_array('ROLE_PUBLICIST', $roles)) {
                $roles[] = 'ROLE_PUBLICIST';

                $profile = \App\Profile::find($user->profile->id);

                $profile->update(['roles' => serialize($roles)]);
                $profile->save();
            }

            return redirect($this->redirectPath());
        }

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/anunciantes');
    }
}
