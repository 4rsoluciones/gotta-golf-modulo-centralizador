<?php

namespace App\Http\Controllers\Publicist;

use App\User;
use App\Profile;
use App\Sha512Hasher;
use App\Mail\newPublicist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/anunciantes';
    protected $scopeData = null;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       //$this->middleware('guest')->except('getStates');
    }

    public function showRegistrationForm()
    {
        $countries = \App\Country::pluck('name', 'id');
        $genders = ['male' => 'Masculino', 'female' => 'Femenino'];

        return view ('publicist.auth.register', compact('countries', 'genders'));
    }

    /**
     * Handle a publicist registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $user = User::where('email', $request->get('email'))->first();
        if ($user) {
            if($user->isPublicist()){
                return redirect('/anunciantes/login')
                    ->with('error', 'El email ingresado ya se encuentra registrado como anunciante');
            }

            return redirect(('/anunciantes/confirmar'))->with('email', $request->get('email'));
        }

        $this->validator($request->all())->validate();

        $this->create($request->all());

        return redirect('/anunciantes/login')
            ->with('success', 'Ha sido registrado correctamente, ya puede iniciar sesión en el área de anunciantes');
    }

    public function inscription()
    {
        return view('publicist.auth.confirm');
    }

    public function confirm()
    {
        if (Session('email')) {
            $email = Session('email');

            return view('publicist.auth.confirm', compact('email'));
        }

        return redirect('/anunciantes/registro')->with('error', 'Ocurrió un error. Por favor intente nuevamente');
    }

    public function setPublicist(Request $request)
    {
        try {
            $user = User::where('email', $request->get('email'))->firstOrFail();

            Hash::setSalt($user->salt);

            if (Hash::check($request->get('password'), $user->password)) {
                $roles = (array) unserialize($user->profile->roles);

                $roles[] = 'ROLE_PUBLICIST';

                $profile = \App\Profile::find($user->profile->id);

                $profile->update(['roles' => serialize($roles)]);
                $profile->save();

                return redirect('/anunciantes/login')
                    ->with(
                        'success',
                        'Ha sido agregado como anunciante correctamente, ya puede iniciar sesión en el área de anunciantes'
                    );
            }

            return redirect('/anunciantes/'.$request->get('destiny'))
                ->with( [
                    'error' => 'Contraseña incorrecta intente nuevamente.',
                    'email' => $user->email
                ]);

        } catch (ModelNotFoundException $e) {
            return redirect('/anunciantes/registro')
                ->with(
                    'error',
                    'Registro no encontrado.'
                );
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'username' => 'required|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'country' => 'required',
            'state' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $hasher = new Sha512Hasher();

        $options = [
            'salt' => base_convert(sha1(uniqid(mt_rand(), true)), 16, 36)
        ];

        $hash = $hasher->make($data['password'], $options);

        $data['locked'] = false;
        $data['expired'] = false;
        $data['credentials_expired'] = false;
        $data['enabled'] = true;
        $data['roles'] = serialize(['ROLE_PUBLICIST']);
        $data['state_id'] = $data['state'];
        $data['birth_date'] = date("Y-m-d", strtotime(str_replace('/','-', $data['birth_date'])));

        $profile = Profile::create($data)->id;

        $user = User::create([
            'email' => $data['email'],
            'username' => $data['username'],
            'password' => $hash,
            'salt' => $options['salt'],
            'profile_id' => $profile
        ]);

        $profile = Profile::find($profile);

        $profile->update(['user_id' => $user->id]);
        $profile->save();

        //Send account details email to new user
        Mail::to($user->email)->send(new newPublicist($user));

        return $user;
    }

    public function getStates(Request $request)
    {
        $this->scopeData = $request->input('country');

        $states = \App\State::whereHas('country', function ($query) {
            $query->where('id', $this->scopeData);
        })->pluck('name', 'id');

        return response()->json($states, 200);
    }
}
