<?php

namespace App\Http\Controllers\Publicist;

use App\Area;
use App\Near;
use App\Service;
use App\Business;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class NearToMeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('publicist');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nearToMeServices = array();

        $area = Area::where('name', 'like', '%Cerca mío%')->first();

//        $services = Auth::user()->services()
//        ->where('name', 'like', '%Cerca mío%')
//        ->where('active', 1)
//        ->pluck('id')->toArray();

        $services = Auth::user()->services()
            ->where('area_id', $area->id)
            ->whereActive(true)
            ->whereHas('platforms', function ($query) {
                $query->whereName(Config::get('constants.platformNames.PUBLICIST_NAME'));
            })->pluck('id')->toArray();

        if(!empty($services)) {
            $service = Service::where('id',$services[0])->get();
            if(!empty($service[0]->pack[0]->fields)) {
                $packFields = $service[0]->pack[0]->fields->where('name', 'like', 'Negocios')->pluck('value')->toArray();
                $limitBusiness = $packFields[0];
                $packFields = $service[0]->pack[0]->fields->where('name', 'like', 'Sucursales')->pluck('value')->toArray();
                $limitSucursales = $packFields[0];
            }
            $user = Auth::user();
            $businesssIds = DB::table('business_service')
            ->where('service_id', $services[0])
            ->pluck('business_id')->toArray();
            $businesss = Business::whereIn('id', $businesssIds)
            ->where('status','like','%Aprobada%')
            ->where('user_id', $user->id)->get();
        } else {
            $businesss = array();
            $limitBusiness = array();
            $limitSucursales = array();

            $nearToMeServices = Service::where('area_id', $area->id)->whereHas('platforms', function ($query) {
                $query->whereName(Config::get('constants.platformNames.PUBLICIST_NAME'));
            })->get();
        }

        return view('publicist.near.home', compact('services','businesss','limitBusiness','limitSucursales','nearToMeServices'));
    }


    /**
     * Validate form
     *
     * @param array $data
     * @param null $serviceId
     * @return mixed
     */
    private function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string',
            'direction' => 'required|string',
            'description' => 'required',
            'longitude' => 'required',
            'latitude' => 'required',
        ]);
    }

    /**
     * [add description]
     */
    public function add($id) {

        if(!empty($id) && is_numeric($id)) {
            $user = Auth::user();
            $business = Business::where('status','like','%Aprobada%')
            ->where('id',$id)
            ->where('user_id',$user->id)->get()->toArray();
            if(!empty($business) && isset($business[0])) {
                $business = $business[0];
                $country[null] = '-- Seleccione una país --';
                $countries = $country+\App\Country::pluck('name', 'id')->toArray();
                $states = array();
                $states[null] = '-- Seleccione una provincia --';
                return view('publicist.near.add', compact('services','business','countries','states'));
            }
        }
        return redirect('anunciantes/cerca-mio')->with('error', 'La sucursal no existe.');
    }

    /**
     * [add description]
     */
    public function create($id,Request $request) {
        $fields = $request->all();
        $validator = $this->validator($fields);

        if ($validator->fails()) {
            return redirect('anunciantes/cerca-mio/nuevo/'.$id)
                ->withErrors($validator)
                ->withInput();
        }
        $user = Auth::user();
        $business = Business::where('status','like','%Aprobada%')
        ->where('id',$id)
        ->where('user_id',$user->id)->get()->toArray();
        if(!empty($business) && isset($business)) {
            $fields['business_id'] = $id;
            $fields['user_id'] = $user->id;
            $fields['status'] = 0;
            $near = Near::create($fields);
            if ($request->hasFile('image')) {
                //Upload new logo
                $path = Storage::putFileAs(
                    'near/'.$near->id,
                    $request->file('image'),
                    $request->file('image')->getClientOriginalName()
                );
                $near->update(['image' => $path]);
                $near->save();
            }
            return redirect('anunciantes/cerca-mio')->with('success', 'Sucursal creada correctamente.');
        }
        return redirect('anunciantes/cerca-mio')->with('error', 'La sucursal no existe.');
    }

    /**
     * [public description]
     * @var [type]
     */
    public function edit($id) {
        if(!empty($id) && is_numeric($id)) {
            $near = Near::findOrFail($id);

            if(!empty($near)) {
                $user = Auth::user();
                $cUser = $near->user;
                if($user->id === $cUser->id) {
                    $country = array();
                    $country[null] = '-- Seleccione una país --';
                    $countries = $country + \App\Country::pluck('name', 'id')->toArray();
                    $states = array();
                    $states[null] = '-- Seleccione una provincia --';
                    $states = $near->country ? ($states+\App\State::where('country_id', $near->country)->pluck('name', 'id')->toArray())  : $states;

                    return view('publicist.near.edit',compact('near','countries','states'));
                }
            }
        }
        return redirect('anunciantes/cerca-mio')->with('error', 'La sucursal no existe.');
    }

    /**
     * [public description]
     * @var [type]
     */
    public function update($id,Request $request) {
        $fields = $request->all();
        $validator = $this->validator($fields);

        if ($validator->fails()) {
            return redirect('anunciantes/cerca-mio/editar/'.$id)
                ->withErrors($validator)
                ->withInput();
        }
        $near = Near::findOrFail($id);
        if(!empty($near)) {
            $user = Auth::user();
            $cUser = $near->user;
            if($user->id === $cUser->id) {
                if ($request->hasFile('image')) {
                    //Delete old logo
                    if(!empty($near->image)) {
                        Storage::delete($near->image);
                    }
                    //Upload new logo
                    $path = Storage::putFileAs(
                        'near/'.$near->id,
                        $request->file('image'),
                        $request->file('image')->getClientOriginalName()
                    );
                    $fields['image'] = $path;
                }
                $updateNow = $near->update($fields);
                return redirect('anunciantes/cerca-mio')->with('success', 'La sucursal fue actualizada exitosamente');
            }
        }
        return redirect('anunciantes/cerca-mio');
    }

    /**
     * [public description]
     * @var [type]
     */
    public function delete($id) {
        if(!empty($id) && is_numeric($id)) {
            $near = Near::findOrFail($id);

            if(!empty($near)) {
                $user = Auth::user();
                $cUser = $near->user;
                if($user->id === $cUser->id) {
                    if(!empty($near->image)) {
                        Storage::delete($near->image);
                    }
                    $near->delete();
                    return redirect('anunciantes/cerca-mio')->with('error', 'La sucursal fue eliminada exitosamente.');
                }
            }
        }
        return redirect('anunciantes/cerca-mio');
    }

    /**
     *
     * @return [type] [description]
     */
    public function assign() {
        $user = Auth::user();
        $area = Area::where('name', 'like', '%Cerca mío%')->first();

        //$services = $user->services()->where('name', 'like', '%Cerca mío%')->pluck('id')->toArray();

        $services = $user->services()
            ->where('area_id', $area->id)
            ->whereActive(true)
            ->whereHas('platforms', function ($query) {
                $query->whereName(Config::get('constants.platformNames.PUBLICIST_NAME'));
            })->pluck('id')->toArray();

        if(!empty($services)) {
            $businesssIds = DB::table('business_service')->where('service_id', $services[0])->pluck('business_id')->toArray();
            $businesses[null] = 'Seleccione un establecimiento';
            $businesses = $businesses + Business::where('status','like','%Aprobada%')->where('user_id',$user->id)->whereNotIn('id', $businesssIds)->pluck('name','id')->toArray();

            return view('publicist.near.assign', compact('businesses'));
        }
        return redirect('anunciantes/cerca-mio')->with('error', 'No tiene permisos para ingresar a esta pagina');
    }

    /**
     *
     * @return [type] [description]
     */
    public function assignSave(Request $request) {
        $fields = $request->all();
        $user = Auth::user();
        $area = Area::where('name', 'like', '%Cerca mío%')->first();

        $services = $user->services()->where('area_id', $area->id)->get()->map(function ($item, $k) {
            if ($item->pivot->active) {
                return $item;
            }
        })->toArray();

        if(!empty($services)) {
            DB::table('business_service')->insert([
                'business_id' => $fields['business_id'],
                'service_id' => $services[0]['id']
            ]);
            return redirect('anunciantes/cerca-mio')->with('success', 'La sucursal fue actualizada exitosamente');
        }

    }

    /**
     * [actives description]
     * @return [type] [description]
     */
    public function actives($id) {
        if(!empty($id) && is_numeric($id)) {
            $user = Auth::user();
            $area = Area::where('name', 'like', '%Cerca mío%')->first();

            $nears = Near::where('business_id',$id)->where('user_id', $user->id)->get();

            $services = $user->services()->where('area_id', $area->id)->pluck('id')->toArray();
            $service = Service::where('id',$services[0])->get();
            $packFields = $service[0]->pack[0]->fields->where('name', 'like', 'Sucursales')->pluck('value')->toArray();
            $limitSucursales = $packFields[0];

            return view('publicist.near.actives', compact('nears','limitSucursales','id'));
        }
        return redirect('anunciantes/cerca-mio')->with('error', 'No tiene permisos para ingresar a esta pagina');
    }

    /**
     * [actives description]
     * @return [type] [description]
     */
    public function activesSave($id,Request $request) {
        if(!empty($id) && is_numeric($id)) {

            $user = Auth::user();
            $area = Area::where('name', 'like', '%Cerca mío%')->first();

            $services = $user->services()->where('area_id', $area->id)->pluck('id')->toArray();
            $service = Service::where('id',$services[0])->get();
            $packFields = $service[0]->pack[0]->fields->where('name', 'like', 'Sucursales')->pluck('value')->toArray();
            $limitSucursales = $packFields[0];
            $fields = $request->all();
            $count = (!empty($fields['nearActive'])?count($fields['nearActive']):0);
            if ($count < $limitSucursales) {
                DB::table('nears')->where('business_id', $id)
                                  ->update(['status' => 0]);
                if(isset($fields['nearActive'])) {
                    DB::table('nears')->whereIn('id', $fields['nearActive'])
                                      ->update(['status' => 1]);
                }
                return redirect('anunciantes/cerca-mio')->with('success', 'Las sucursales fueron actualizada exitosamente');
            }
        }
        return redirect('anunciantes/cerca-mio')->with('error', 'No tiene permisos para ingresar a esta pagina');
    }
}
