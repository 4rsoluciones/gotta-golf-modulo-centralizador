<?php

namespace App\Http\Controllers\Admin;

use App\Area;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AreaController extends Controller
{
    /**
     * Display a listing Areas.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $areas = Area::with('services')->get();

        return view('admin.areas.list', compact('areas'));
    }

    /**
     * Show the form for creating a new Area.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.areas.new');
    }

    /**
     * Store a newly created Area.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = request()->all();

        $validator = $this->validator($inputs);

        if ($validator->fails()) {
            return redirect('admin/areas/new')
                ->withErrors($validator)
                ->withInput();
        }

        $inputs['slug'] = str_slug($inputs['name'], '-');

        Area::create($inputs);

        return redirect('admin/areas')->with('success', 'Area creada correctamente');
    }

    /**
     * Display the specified Area.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $area = Area::with('services')->find($id);

        return view('admin.areas.detail', compact('area'));
    }

    /**
     * Show the form for editing the specified Area.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $area = Area::find($id);

        return view('admin.areas.edit', compact('area'));
    }

    /**
     * Update the specified Area.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = request()->all();

        $validator = $this->validator($inputs, $id);

        if ($validator->fails()) {
            return redirect('admin/areas/edit/'.$id)
                ->withErrors($validator)
                ->withInput();
        }

        $area = Area::find($id);

        $inputs['slug'] = str_slug($inputs['name'], '-');

        $area->update($inputs);
        $area->save();

        return redirect('admin/areas')->with('success', 'Area actualizada correctamente');
    }

    /**
     * Load delete Area confirmation form
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function drop($id)
    {
        $area = Area::find($id);

        return view('admin.areas.drop', compact('area'));
    }

    /**
     * Remove the specified Area.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $area = Area::find($id);

        $area->delete();

        return redirect('admin/areas')->with('success', 'Area eliminada correctamente');
    }

    /**
     * Validate form
     *
     * @param array $data
     * @param null $serviceId
     * @return mixed
     */
    private function validator(array $data, $areaId = null)
    {
        if (!empty($areaId)) {
            $nameRule = 'string|max:255|unique:areas,name,'.$areaId;
        } else {
            $nameRule = 'required|string|max:255|unique:areas';
        }
        return Validator::make($data, [
            'name' => $nameRule,
            'description' => 'required|string',
        ]);
    }
}
