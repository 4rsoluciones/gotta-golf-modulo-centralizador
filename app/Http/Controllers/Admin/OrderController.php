<?php

namespace App\Http\Controllers\Admin;

use App\PaymentType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    /**
     * List Orders
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $orders = \App\Order::with('status', 'promoCode', 'payments')->get();

        return view('admin.orders.list', compact('orders'));
    }

    /**
     * Show Order details
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $order = \App\Order::with('status', 'promoCode', 'payments', 'services')->find($id);

        return view('admin.orders.detail', compact('order'));
    }

    /**
     * Load Order edit form
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $order = \App\Order::with('status', 'promoCode', 'payments', 'services')->find($id);

        $statuses = \App\OrderStatus::pluck('name', 'id');

        $paymentTypes = \App\PaymentType::pluck('name', 'id');

        return view('admin.orders.edit', compact('order', 'statuses', 'paymentTypes'));
    }

    /**
     * Update Order record
     *
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        $inputs = request()->all();

        $inputs['cash_amount'] = $this->formatNumber($inputs['cash_amount']);

        $order = \App\Order::with('status', 'promoCode', 'payments', 'services')->find($id);

        $validator = $this->validator($inputs, $order);

        if ($validator->fails()) {
            return redirect('admin/orders/edit/'.$id)
                ->withErrors($validator)
                ->withInput();
        }

        if (isset($inputs['pay_cash']) && $order->pay_cash != 1) {

            if (($order->payments->sum('amount') + $inputs['cash_amount']) > $order->services->sum('price')) {
                return redirect('admin/orders/edit/'.$id)
                    ->withErrors(['cash_amount' => 'El monto ingresado supera el costo total de los servicios.'])
                    ->withInput();
            }

            $inputs['pay_cash'] = 1;

            $type = \App\PaymentType::where('name', 'Efectivo')->first()->id;

            \App\Payment::create([
                'amount' => $inputs['cash_amount'],
                'payment_type_id' => $type,
                'order_id' => $order->id,
            ]);

        } else {
            $inputs['pay_cash'] = (isset($inputs['pay_cash']) && $order->pay_cash == 1) ? 1 : 0;
        }

        $status = \App\OrderStatus::find($inputs['order_status_id']);

        $order->status()->associate($status);
        $order->update($inputs);
        $order->save();

        return redirect('admin/orders')->with('success', 'Orden actualizada correctamente');
    }

    public function addPayment(Request $request)
    {
        $inputs = $request->all();

        $inputs['amount'] = $this->formatNumber($inputs['amount']);

        $order = \App\Order::with('payments', 'services')->find($inputs['order_id']);

        $validator = $this->validator($inputs, $order, 'new-payment');

        if ($validator->fails()) {
            return response()->json($validator->errors(), 500);
        }

        \App\Payment::create($inputs);

        return response()->json('El pago fue registrado exitosamente.', 200);
    }

    /**
     * Form validator
     *
     * @param array $data
     * @param $oldData
     * @param string $form
     * @return mixed
     */
    private function validator(array $data, $oldData, $form = 'update')
    {
        $max = $oldData->services->sum('price') - $oldData->payments->sum('amount');

        switch ($form) {
            case 'update':
                    $rules = [
                        'order_status_id' => 'required'
                    ];

                    if (isset($data['pay_cash']) && $oldData->pay_cash != 1) {
                        $rules ['cash_amount'] = 'required|numeric|min:1|max:'.$max;
                    }
                break;

            case 'new-payment':
                    $rules = [
                        'payment_type_id' => 'required',
                        'amount' => 'required|numeric|min:1|max:'.$max,
                    ];
                break;
        }

        return Validator::make($data, $rules);
    }

    private function formatNumber($number)
    {
        return str_replace(',', '.', str_replace('.', '', $number));
    }
}