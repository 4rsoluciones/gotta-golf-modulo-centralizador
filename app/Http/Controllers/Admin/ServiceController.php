<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use \Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use SantiGraviano\LaravelMercadoPago\MP;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ServiceController extends Controller
{
    public $scopeVar = null;

    /**
     * MercadoPago SDK handle
     *
     * @var MP
     */
    protected $mp;

    public function __construct()
    {
        $this->middleware('auth.admin');

        //New instance of MercadoPago SDK
        $this->mp = new MP (Config::get('mercadopago.access_token'));
    }

    /**
     * List Services
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $services = \App\Service::get();

        $this->setServicesOrder();

        return view('admin.services/list', compact('services'));
    }

    /**
     * Show Service detail
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $service = \App\Service::with('platforms', 'orders', 'users')->find($id);
;
        $this->scopeVar = $service->id;

        // Get Orders associated to the Service
        $orders = \App\Order::whereHas('services', function ($query) {
            $query->where('id', $this->scopeVar);
        })->with('status', 'promoCode', 'payments')->get();

        $area = DB::table('areas')->find($service->area_id)->name;

        return view('admin.services/detail', compact('service', 'orders', 'area'));
    }

    /**
     * Load Service edit form
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        try {
            $service = \App\Service::with('platforms', 'orders', 'users')->findOrFail($id);
            $packs = $service->platforms->map(function ($item, $k) {
                return \App\Pack::where('id', $item->pivot->pack_id)->first();
            });
            $packsFields = [];
            if (count($packs)) {
                foreach ($packs as $pack) {
                    if(!empty($pack)) {
                        $packsFields[$pack->id] = $pack->fields->pluck('value', 'name')->toArray();
                    }
                }
            }

            $platforms = \App\Platform::all();
            $serviceTypes = \App\ServiceType::pluck('name', 'id');
            $debitDues = $this->getDebitDues();
            $areas = DB::table('areas')->pluck('name', 'id');
            $services = \App\Service::orderBy('order', 'ASC')->get();
            return view(
                'admin.services/edit',
                compact(
                    'service',
                    'platforms',
                    'serviceTypes',
                    'debitDues',
                    'areas',
                    'services',
                    'packsFields'
                )
            );
        } catch (ModelNotFoundException $e) {
            return redirect('/admin/services')->with('error', 'Registro no encontrado');
        }

    }

    /**
     * Update Service record
     *
     * @param $id
     * @return $this
     */
    public function update($id)
    {
        $inputs = request()->all();

        $inputs['price'] = (float) $inputs['price'];

        $validator = $this->validatoUpdate($inputs, $id);

        if ($validator->fails()) {
            return redirect('admin/services/edit/'.$id)
                ->withErrors($validator)
                ->withInput();
        }

        $service = \App\Service::with('platforms' , 'orders', 'users')->find($id);

        if (isset($inputs['visible'])) {
            $inputs['visible'] = 1;

            $planData = ['status' => 'active'];
        } else {
            $inputs['visible'] = 0;

            $planData = ['status' => 'inactive'];
        }

		//Update the status on MercadoPago
        $this->mp->put('/v1/plans/'.$service->mp_plan_id, $planData);

        $inputs['highlighted'] = (isset($inputs['highlighted'])) ? 1 : 0;

		//If new image drop the olf from disk and store the new
		if (Request()->hasFile('image')) {
			Storage::delete($service->image);

			$inputs['image'] = Request()->file('image')->store('services');
		}

        $service->update($inputs);
        $service->save();
        //Change status platfoms
        foreach($service->platforms as $platform) {
            if(!isset($inputs['platformActive'])) {
                $inputs['platformActive'] = array();
            }
            if(array_key_exists($platform->id,$inputs['platformActive'])) {
                DB::table('services_platforms')->where([
                    'service_id' => $service->id,
                    'platform_id' => $platform->id,
                ])->update(['active' => 1]);
            } else {
                DB::table('services_platforms')->where([
                    'service_id' => $service->id,
                    'platform_id' => $platform->id,
                ])->update(['active' => 0]);
            }
        }
        // add platforms
        if(!empty($inputs['platform'])) {
            foreach($inputs['platform'] as $idPlatform => $platformFields) {
                $packFields = [
                    'name' => $inputs['name']
                ];
                $pack = \App\Pack::create($packFields);
                foreach($platformFields as $name => $value) {
                    $packFields = [
                        'name' => $name,
                        'value' => $value,
                        'pack_id' => $pack->id
                    ];
                    $packField = \App\PackField::create($packFields);
                }
                // Check if are Platforms to add to the Service
                if (isset($inputs['platforms']) && count($inputs['platforms'])) {
                    $service->platforms()->attach($idPlatform, array('pack_id' => $pack->id));
                    $service->save();
                }
            }
        }

        //Update Order
        if($inputs['changedOrder'] !== 0 && !empty($inputs['serviceOrder'])) {
            foreach($inputs['serviceOrder'] as $idService => $value) {
                DB::table('services')->where([
                    'id' => $idService,
                ])->update(['order' => $value]);
            }
        }
        return redirect('admin/services')->with('success', 'Servicio actualizado correctamente');
    }

    /**
     * Load create Service form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        //$platforms = \App\Platform::pluck('name', 'id');
        $serviceTypes = \App\ServiceType::pluck('name', 'id');
        $debitDues = $this->getDebitDues();
        $debitDues = $this->getDebitDues();
        $platforms = \App\Platform::all();

        $areas = DB::table('areas')->pluck('name', 'id');

        return view('admin.services/new', compact('platforms', 'serviceTypes', 'debitDues', 'areas'));
    }

    /**
     * Store new Service record
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store()
    {

        $inputs = request()->all();
        //dd($inputs);
        $validator = $this->validator($inputs);

        if ($validator->fails()) {
            return redirect('admin/services/new')
                ->withErrors($validator)
                ->withInput();
        }
        //begin transaction
        DB::beginTransaction();
        try {

            $planData = $this->buildPlanData($inputs);

            $plan = $this->mp->post('/v1/plans', $planData);

            if ($plan['status'] != 201) {
                return redirect('admin/services')->with('error', 'Ocurrió un error creando el servicio.');
            }

    		//If have image store on disk
    		if (Request()->hasFile('image')) {
    			$inputs['image'] = Request()->file('image')->store('services');
    		}

            $inputs['mp_plan_id'] = $plan['response']['id'];
            //$inputs['mp_plan_id'] = '';

            $inputs['visible'] = (isset($inputs['visible'])) ? 1 : 0;
            $inputs['type'] = \App\ServiceType::find($inputs['service_type_id'])->name;
            $inputs['highlighted'] = (isset($inputs['highlighted'])) ? 1 : 0;
            $inputs['important'] = (isset($inputs['important'])) ? 1 : 0;
            $inputs['is_lifetime'] = (isset($inputs['is_lifetime'])) ? 1 : 0;
            $inputs['days'] = (isset($inputs['days']) ? $inputs['days'] : 0);
            $inputs['debitDues'] = 1;

            $service = \App\Service::create($inputs);
            if(!empty($inputs['platform'])) {

                foreach($inputs['platform'] as $idPlatform => $platformFields) {
                    $packFields = [
                        'name' => $inputs['name']
                    ];
                    $pack = \App\Pack::create($packFields);
                    foreach($platformFields as $name => $value) {
                        $packFields = [
                            'name' => $name,
                            'value' => $value,
                            'pack_id' => $pack->id
                        ];
                        $packField = \App\PackField::create($packFields);
                    }
                    // Check if are Platforms to add to the Service
                    if (isset($inputs['platforms']) && count($inputs['platforms'])) {
                        $service->platforms()->attach($idPlatform, array('pack_id' => $pack->id, 'active' => true));
                        $service->save();
                    }
                }
            }

            DB::commit();
            return redirect('admin/services')->with('success', 'Servicio creado correctamente.');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect('admin/services/new')
                ->with(['error' => 'Ocurrió un error al crear el servicio.'])
                ->withErrors($e->getMessage())
                ->withInput();
        }

    }

    /**
     * Load delete Service confirmation form
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function drop($id)
    {
        $service = \App\Service::with('platforms', 'orders')->find($id);

        $this->scopeVar = $service->id;

        $orders = \App\Order::whereHas('services', function ($query) {
            $query->where('id', $this->scopeVar);
        })->get(['id'])->toArray();

        return view('admin.services.drop', compact('service', 'orders'));
    }

    /**
     * Remove Service record
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove($id)
    {
        $service = \App\Service::find($id);

		$this->mp->put('/v1/plans/'.$service->mp_plan_id, ['status' => 'cancelled']);

        $service->delete();

        return redirect('admin/services')->with('success', 'Servicio eliminado correctamente');
    }

    /**
     * Return the fields pack for given platforms
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function getPacks(Request $request)
    {
        if (empty($request->input('platforms'))) {
            return null;
        }

        $packs = \App\Platform::whereIn('id', $request->input('platforms'))
            ->get(['id', 'name'])->mapWithKeys(function ($item, $k) {
                return [$item->name => $item->platformPacks];
            });

        return Response()->json($packs, 200);
    }

    /**
     * Build an array for pass to MercadoPago Api when create new plan
     *
     * @param $inputs
     * @return array
     */
    private function buildPlanData($inputs)
    {
        $res = [
            'description' => $inputs['description'],
            'auto_recurring' => [
                'frequency' => 1,
                'frequency_type' => 'months',
                'transaction_amount' => (float) $inputs['price'],
                'debit_date' => 25,
            ],
        ];

        if (isset($inputs['days']) && $inputs['days'] > 0) {
            $freeTrial = [
                'free_trial' => [
                    'frequency' => (int) $inputs['days'],
                    'frequency_type' => 'days',
                ]
            ];

            $res['auto_recurring'] = $res['auto_recurring'] + $freeTrial;
        }

        return $res;
    }

    /**
     * Return the Platforms to add and remove relationship with Service
     *
     * @param $servicePlatforms
     * @param $formPlatforms
     * @return array
     */
    private function getPlatformsToAttachAndDetach($servicePlatforms, $formPlatforms)
    {
        $res = [];

        $res['toAttach'] = array_diff($formPlatforms, $servicePlatforms);
        $res['toDetach'] = array_diff($servicePlatforms, $formPlatforms);

        return $res;
    }

    /**
     * Add and remove Platforms relationship with Service
     *
     * @param $service
     * @param $formPlatforms
     */
    private function attachAndDetachPlatforms($service, $formPlatforms)
    {
        if (count($formPlatforms)) {
            $platforms = $this->getPlatformsToAttachAndDetach(
                $service->platforms()->allRelatedIds()->toArray(),
                $formPlatforms
            );

            if (count($platforms['toAttach'])) {
                $service->platforms()->attach($platforms['toAttach']);
            }

            if (count($platforms['toDetach'])) {
                $service->platforms()->detach($platforms['toDetach']);
            }
        }
    }

    /**
     * Remove relationship between Platforms and Service
     *
     * @param $service
     */
    private function detachPlatforms($service) {
        if (count($service->platforms()->allRelatedIds())) {
            $platforms = \App\Platform::all()->pluck('id')->toArray();

            $service->platforms()->detach($platforms);
        }
    }

    /**
     * Validate form
     *
     * @param array $data
     * @param null $serviceId
     * @return mixed
     */
    private function validator(array $data, $serviceId = null)
    {
        //        if (!empty($serviceId)) {
        //            $nameValidator = 'required|string|max:255|unique:services,name,'.$serviceId;
        //        } else {
        //            $nameValidator = 'required|string|max:255|unique:services';
        //        }
        $data['gpstatsweb-estadísticas'] = null;
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'description' => 'required|string',
            'long_description' => 'required',
            'price' => 'required|numeric',
            'area_id' => 'required',
            'trial_days' => 'numeric',
            'platforms' => 'required',
            //'gpstatsweb-estadísticas' => 'required'
        ]);
    }

    /**
     * Validate form
     *
     * @param array $data
     * @param null $serviceId
     * @return mixed
     */
    private function validatoUpdate(array $data, $serviceId = null)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'description' => 'required|string',
            'long_description' => 'required',
            'price' => 'required|numeric',
            'area_id' => 'required',
            'trial_days' => 'numeric',
        ]);
    }

    /**
     * Return an array with debit dues options
     *
     * @return mixed
     */
    private function getDebitDues()
    {
        return Config::get('constants.debitDues');
    }

    private function setServicesOrder()
    {
        $platforms = \App\Platform::all();

        if (count($platforms)) {
            foreach ($platforms as $platform) {
                if (count($platform->services)) {
                    foreach ($platform->services->groupBy('area_id') as $area) {
                        if ($area->every(function ($value, $k) {
                            return $value->order == 0;
                        })) {
                            $order = 1;
                            foreach ($area as $service) {
                                $service->order = $order;

                                $service->save();

                                $order++;
                            }
                        }
                    }
                }
            }
        }
    }
}
