<?php

namespace App\Http\Controllers\Admin;

use App\Mail\businessStatusUpdate;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\BusinessCategory;
use Illuminate\Http\Request;
use App\Business;

class BusinessController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    /**
     * List Business
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = \App\User::whereHas('profile', function ($query) {
            $query->where('roles', 'like', '%PUBLICIST%');
        })->get();

        return view('admin.publicists/list', compact('users'));
    }

    public function show($id)
    {
        try {
            $user = \App\User::findOrFail($id);

            return view('admin.publicists/detail', compact('user'));
        } catch (ModelNotFoundException $e) {
            return redirect('/admin/publicists')->with('error', 'Registro no encontrado.');
        }
    }

    public function businessDetail($id)
    {
        try {
            $business = \App\Business::findOrFail($id);

            $statuses = config('constants.businessStatus');

            return view('admin.publicists.business.detail', compact('business', 'statuses'));
        } catch (ModelNotFoundException $e) {
            return redirect('/admin/publicists')->with('error', 'Registro no encontrado.');
        }
    }

    public function update($id)
    {
        try {
            $business = \App\Business::findOrFail($id);

            $validator = $this->validator(Request()->all());

            if ($validator->fails()) {
                return redirect('admin/publicists/places/'.$id)
                    ->withErrors($validator)
                    ->withInput();
            }

            $business->update([
                'cuit' => Request()->get('cuit'),
                'status' => config('constants.businessStatus.'.Request()->get('status'))
            ]);

            $business->save();

            //Send status update notification to the user
            Mail::to($business->owner->email)->send(new businessStatusUpdate($business));

            return redirect('/admin/publicists/'.$business->owner->id)->with('success', 'Establecimiento actualizado correctamente.');
        } catch (ModelNotFoundException $e) {
            return redirect('/admin/publicists')->with('error', 'Registro no encontrado.');
        }
    }

    /**
     * Validate form
     *
     * @param array $data
     * @param null $serviceId
     * @return mixed
     */
    private function validator(array $data)
    {
        return Validator::make($data, [
            'status' => 'required|string',
        ]);
    }

    /**
     * [public description]
     * @var [type]
     */
    public function categoriesList() {
        $categories = BusinessCategory::all();
        return view('admin.businesses.categories.list',compact('categories'));
    }

    /**
     * [public description]
     * @var [type]
     */
    public function categoriesNew() {
        $categories = BusinessCategory::find(1);
        return view('admin.businesses.categories.new');
    }

    /**
     * [public description]
     * @var [type]
     */
    public function categoriesCreate(Request $request) {
        $this->validate($request, [
            'name' => 'required',
        ]);

        BusinessCategory::create($request->all());

        return redirect('/admin/businesses/categories');
    }
    /**
     * [public description]
     * @var [type]
     */
    public function categoriesEdit($id) {
        if(!empty($id) && is_numeric($id)) {
            $category = BusinessCategory::findOrFail($id);
            if(!empty($category)) {
                return view('admin.businesses.categories.edit',compact('category'));
            }
        }
        return redirect('/admin/businesses/categories');
    }

    /**
     * [public description]
     * @var [type]
     */
    public function categoriesUpdate($id,Request $request) {
        $category = BusinessCategory::findorfail($id);
        $validatedData = $this->validate($request, [
            'name' => 'required',
        ]);
        $input = $request->all();
        $updateNow = $category->update($input);

        return redirect('/admin/businesses/categories');
    }

    /**
     * [public description]
     * @var [type]
     */
    public function getList() {
        $businesses = Business::all();
        return view('admin.businesses.list',compact('businesses'));
    }
}
