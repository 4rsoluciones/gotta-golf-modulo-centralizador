<?php

namespace App\Http\Controllers\Admin;

use Validator;
use App\PromoCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PromoCodeController extends Controller
{
    /**
     * Display a listing Promo Codes.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promoCodes = PromoCode::with('services', 'type')->get();

        return view('admin.promo-codes.list', compact('promoCodes'));
    }

    /**
     * Show the form for creating a new Promo Code.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services = \App\Service::pluck('name', 'id');

        $code = strtoupper(str_random(8));

        return view('admin.promo-codes.new', compact('services', 'code'));
    }

    /**
     * Store a newly created Promo Code.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = request()->all();

        $validator = $this->validator($inputs);

        if ($validator->fails()) {
            return redirect('admin/promo-codes/new')
                ->withErrors($validator)
                ->withInput();
        }

        if (!isset($inputs['services'])) {
            $inputs['services'] = \App\Service::pluck('id')->toArray();
        }

        $inputs['promo_code_type_id'] = 1;

        $promoCode = PromoCode::create($inputs);

        $promoCode->services()->attach($inputs['services']);

        return redirect('admin/promo-codes')->with('success', 'Código creado correctamente');
    }

    /**
     * Display the specified Promo Code.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $promoCode = PromoCode::with('services')->find($id);

        return view('admin.promo-codes.detail', compact('promoCode'));
    }

    /**
     * Show the form for editing the specified Promo Code.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $promoCode = PromoCode::with('services', 'type')->find($id);

        $services = \App\Service::pluck('name', 'id');

        return view('admin.promo-codes.edit', compact('promoCode', 'services'));
    }

    /**
     * Update the specified Promo Code.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = request()->all();

        $validator = $this->validator($inputs, $id);

        if ($validator->fails()) {
            return redirect('admin/promo-codes/edit/'.$id)
                ->withErrors($validator)
                ->withInput();
        }

        if (!isset($inputs['services'])) {
            $inputs['services'] = \App\Service::pluck('id')->toArray();
        }

        $promoCode = PromoCode::find($id);

        $promoCode->update($inputs);
        $promoCode->save();

        $promoCode->services()->sync($inputs['services']);

        return redirect('admin/promo-codes')->with('success', 'Código editado correctamente');
    }

    /**
     * Load delete Promo Code confirmation form
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function drop($id)
    {
        $promoCode = PromoCode::with('services')->find($id);

        return view('admin.promo-codes.drop', compact('promoCode'));
    }

    /**
     * Remove the specified Promo Code.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $promoCode = PromoCode::find($id);

        $promoCode->delete();

        return redirect('admin/promo-codes')->with('success', 'Código promocional eliminado correctamente');
    }

    /**
     * Validate form
     *
     * @param array $data
     * @param null $serviceId
     * @return mixed
     */
    private function validator(array $data, $promoCodeId = null)
    {
        if (!empty($promoCodeId)) {
            $codeRule = 'string|max:8|min:8|unique:promo_codes,code,'.$promoCodeId;
        } else {
            $codeRule = 'required|string|max:8|min:8|unique:promo_codes';
        }
        return Validator::make($data, [
            'code' => $codeRule,
            'discount' => 'required|numeric|min:1|max:100',
            'valid_until' => 'date',
        ]);
    }
}
