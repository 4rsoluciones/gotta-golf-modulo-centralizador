<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\State;
use App\Sha512Hasher;
use App\Mail\newUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use \Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }

    /**
     * List Users
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::with('profile', 'platforms')->get();

        foreach ($users as $user) {
            $user->roles = $this->getRolesNames(unserialize($user->profile->roles));
        }

        $isRedirect = (!empty(request()->only('success')) ? true : false);

        if ($isRedirect) {
            session()->flash('success', 'Sus datos fueron configurados correctamente');
        }

        return view(
            'admin.users/list',
            compact('users')
        );
    }

    /**
     * Show User details
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $user = User::with('profile', 'platforms')->find($id);
        $roles = unserialize($user['profile']['roles']);

        $roles = $this->getRolesNames($roles);

        if (isset($user['profile']['state_id'])) {
            $state = State::with('country')->find($user['profile']['state_id']);
        } else {
            $state = null;
        }

        return view('admin.users/detail', compact('user', 'roles', 'state'));
    }

    /**
     * Load User edit form
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::with('profile', 'platforms')->find($id);
        $roles = unserialize($user['profile']['roles']);

        $roles = $this->getRolesNames($roles);

        if (isset($user['profile']['state_id'])) {
            $state = State::with('country')->find($user['profile']['state_id']);

            $states = State::all()->where('country_id', $state['country']->id);
        } else {
            $state = $states = null;
        }

        $allRoles = $this->getRoles();

        $countries = \App\Country::pluck('name', 'id');
        $genders = ['male' => 'Masculino', 'female' => 'Femenino'];
        $platforms = \App\Platform::pluck('name', 'id');

        return view(
            'admin.users/edit',
            compact(
                'user',
                'roles',
                'state',
                'countries',
                'allRoles',
                'genders',
                'platforms',
                'states'
            )
        );
    }

    /**
     * Update User record
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id)
    {
        $user = User::with('profile', 'platforms')->find($id);

        $inputs = \request()->all();

        $validator = $this->validator($inputs, $user->id);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 200);
        }

        $profile = \App\Profile::find($user->profile_id);

        if (isset($inputs['roles']) && count($inputs['roles'])) {
            $inputs['roles'] = serialize($inputs['roles']);
        }

        $profile->update($inputs);
        $profile->save();

        //Check for Platforms to add or remove to the User
        if (isset($inputs['platforms']) && count($inputs['platforms'])) {

            $platforms = $this->getPlatformsToAttachAndDetach(
                $user->platforms()->allRelatedIds()->toArray(),
                $inputs['platforms']
            );

            if (count($platforms['toAttach'])) {
                $user->platforms()->attach($platforms['toAttach']);
            }

            if (count($platforms['toDetach'])) {
                $user->platforms()->detach($platforms['toDetach']);
            }
        } elseif(count($user->platforms()->allRelatedIds())) {
            $platforms = \App\Platform::all()->pluck('id')->toArray();

            $user->platforms()->detach($platforms);
        }

        $user->update($inputs);
        $user->save();

        return response()->json('success', 200);

        //return redirect('/admin/users');
    }

    /**
     * Load create User form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $allRoles = $this->getRoles();

        $countries = \App\Country::pluck('name', 'id');
        $genders = ['male' => 'Masculino', 'female' => 'Femenino'];
        $platforms = \App\Platform::pluck('name', 'id');

        return view(
            'admin.users/new',
            compact(
                'allRoles',
                'genders',
                'platforms',
                'countries'
            )
        );
    }

    /**
     * Store new User record
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return response(['status' => 'error', 'messages' => $validator->errors()], '200');

            /*return redirect('admin/users/new')
                ->withErrors($validator)
                ->withInput();*/
        }

        $inputs = \request()->all();

        // Get new instance of custom Hasher to encode password with SHA512
        $hasher = new Sha512Hasher();

        // Create new random salt for encode the password
        $options = [
            'salt' => base_convert(sha1(uniqid(mt_rand(), true)), 16, 36)
        ];

        //Generate random password
        $plainPassword = str_random(8);

        $inputs['password'] = $plainPassword;

        //Create new password encoded with SHA512 algorithm
        $hash = $hasher->make($inputs['password'], $options);

        $inputs['locked'] = false;
        $inputs['expired'] = false;
        $inputs['credentials_expired'] = false;
        $inputs['enabled'] = true;

        if (isset($inputs['roles'])) {
            $inputs['roles'] = serialize($inputs['roles']);
        } else {
            $inputs['roles'] = serialize(['ROLE_PLAYER']);
        }

        if (isset($inputs['birth_date'])) {
            $inputs['birth_date'] = date("Y-m-d", strtotime(str_replace('/','-', $inputs['birth_date'])));
        } else {
            $inputs['birth_date'] = null;
        }

        // Create new profile
        $profile = \App\Profile::create($inputs)->id;

        // Create new user and assign the profile
        $user = User::create([
            'email' => $inputs['email'],
            'username' => $inputs['username'],
            'password' => $hash,
            'salt' => $options['salt'],
            'profile_id' => $profile
        ]);

        // Add the Platforms to the User
        if (isset($inputs['platforms'])) {
            $user->platforms()->attach($inputs['platforms']);
            $user->save();

            $onlyPlayer = false;
        } else {
            $onlyPlayer = true;
        }

        $user->plainPassword = $plainPassword;

        //Send account details email to new user
        Mail::to($user->email)->send(new newUser($user));

        $profile = \App\Profile::find($profile);

        $profile->update(['user_id' => $user->id]);
        $profile->save();

        return response()->json(['status' => "success", 'email' => $user->email, 'onlyPlayer' => $onlyPlayer], 200);

        //return redirect('admin/users')->with('success', 'Usuario creado correctamente');

    }

    /**
     * Load User delete confirmation form
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function drop($id)
    {
        $user = User::with('profile', 'platforms')->find($id);
        $roles = unserialize($user['profile']['roles']);

        $roles = $this->getRolesNames($roles);

        return view('admin.users/drop', compact('user', 'roles'));
    }

    /**
     * Remove User record
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove($id)
    {
        $user = User::find($id);

        $user->delete();

        return redirect('admin/users')->with('success', 'Usuario eliminado correctamente');
    }

    /**
     * Get Platforms to add or remove relationship with User
     *
     * @param $userPlatforms
     * @param $formPlatforms
     * @return array
     */
    private function getPlatformsToAttachAndDetach($userPlatforms, $formPlatforms)
    {
        $res = [];

        $res['toAttach'] = array_diff($formPlatforms, $userPlatforms);
        $res['toDetach'] = array_diff($userPlatforms, $formPlatforms);

        return $res;
    }

    /**
     * Return the Roles names
     *
     * @param $roles
     * @return array
     */
    private function getRolesNames($roles)
    {
        $names = [];

        if ($roles) {
            foreach ($roles as $role) {
                $names[] = Config::get('constants.roles')[$role];
            }
        }

        return $names;
    }

    /**
     * Return array with all Roles
     *
     * @return mixed
     */
    private function getRoles()
    {
        $roles = Config::get('constants.roles');

        return $roles;
    }

    /**
     * Return array with roles per Platforms
     *
     * @param $platforms
     * @return array
     */
    private function getRolesByPlatform($platforms)
    {
        $roles = [];

        foreach ($platforms as $platform) {
            switch ($platform) {
                case 'GP Stats' :
                    foreach (Config::get('constants.gpStatsRoles') as $role) {
                        $roles[$role] = Config::get('constants.roles')[$role];
                    }
                    break;
                case 'Sport in Touch':
                    foreach (Config::get('constants.sportInTouchRoles') as $role) {
                        $roles[$role] = Config::get('constants.roles')[$role];
                    }

                case 'Anunciantes':
                    foreach (Config::get('constants.publicistRoles') as $role) {
                        $roles[$role] = Config::get('constants.roles')[$role];
                    }
                    break;
            }
        }

        return $roles;
    }

    /**
     * Form validator
     *
     * @param array $data
     * @return mixed
     */
    private function validator(array $data, $userId = null)
    {
        if (empty($userId)) {
            $rules = [
                'first_name' => 'nullable|string|max:255',
                'last_name' => 'nullable|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'username' => 'required|unique:users',
                //'password' => 'required|string|min:6|confirmed',
                'country' => 'nullable',
                'state_id' => 'nullable',
                'city' => 'nullable|string|max:255',
                'birth_date' => 'nullable|date',
                'roles' => 'nullable',
                'gender' => 'nullable',
                'platforms' => 'nullable'
            ];
        } else {
            $rules = [
                'first_name' => 'nullable|string|max:255',
                'last_name' => 'nullable|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email,'.$userId,
                'username' => 'required|unique:users,username,'.$userId,
                'country' => 'nullable',
                'state_id' => 'nullable',
                'city' => 'nullable|string|max:255',
                'birth_date' => 'nullable|date',
                'roles' => 'nullable',
                'gender' => 'nullable',
                'platforms' => 'nullable'
            ];
        }

        return Validator::make($data, $rules);
    }

    /**
     * Return Json of all Roles for Ajax request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function getRolesAjax(Request $request)
    {
        if (empty($request->input('platforms'))) {
            return null;
        }

        $platforms = is_array($request->get('platforms')) ? $request->get('platforms') : [$request->get('platforms')];

        $platformNames = \App\Platform::whereIn('id', $platforms)->pluck('name')->toArray();

        $roles = $this->getRolesByPlatform($platformNames);

        return response()->json($roles, 200);
    }

    public function showImportForm()
    {
        $platforms = \App\Platform::pluck('name', 'id');

        return view('admin.users.import', compact('platforms'));
    }

    public function import(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'platform' => 'required|exists:platforms,id',
            'service' => 'required|exists:services,id',
            'roles' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('users.import')->with('error', 'Debe completar todos los datos.');
        }

        if($request->hasFile('file')){
            $extension = File::extension($request->file->getClientOriginalName());

            if ($extension == "xlsx" || $extension == "xls") {

                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {

                })->get();

                if(!empty($data) && $data->count()){

                    foreach ($data as $key => $value) {
                        if ($value->email) {
                            $importUser = User::whereEmail($value->email)->first();

                            if (!$importUser) {
                                $inputs = [];

                                $salt = $this->makeSalt();

                                $inputs['password'] = $this->makePasswordHash($salt, $value->clave);
                                $inputs['first_name'] = $value->nombre;
                                $inputs['last_name'] = $value->apellido;
                                $inputs['locked'] = false;
                                $inputs['expired'] = false;
                                $inputs['credentials_expired'] = false;
                                $inputs['enabled'] = true;
                                $inputs['birth_date'] = date("Y-m-d", strtotime(str_replace('/','-', $value->fecha_de_nacimiento)));
                                $inputs['roles'] = serialize($request->get('roles'));
                                $inputs['state_id'] = 188;

                                // Create new profile
                                $profile = \App\Profile::create($inputs);

                                // Create new user and assign the profile
                                $user = User::create([
                                    'email' => $value->email,
                                    'username' => $value->email,
                                    'password' => $inputs['password'],
                                    'salt' => $salt,
                                    'profile_id' => $profile->id
                                ]);

                                // Add the Platform to the User
                                $user->platforms()->attach($request->get('platform'));
                                $user->save();

                                $profile->update(['user_id' => $user->id]);
                                $profile->save();

                                $user->services()->attach([ $request->get('service') => [
                                    'active' => true,
                                    'platform_id' => $request->get('platform'),
                                ]
                                ]);
                            } else {
                                $inputs = [];

                                $salt = $this->makeSalt();

                                $inputs['password'] = $this->makePasswordHash($salt, $value->clave);
                                $inputs['first_name'] = $value->nombre;
                                $inputs['last_name'] = $value->apellido;
                                $inputs['birth_date'] = date("Y-m-d", strtotime(str_replace('/','-', $value->fecha_de_nacimiento)));
                                $inputs['roles'] = serialize($request->get('roles'));

                                // Update user profile
                                $importUser->profile()->update($inputs);

                                // Update user and assign the profile
                                $importUser->update([
                                    'password' => $inputs['password'],
                                    'salt' => $salt,
                                ]);

                                // Add the Platform to the User
                                $importUser->platforms()->sync($request->get('platform'));
                                $importUser->save();

                                $importUser->services()->sync([ $request->get('service') => [
                                    'active' => true,
                                    'platform_id' => $request->get('platform'),
                                ]
                                ]);
                            }
                        }
                    }
                }

                return redirect()->route('admin.users')->with('success', 'Ususarios importados correctamente.');

            }else {
                return redirect()->route('admin.users')->with('error', 'Solo se pueden cargar archivos .xls y .xlsx');
            }
        }
        return redirect()->route('admin.users')->with('error', 'Debe adjuntar un archivo.');
    }

    private function makePasswordHash($salt, $plainPassword)
    {
        // Get new instance of custom Hasher to encode password with SHA512
        $hasher = new Sha512Hasher();

        // Create new random salt for encode the password
        $options = [
            'salt' => $salt,
        ];

        //Create new password encoded with SHA512 algorithm
        return $hasher->make($plainPassword, $options);
    }

    private function makeSalt()
    {
        return base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
    }
}
