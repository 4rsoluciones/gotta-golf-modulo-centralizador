<?php

namespace App\Http\Controllers;

use App\Platform;
use App\Sha512Hasher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class FrontController extends Controller
{
    /**
     * Show all services areas
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $platforms = Platform::
            where('name', '<>', Config::get('constants.platformNames.PUBLICIST_NAME'))
            ->get();

        if (Session('platform')) {
            Request()->session()->put('platform', Session('platform'));
        }

        if(Session('redirectTo')) {
            Request()->session()->put('redirectTo', Session('redirectTo'));
        }

        return view('welcome', compact('platforms'));
    }

    public function areaServices($id)
    {
        $services = \App\Service::where('area_id', $id)->get();

        return view('services.list', compact('services'));
    }

    public function services(Request $request)
    {
        $platformName = $this->getPlatformName($request->get('platform'));

        return redirect('/')->with($request->all());
    }

    public function sha512Password(Request $request)
    {
        if ($request->exists(['password', 'salt'])) {
            $hasher = new Sha512Hasher();

            return Response($hasher->make($request->get('password'), ['salt' => $request->get('salt')]));
        }

        return redirect('/');
    }

    private function getPlatformName($platform)
    {
        switch ($platform) {
            case 'stats':
                    $res = Config::get('constants.platformNames.GPSTATS_NAME');
                break;
            case 'sportintouch':
                    $res = Config::get('constants.platformNames.SPORT_IN_TOUCH_NAME');
                break;
            default:
                    $res = null;
                break;
        }

        return $res;
    }
}
