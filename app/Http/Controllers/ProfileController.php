<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the user profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = Auth::user()->with('profile', 'services')->find(Auth::user()->id);

        return view('user.profile', compact('profile'));
    }

    public function edit($id)
    {
        $user = Auth::user();
        if($user->id == $id) {
            try {
                $profile = Auth::user()->with('profile', 'services')->findOrFail($id);

                $states = $profile->profile->state ? \App\State::where('country_id', $profile->profile->state->country->id)->pluck('name', 'id')  : null;

                $countries = \App\Country::pluck('name', 'id');
                $genders = ['male' => 'Masculino', 'female' => 'Femenino'];

                return view('user.profile-edit', compact('profile', 'countries', 'genders', 'states'));

            } catch (ModelNotFoundException $e) {
                return redirect('/profile')
                    ->with(['error' => 'Registro no encontrado.']);
            }
        }
        return redirect('/profile')
            ->with(['error' => 'Registro no encontrado.']);
    }

    public function update(Request $request, $id)
    {
        try {
            $user =\App\User::findOrFail($id);

            $inputs = $request->all();

            if (isset($inputs['old_password']) && !empty($inputs['old_password'])) {
               \Hash::setSalt($user->salt);

                if (\Hash::check($inputs['old_password'], $user->password)) {
                    $passwordRule = 'required|string|min:6|confirmed';
                } else {
                    $errorPassword = true;
                    $passwordRule = 'nullable';
                }

            } else {
                $passwordRule = 'nullable';
            }

            $validator = Validator::make($inputs, [
                'email' => 'required|email|unique:users,email,'.$user->id,
                'new_password' => $passwordRule,
            ]);

            if ($validator->fails() || isset($errorPassword)) {
                if (isset($errorPassword)) {
                    $validator->errors()->add('old_password', 'La contraseña actual ingresada es incorrecta.');
                }

                return redirect('/profile/edit/'.$id)
                    ->withErrors($validator)
                    ->withInput();
            }

            if (isset($inputs['old_password']) && !empty($inputs['old_password'])) {
                $inputs['password'] = \Hash::make($inputs['new_password'], ['salt' => $user->salt]);
            }

            $profile = \App\Profile::find($user->profile_id);

            $profile->update($inputs);
            $profile->save();

            $user->update($inputs);
            $user->save();

            return redirect('/profile')->with('success', 'Registro editado correctamente.');

        } catch (ModelNotFoundException $e) {
            return redirect('/profile')
                ->with(['error' => 'Registro no encontrado.']);
        }
    }
}
