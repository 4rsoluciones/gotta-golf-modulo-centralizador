<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class PublicistMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::guard()->check()) {
            return redirect('/anunciantes/login');
        }

        if (Auth::user() && in_array('ROLE_PUBLICIST', (array) unserialize(Auth::user()->profile->roles))) {
            return $next($request);
        }

        return back();
    }
}
