<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::guard()->check()) {
            return redirect('/admin/login');
        }

        if (Auth::guard()->check()) {
            $user = Auth::user();

            $user = $user->with('profile')->find($user->id);

            if (!in_array('ROLE_SUPER_ADMIN', unserialize($user['profile']['roles']))){
                return redirect('/home');
            }
        }

        return $next($request);
    }
}
