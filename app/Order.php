<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pay_cash', 'order_status_id', 'promo_code_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'order_status_id', 'promo_code_id'
    ];

    public function status()
    {
        return $this->belongsTo('App\OrderStatus','order_status_id');
    }

    public function promoCode()
    {
        return $this->belongsTo('App\PromoCode');
    }

    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    public function services()
    {
        return $this->belongsToMany('App\Service', 'services_orders');
    }
}
