<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pack extends Model
{
    protected $fillable = [
        'name', 'value',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'pivot',
    ];

    public function servicebyPlatform()
    {
        return $this->hasOne('App\ServicePlatform');
    }

    public function fields()
    {
        return $this->hasMany('App\PackField');
    }
}
