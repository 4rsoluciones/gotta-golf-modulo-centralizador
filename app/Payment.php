<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount', 'payment_type_id', 'order_id', 'mp_payment_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'payment_type_id', 'order_id', 'mp_payment_id',
    ];

    public function type()
    {
        return $this->belongsTo('App\PaymentType', 'payment_type_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
