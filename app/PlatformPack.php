<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlatformPack extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'platform_id',
    ];

    public function platform()
    {
        return $this->belongsTo('App\Platform');
    }
}
