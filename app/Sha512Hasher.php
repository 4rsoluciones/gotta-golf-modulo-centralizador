<?php

namespace App;

use Illuminate\Contracts\Hashing\Hasher;
use App\SymfonyDigestPasswordEncoder;

class Sha512Hasher implements Hasher
{
    private $salt;

    /**
     * @return mixed
     */
    const SALT = '145154181';

    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param mixed $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }


    public function make($value, array $options = [])
    {
        $encoder = new SymfonyDigestPasswordEncoder();

        if (isset($options['salt'])) {
            $hash = $encoder->encodePassword($value, $options['salt']);
        } else {
            $hash = $hash = $encoder->encodePassword($value, self::SALT);
        }



        if ($hash === false) {
            throw new \RuntimeException('Sha512 hashing not supported.');
        }

        return $hash;
    }

    public function check($value, $hashedValue, array $options = [])
    {
        $encoder = new SymfonyDigestPasswordEncoder('sha512', true, 5000);

        $try1 = $encoder->isPasswordValid($hashedValue, $value, $this->salt);
        return $try1 || $encoder->isPasswordValid($hashedValue, $value, self::SALT);
    }

    public function needsRehash($hashedValue, array $options = [])
    {
        return false;
    }
}