<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Platform extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    protected $hidden = [
        'pivot',
    ];

    public function users()
    {
        return $this->belongsToMany('App\User', 'users_platforms');
    }

    public function services()
    {
        return $this->belongsToMany('App\Service', 'services_platforms')
            ->using('App\ServicePlatform')
            ->withPivot('pack_id');
    }

    public function platformPacks()
    {
        return $this->hasMany('App\PlatformPack');
    }
}
