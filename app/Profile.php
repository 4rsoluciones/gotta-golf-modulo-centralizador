<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'enabled',
        'last_login',
        'locked',
        'expired',
        'expires_at',
        'confirmation_token',
        'password_requested_at',
        'roles',
        'credentials_expired',
        'credentials_expired_at',
        'state_id',
        'city',
        'company_name',
        'birth_date',
        'gender',
        'mobile_phonenumber',
        'user_id'
    ];

    public function users()
    {
        return $this->hasOne('App\User');
    }

    public function state()
    {
        return $this->belongsTo('App\State');
    }
}
