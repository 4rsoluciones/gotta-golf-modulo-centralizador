<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackField extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'value', 'pack_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'pack_id'
    ];

    public function pack()
    {
        return $this->belongsTo('App\Pack');
    }
}
