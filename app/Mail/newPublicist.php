<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class newPublicist extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The new created user
     *
     * @var user
     */
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('registro@andresgotta.com.ar', 'Andres Gotta Golf')
            ->subject('Nuevo usuario')
            ->view('mails.newPublicist');
    }
}
