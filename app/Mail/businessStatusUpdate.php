<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class businessStatusUpdate extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The business updated
     *
     * @var user
     */
    public $business;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($business)
    {
        $this->business = $business;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('registro@andresgotta.com.ar', 'Andres Gotta Golf')
        ->subject('Estatus de Establecimiento Actualizado')
        ->view('mails.businessStatusUpdate');
    }
}
