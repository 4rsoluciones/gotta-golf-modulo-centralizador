<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class servicesPurchase extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The new user order
     *
     * @var order
     */
    public $order;
    public $user;
    public $linkAcademia;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order, $user, $linkAcademia)
    {
        $this->order = $order;
        $this->user = $user;
        $this->linkAcademia = $linkAcademia;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('ventas@andresgotta.com.ar', 'Andres Gotta Golf')
            ->subject('Contratación de servicio')
            ->view('mails.servicePurchase');
    }
}
