<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ServicePlatform extends Pivot
{
    public $timestamps = false;

    protected $table = 'services_platforms';

    protected $fillable = [
        'service_id', 'platform_id', 'pack_id', 'active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function pack()
    {
        return $this->belongsTo('App\Pack');
    }
}
