<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'business_name',
        'description',
        'cuit',
        'web',
        'logo',
        'status',
        'business_category_id',
        'user_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'business_category_id', 'user_id',
    ];

    //Relationships
    public function category()
    {
        return $this->belongsTo('App\BusinessCategory', 'business_category_id');
    }

    public function owner()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function near() {
        return $this->hasMany('App\Near');
    }
    public function coupons() {
        return $this->hasMany('App\Coupons');
    }
}
