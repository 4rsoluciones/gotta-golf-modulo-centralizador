<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'long_description',
        'price',
        'highlighted',
        'important',
        'visible',
        'type',
        'days',
        'trial_days',
        'is_lifetime',
        'debitDues',
        'service_type_id',
        'area_id',
        'mp_plan_id',
		'image',
        'parent_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'service_type_id', 'area_id', 'pivot',
    ];

    public function type()
    {
        return $this->belongsTo('App\ServiceType');
    }

    public function area()
    {
        return $this->belongsTo('App\Area');
    }

    public function platforms()
    {
        return $this->belongsToMany('App\Platform', 'services_platforms')
            ->using('App\ServicePlatform')
            ->withPivot(['pack_id','active']);
    }
    public function pack()
    {
        return $this->belongsToMany('App\Pack', 'services_platforms');
    }
    public function orders()
    {
        return $this->belongsToMany('App\Order', 'services_orders');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'users_services')
            ->withPivot('active', 'expired_at', 'mp_subscription_id');
    }

    public function promoCodes()
    {
        return $this->belongsToMany('App\PromoCode', 'promo_codes_services');
    }

    public function getAreas($services) {
        $areas = array();
        if(!empty($services)) {
            foreach($services as $service) {
                if(isset($service->area)) {
                    $areas[$service->area->id] = [
                        'name' => $service->area->name,
                        'description' => $service->area->description
                    ];
                }
            }
        }
        return $areas;
    }
    public function getByAreaPlatfom($idPlatform,$idArea) {
        $servicesIds = DB::table('services_platforms')
                        ->where('platform_id', $idPlatform)
                        ->where('active', 1)
                        ->pluck('service_id')->toArray();

        if(!empty($servicesIds)) {
            $services = $this->whereIn('id', $servicesIds)
                             ->where('area_id', $idArea)
                             ->where('visible', true)
                             ->orderBy('order', 'asc')
                             ->get();
        } else {
            $services = array();
        }

        return $services;
    }
}
