<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromoCodeType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function codes()
    {
        return $this->hasMany('App\PromoCode');
    }
}
