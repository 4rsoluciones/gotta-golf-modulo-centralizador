<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'discount', 'valid_until', 'promo_code_type_id', 'allowed_uses'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'promo_code_type_id'
    ];

    public function type()
    {
        return $this->belongsTo('App\PromoCodeType', 'promo_code_type_id');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function services()
    {
        return $this->belongsToMany('App\Service', 'promo_codes_services');
    }
}
