<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'email',
        'password',
        'username',
        'profile_id',
        'salt'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'profile_id', 'remember_token',
    ];

    public function __construct(array $attributes = [])
    {
        if (empty($attributes['salt'])) {
            $attributes['salt'] = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        }

        parent::__construct($attributes);
    }


    public function platforms()
    {
        return $this->belongsToMany('App\Platform', 'users_platforms');
    }

    public function services()
    {
        return $this->belongsToMany('App\Service', 'users_services')
            ->withPivot('active', 'expired_at', 'mp_subscription_id');
    }

    public function profile()
    {
        return $this->belongsTo('App\Profile');
    }

    public function businesses()
    {
        return $this->hasMany('App\Business');
    }

    public function isPublicist()
    {
        if (in_array('ROLE_PUBLICIST', (array) unserialize($this->profile->roles))) {
            $res = true;
        }

        return isset($res);
    }
}
