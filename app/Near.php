<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Near extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'direction',
        'longitude',
        'latitude',
        'country',
        'state',
        'image',
        'business_id',
        'status',
        'user_id',
    ];

    //Relationships
    public function business()
    {
        return $this->belongsTo('App\Business', 'business_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
