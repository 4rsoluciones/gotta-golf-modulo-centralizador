<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupons extends Model
{

    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'image',
        'stock',
        'status',
        'price',
        'price_old',
        'discount',
        'business_id',
        'date_active',
        'status',
        'user_id',
    ];

    //Relationships
    public function business() {
        return $this->belongsTo('App\Business', 'business_id');
    }
    /**
     * [user description]
     * @return [type] [description]
     */
    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function users() {
        return $this->belongsToMany('App\User');
    }
}
