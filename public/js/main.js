jQuery(document).ready(function($){
    $('#country').change(function(){
        $.get(statesUrl,
            { country: $(this).val() },
            function(data) {
                var model = $('#state');
                model.empty();

                $.each(data, function(index, element) {
                    $('#state').append("<option value='"+ index +"'>" + element + "</option>");
                });
            });
    });

    $('.datepicker').datepicker({
        dateFormat: 'yy-mm-dd'
    });
});