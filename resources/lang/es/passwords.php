<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La contraseña debe ser de por lo menos 6 caracteres y debe coincidir con la confirmación.',
    'reset' => 'Su contraseña ha sido reseteada!',
    'sent' => 'Se le ha enviado un email con un link para resetar su contraseña!',
    'token' => 'El códifo de reseteo de contraseña es inválido.',
    'user' => "No podemos encontrar un usuario con esa dirección de email.",

];
