@extends('layouts.app')

@section('content')
    <div class="container marketing">
        <div class="row">
            @if(Session('error'))
                <div class="row alert alert-danger">
                    <p>{{ Session('error') }}</p>
                </div>
            @endif
            <p>Registrate en nuestra plataforma y contrata el plan que se adapte a tu medida.</p>
            <p>Gestión colectiva y eficiente, conviven en un único ambiente brindándote todos los servicos para mejorar tu empresa.</p>
        </div>
    </div>
    <div class="container marketing">
        <div class="row">
            <h1>Descubre nuestro mundo</h1>
            <hr/>
            <div class="row">
                @foreach ($platforms as $platform)
                    <div class="col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center">
                                <strong>{{ $platform->name }}</strong>
                            </div>
                            <div class="panel-body">
                                <p>
                                    <a class="btn btn-primary btn-block" href="{{ url('platform/'.$platform->id) }}" role="button">Ver Areas »</a>
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
