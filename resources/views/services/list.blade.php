@extends('layouts.app')

@section('content')
    <div class="container marketing">
        @if(isset($response) && !empty($response))
            <div class="row alert alert-{{ (($response['status'] == Config::get('mercadopago.MP_PAYMENT_APPROVED') || $response['status'] == Config::get('mercadopago.MP_PAYMENT_PENDING')) ? 'success' : 'danger') }}">
                <p>{{ $response['message'] }}</p>
            </div>
        @endif
        @if(isset($error) && !empty($error))
            <div class="row alert alert-danger">
                <p>{{ $error }}</p>
            </div>
        @endif
        @if(Session::has('error'))
            <div class="row alert alert-danger">
                <p>{{ Session::get('error') }}</p>
            </div>
        @endif
        <p>
            <a class="btn btn-default btn-sm" href="{{ url('/platform/'.$idPlatform) }}" role="button">&#171; Volver</a>
        </p>
        <div class="row">
            @foreach ($services as $service)
                <div class="col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-heading text-center">
                            <strong>{{ $service->name }}</strong>
                        </div>
                        <div class="panel-body">
                            @if (!empty($service->image))
                                <p class="text-center">
                                    <img src="{{ Storage::url($service->image) }}" class="img-responsive">
                                </p>
                            @endif
                            <p>{{ $service->description }}</p>
                            <p><strong>Precio: </strong>$ {{ $service->price }}</p>
                            <p>
                                <a class="btn btn-primary" href="{{ url('services/'.$service->id) }}" role="button">Ver detalle »</a>
                                @if (Auth::user())
                                    <input
                                            type="button"
                                            id="buy-btn"
                                            class="btn btn-success buy-btn-click"
                                            data-toggle="modal"
                                            data-target="#payment-modal"
                                            data-service="{{ $service->id }}"
                                            data-amount="{{ $service->price }}"
                                            data-platform="{{ $idPlatform }}"
                                            value="Suscribir">
                                @else
                                    <a href="{{ url('login') }}" id="buy-btn-login" class="btn btn-success buy-btn-click"> Suscribir</a>
                                @endif
                            </p>
                        </div>
                    </div>
                </div><!-- /.col-lg-4 -->
            @endforeach
        </div>

    </div>

    <div class="modal fade bs-example-modal-lg" id="payment-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Realizar Pago</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('services/pay') }}" method="post" id="payForm" name="payForm" >
                        {{ csrf_field() }}
                        {{--<div class="form-group">
                        <label for="docType">Tipo de pago:</label>
                        <select class="form-control" name="paymentMethodId" id="paymentMethodId" data-checkout="paymentMethodId">
                        <option value="">Seleccione un medio de pago</option>
                    </select>
                </div>--}}
                <div id="cardFields">
                    <div class="form-group">
                        <label for="cardNumber">Número de tarjeta:</label>
                        <input class="form-control" type="text" id="cardNumber" data-checkout="cardNumber" placeholder="4509 9535 6623 3704" />
                    </div>
                    <div class="form-group col-md-4">
                        <label for="securityCode">Código de seguridad:</label>
                        <input class="form-control" type="text" id="securityCode" data-checkout="securityCode" placeholder="123" />
                    </div>
                    <div class="form-group col-md-4">
                        <label for="cardExpirationMonth">Mes de vencimiento:</label>
                        <input class="form-control" type="text" id="cardExpirationMonth" data-checkout="cardExpirationMonth" placeholder="12" />
                    </div>
                    <div class="form-group col-md-4">
                        <label for="cardExpirationYear">Año de vencimiento:</label>
                        <input class="form-control" type="text" id="cardExpirationYear" data-checkout="cardExpirationYear" placeholder="2015" />
                    </div>
                    <div class="form-group">
                        <label for="cardholderName">Nombre del tarjetabiente:</label>
                        <input class="form-control" type="text" id="cardholderName" data-checkout="cardholderName" placeholder="Pedro Perez" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="docType">Tipo de documento:</label>
                    <select class="form-control" id="docType" data-checkout="docType"></select>
                </div>
                <div class="form-group">
                    <label for="docNumber">Número de documento:</label>
                    <input class="form-control" type="text" id="docNumber" data-checkout="docNumber" placeholder="12345678" />
                </div>
                <div class="form-group">
                    <label for="promoCode">Código promocional:</label>
                    <input class="form-control" type="text" name="promoCode" id="promoCode" placeholder="PROMO50" />
                </div>
                <input type="hidden" id="amount" name="amount" />
                <input type="hidden" id="platform" name="platform" />
            </form>
        </div>
        <div class="modal-footer">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-3">
                    <div class="form-group">
                        <button type="button" class="form-control btn btn-danger" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-3 col-md-offset-6 col-sm-offset-6 col-xs-offset-6">
                    <div class="form-group">
                        <button type="button" id="pay" class="form-control btn btn-success">Pagar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('js')
    <script src="https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js"></script>
    <script>
    $(function () {
        var redirect = '{{ (isset($response['redirect']) ? $response['redirect'] : '') }}';

        if (redirect != '') {
            window.open(redirect, '_blank');
        }

        $(document).on('click', '#buy-btn', function () {
            $('#payForm').attr('action', '{{ url('services/pay') }}' + '/' + $(this).data('service'));

            $('#amount').val($(this).data('amount'));

            $('#platform').val($(this).data('platform'));
        });

        Mercadopago.setPublishableKey('{{ Config::get('mercadopago.public_key') }}');

        var idTypes = Mercadopago.getIdentificationTypes();

        $.each(idTypes, function (id, name) {
            $('#docType').append('<option value="' + id + '">' + name + '</option>');
        });

        {{--$.get('https://api.mercadopago.com/v1/payment_methods?public_key={{ Config::get('mercadopago.public_key') }}&js_version=1.5.4&locale=es&referer={{ url('/') }}')
        .done(function (data) {
        $.each(data, function (index, obj) {
        $('#paymentMethodId').append('<option value="' + obj.id + '" data-payment-type="' + obj.payment_type_id + '">' + obj.name + '</option>');
    })
});
$('#paymentMethodId').on('change', function () {
if ($(this).val() !== '') {
switch ($(this).find(':selected').data('payment-type')) {
case 'credit_card':
case 'debit_card':
$('#cardFields').html('<div class="form-group">\n' +
'                       <label for="cardNumber">Número de tarjeta:</label>\n' +
'                       <input class="form-control" type="text" id="cardNumber" data-checkout="cardNumber" placeholder="4509 9535 6623 3704" />\n' +
'                  </div>\n' +
'                  <div class="form-group col-md-4">\n' +
'                       <label for="securityCode">Código de seguridad:</label>\n' +
'                       <input class="form-control" type="text" id="securityCode" data-checkout="securityCode" placeholder="123" />\n' +
'                  </div>\n' +
'                  <div class="form-group col-md-4">\n' +
'                       <label for="cardExpirationMonth">Mes de vencimiento:</label>\n' +
'                       <input class="form-control" type="text" id="cardExpirationMonth" data-checkout="cardExpirationMonth" placeholder="12" />\n' +
'                  </div>\n' +
'                  <div class="form-group col-md-4">\n' +
'                       <label for="cardExpirationYear">Año de vencimiento:</label>\n' +
'                       <input class="form-control" type="text" id="cardExpirationYear" data-checkout="cardExpirationYear" placeholder="2015" />\n' +
'                  </div>\n' +
'                  <div class="form-group">\n' +
'                       <label for="cardholderName">Nombre del tarjetabiente:</label>\n' +
'                       <input class="form-control" type="text" id="cardholderName" data-checkout="cardholderName" placeholder="Pedro Perez" />\n' +
'                  </div>' +
'                  <div class="form-group">\n' +
'                       <label for="issuer">Banco:</label>\n' +
'                       <select class="form-control" id="issuer" name="issuer" disabled></select>\n' +
'                  </div>' +
'                  <div class="form-group">\n' +
'                       <label for="installments">Número de pagos:</label>\n' +
'                       <select class="form-control" id="installments" name="installments" disabled></select>\n' +
'                  </div>' +
'                  <div class="form-group" id="interests">' +
'                       <strong><span id="cft"></span><span id="tea"><span></strong>' +
'                  </div> ');
break;
default:
$('#cardFields').html('');
break;
}
} else {
$('#cardFields').hide();
}
});
$(document).on('blur','#cardNumber', function () {
Mercadopago.getIssuers($('#paymentMethodId').val(), showCardIssuers);
});

$(document).on('change', '#issuer', function() {
Mercadopago.getInstallments({
"bin": $('#cardNumber').val().replace(/[ .-]/g, '').slice(0, 6),
"amount": $('#amount').val(),
"issuer_id": $('#issuer').val()
}, setInstallmentInfo);
});

$(document).on('change', '#installments', function () {
if ($(this).find(':selected').data('cft') != '' && $(this).find(':selected').data('tea') != '') {
$('#cft').text('CFT: '+$(this).find(':selected').data('cft'));
$('#tea').text('TEA: '+$(this).find(':selected').data('tea'));

$('#interests').show();
} else {
$('#cft').text('');
$('#tea').text('');

$('#interests').hide();
}
});
--}}
$('#pay').on('click', function(event) {
    event.preventDefault();

    Mercadopago.createToken($('#payForm'), sdkResponseHandler);

    {{--switch ($('#paymentMethodId').find(':selected').data('payment-type')) {
    case 'credit_card':
    case 'debit_card':
    Mercadopago.createToken($('#payForm'), sdkResponseHandler);

    return false;
    break;
    default:
    $('#payForm').submit();
    break;
} --}}
});
});

function  sdkResponseHandler(status, response) { console.log('response', response);
if (status != 200 && status != 201) {
    alert('revisar campos');
} else {
    var form = $('#payForm');

    form.append('<input type="hidden" name="token" value="'+ response.id +'" />');

    form.submit();
}
}

function showCardIssuers(status, issuers) {
    $('#issuer').html('');

    $('#issuer').append('<option value="">Seleccione un banco</option>');

    for (var i = 0; i < issuers.length; i++) {
        if (issuers[i].name != "default") {
            $('#issuer').append('<option value="'+ issuers[i].id +'">'+ issuers[i].name +'</option>');
        } else {
            $('#issuer').append('<option value="'+ issuers[i].id +'">Otro</option>');
        }

        $('#issuer').prop('disabled', false);
    }
}

function setInstallmentInfo(status, response) {
    $('#installments').html('');

    if (response.length > 0) {
        var payerCosts = response[0].payer_costs;
        var cft = tea = '';

        for (var i = 0; i < payerCosts.length; i++) {
            if (payerCosts[i].installments > 1) {
                var interests = payerCosts[i].labels;

                $.each(interests, function (index, data) {
                    if (data.indexOf('CFT_') !== -1) {
                        interests = data.split('|');
                    }
                });

                cft = interests[0].replace('CFT_', '');
                tea = interests[1].replace('TEA_', '');
            }

            $('#installments').append('<option value="'+ payerCosts[i].installments +'" data-cft="'+cft+'" data-tea="'+tea+'">'+ payerCosts[i].recommended_message +'</option>');
        }
    } else {
        $('#installments').append('<option value="1">1 cuota de $ '+ $('#amount').val() +' ($ '+ $('#amount').val() +')</option>');
    }

    $('#installments').prop('disabled', false);
}
</script>
@endsection
