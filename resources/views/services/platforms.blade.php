@extends('layouts.app')

@section('content')
    <div class="container marketing">
        <p>
            <a class="btn btn-default btn-sm" href="{{ url('/') }}" role="button">&#171; Volver</a>
        </p>
        <div class="row">
            @if(Session('error'))
                <div class="row alert alert-danger">
                    <p>{{ Session('error') }}</p>
                </div>
            @endif
            <h2>{{$platfom->name}} - Areas</h2>
        </div>
    </div>
    <div class="container marketing">
        <div class="row">
            @foreach ($areas as $idArea => $area)
                <div class="col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-heading text-center">
                            <strong>{{ $area['name'] }}</strong>
                        </div>
                        <div class="panel-body">
                            <div>
                                <p>
                                    {{ $area['description'] }}
                                </p>
                            </div>
                            <p>
                                <a class="btn btn-primary btn-block" href="{{ url('platform/'.$platfom->id.'/area/'.$idArea) }}" role="button">Ver Servicios »</a>
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
