@extends('publicist.dashboard')

@section('main')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <h1 class="page-header">Nuevo Establecimiento</h1>
        <div class="jumbotron">
            <h1>Importante!</h1>
            <p>Te informamos que al registrar tu establecimiento, pasará a estado de evaluación y solamente si es
            aprobado podrás publicitarlo dentro de los productos de <strong><a href="www.andresgotta.com.ar">www.andresgotta.com.ar</a></strong></p>

            <p>La aprobación de tus establecimientos va a depender de la veracidad de los datos ingresados y de si es un tipo
            de establecimiento aceptado para anunciarse dentro de la plataforma.</p>
        </div>
        <form method="post" id="newUser" action="{{ url('anunciantes/establecimientos/registro') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">Nombre del negocio</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}" required/>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('business_name') ? ' has-error' : '' }}">
                        <label for="business_name">Razon Social</label>
                        <input type="text" name="business_name" id="business_name" class="form-control" value="{{ old('business_name') }}" />
                        @if ($errors->has('business_name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('business_name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        <label for="description">Descripción</label>
                        <input type="text" name="description" id="description" class="form-control" value="{{ old('description') }}" required/>
                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('cuit') ? ' has-error' : '' }}">
                        <label for="cuit">Cuit</label>
                        <input type="text" name="cuit" id="cuit" class="form-control" value="{{ old('cuit') }}" required/>
                        @if ($errors->has('cuit'))
                            <span class="help-block">
                                <strong>{{ $errors->first('cuit') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('web') ? ' has-error' : '' }}">
                        <label for="web">Página web</label>
                        <input type="text" name="web" id="web" class="form-control" value="{{ old('web') }}" />
                        @if ($errors->has('web'))
                            <span class="help-block">
                                <strong>{{ $errors->first('web') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('business_category_id') ? ' has-error' : '' }}">
                        <label for="business_category_id">Categoría</label>
                        <select class="form-control" name="business_category_id" id="business_category_id">
                            <option value="">-- Seleccione una categoría --</option>
                            @if(count($categories))
                                @foreach($categories as $id => $category)
                                    <option value="{{ $id }}" {{ $id == old('business_category_id') ? 'selected' : '' }}>{{ $category }}</option>
                                @endforeach
                            @endif
                        </select>
                        @if ($errors->has('business_category_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('business_category_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                        <label for="logo">Logo</label>
                        <input type="file" name="logo" id="logo" />
                        <p class="help-block">Logotipo de la empresa</p>
                        @if ($errors->has('logo'))
                            <span class="help-block">
                                <strong>{{ $errors->first('logo') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="button" class="form-control btn btn-flat btn-danger" value="Cancelar" onclick="location.href ='{{ url('anunciantes/establecimientos') }}'">
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-6">
                    <div class="form-group">
                        <input type="submit" class="form-control btn btn-flat btn-success" value="Guardar" />
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection