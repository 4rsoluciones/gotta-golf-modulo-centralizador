@extends('publicist.dashboard')

@section('main')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <h1 class="page-header">Establecimientos</h1>
        <div class="col-md-3 col-md-offset-9">
            <div class="form-group">
                <input type="button" class="form-control btn btn-flat btn-success" value="Nuevo Establecimiento" onclick="location.href ='{{ url('anunciantes/establecimientos/registro') }}'">
            </div>
        </div>
        @if (session('success'))
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success">{{ session('success') }}</div>
                </div>
            </div>
        @endif
        @if (session('error'))
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger">{{ session('error') }}</div>
                </div>
            </div>
        @endif
        @if(count($businesses))
            <div class="dataTables_wrapper">
                <table id="business" class="table table-bordered table-hover dataTable">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Razon Social</th>
                        <th>Cuit</th>
                        <th>Categoría</th>
                        <th>Estatus</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($businesses as $business)
                        @php
                            switch($business->status) {
                                case config('constants.businessStatus.APPROVED'):
                                        $class = 'label-success';
                                    break;

                                case config('constants.businessStatus.DECLINED'):
                                case config('constants.businessStatus.CANCELLED'):
                                        $class = 'label-danger';
                                    break;

                                default:
                                        $class = 'label-info';
                                    break;
                            }
                        @endphp
                        <tr>
                            <td>{{ $business->name }}</td>
                            <td>{{ $business->business_name ? $business->business_name : '-' }}</td>
                            <td>{{ $business->cuit }}</td>
                            <td>{{ $business->category->name }}</td>
                            <td><span class="label {{ $class }}">{{ $business->status }}</span></td>
                            <td class="text-center" style="width: 1%;">
                                <div class="btn-group">
                                    <button
                                            type="button"
                                            class="btn btn-default btn-flat"
                                            onclick="location.href ='{{ url('anunciantes/establecimientos/editar/'.$business->id) }}'"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Editar"
                                            {{ $business->status == config('constants.businessStatus.DECLINED') ? 'disabled' : '' }}
                                    >
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="row">
                <p>No posee establecimientos registrados.</p>
            </div>
        @endif
    </div>
@endsection