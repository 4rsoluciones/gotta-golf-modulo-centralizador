@extends('publicist.dashboard')

@section('main')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <h1 class="page-header">Dashboard</h1>

        <div class="row placeholders">
            <div class="col-xs-12 col-sm-4 placeholder">
                <a href="{{ url('anunciantes/establecimientos') }}">
                    <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
                </a>
                <h4>Establecimientos</h4>
                <span class="text-muted">Administrá tus datos</span>
            </div>
            <div class="col-xs-12 col-sm-4 placeholder">
                <a href="{{ url('anunciantes/cerca-mio') }}">
                    <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
                </a>
                <h4>Cerca mío</h4>
                <span class="text-muted">Datos de ubicación</span>
            </div>
            <div class="col-xs-12 col-sm-4 placeholder">
                <a href="{{ url('anunciantes/beneficios') }}">
                    <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
                </a>
                <h4>Beneficio</h4>
                <span class="text-muted">Administra tus beneficios</span>
            </div>
        </div>
    </div>
@endsection
