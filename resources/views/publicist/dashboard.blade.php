@extends('layouts.app')

@section('css')
    <link href="https://getbootstrap.com/docs/3.3/examples/dashboard/dashboard.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/font-awesome/css/font-awesome.min.css') }}">
    <!-- DataTables -->
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="/css/multi-select.dist.css">
    <link rel="stylesheet" href="/css/custom.css">
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <li class="{{ Request::is('anunciantes') ? 'active' : '' }}"><a href="{{ url('anunciantes') }}">Resumen</a></li>
                    <li class="{{ strpos(Request::path(), 'establecimientos') ? 'active' : '' }}"><a href="{{ url('anunciantes/establecimientos') }}">Establecimientos</a></li>
                    <li class="{{ strpos(Request::path(), 'cerca-mio') ? 'active' : '' }}"><a href="{{ url('anunciantes/cerca-mio') }}">Cerca mío</a></li>
                    <li class="{{ strpos(Request::path(), 'beneficios') ? 'active' : '' }}"><a href="{{ url('anunciantes/beneficios') }}">Beneficios</a></li>
                </ul>
            </div>
            @yield('main')
        </div>
    </div>
@endsection

@section('js')
    <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>

    <script>
        $(function () {
            $('.dataTable').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : false,
                'language': {
                    'search' : 'Buscar',
                    'emptyTable': 'No se encontraron registros.',
                    'zeroRecords': 'No se encontraron registros que concuerden con la busqueda',
                    'paginate': {
                        'first': 'Primero',
                        'last': 'Último',
                        'next': 'Siguiente',
                        'previous': 'Anterior',
                    }
                }
            });
        })
    </script>
@endsection
