@extends('publicist.dashboard')

@section('main')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <h4>Editar Cupón:</h4>
        {{ Form::model($coupon, ['method' => 'post', 'files' => true , 'action' => ['Publicist\BenefitsController@update',$coupon->id]]) }}
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group {{$errors->first('name', ' has-error')}}">
                        <label>Nombre</label>
                        {{ Form::text('name', null,['class' => 'form-control','required']) }}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group {{$errors->first('stock', ' has-error')}}">
                        <label>Stock:</label>
                        {{ Form::text('stock', null,['class' => 'form-control','required'])}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group {{$errors->first('image', ' has-error')}}">
                        <label>Imagen:</label>
                        {{ Form::file('image', ['class'=>'form-control']) }}
                        @php if(!empty($coupon->image)): @endphp
                        <div class="mt10">
                            <img src="{{ Storage::url($coupon->image) }}" class="m-w-200 img-responsive" />
                        </div>
                        @php endif; @endphp
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group {{$errors->first('discount', ' has-error')}}">
                        <label >Descuento:(%)</label>
                        {{ Form::text('discount', null,['class' => 'form-control','placeholder'=>'20%'])}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group {{$errors->first('price_old', ' has-error')}}">
                        <label>Precio:</label>
                        {{ Form::text('price_old', null,['class' => 'form-control','placeholder'=>'$1000']) }}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group {{$errors->first('price', ' has-error')}}">
                        <label>Nuevo Precio:</label>
                        {{ Form::text('price', null,['class' => 'form-control','placeholder'=>'$1000'])}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group {{$errors->first('date_active', ' has-error')}}">
                        <label>Fecha de vencimiento:</label>
                        {{ Form::text('date_active', null,['class' => 'form-control','id'=>'date_active','required']) }}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group {{$errors->first('status', ' has-error')}}">
                        <label>Estado:</label>
                        {{ Form::select('status', [0=>'Despublicado',1=>'Publicado'], null , ['class'=>'form-control','required']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group {{$errors->first('description', ' has-error')}}">
                        <label>Descripción:</label>
                        {{ Form::textarea('description', null,['class' => 'form-control']) }}
                    </div>
                </div>
            </div>
            <div class="text-right">
                <button class="btn btn-primary">Guardar</button>
            </div>
            {{ Form::hidden('businesses_id', $coupon->businesses_id) }}
        {{ Form::close() }}
    </div>
@endsection

@section('js')
    <script>
    $( function() {
        $("#date_active").datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });
    </script>
@endsection
