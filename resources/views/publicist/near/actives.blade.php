@extends('publicist.dashboard')

@section('main')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <p>
            <a href="/anunciantes/cerca-mio" role="button" class="btn btn-default btn-sm">« Volver</a>
        </p>
        <h4>Agregar establecimiento:</h4>
        <hr/>
        @php if(!empty($nears)): @endphp
        {{ Form::open(array('url' => '/anunciantes/cerca-mio/activos/'.$id, 'method' => 'post')) }}
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group {{$errors->first('name', ' has-error')}}">
                        <select id='pre-selected-options' name="nearActive[]" multiple='multiple'>
                            @php foreach($nears as $near): @endphp
                            <option {{ (($near->status)?'selected="selected"':'')}} value='{{ $near->id }}'>{{$near->name}}</option>
                            @php endforeach; @endphp
                        </select>
                    </div>
                </div>
            </div>
            <div class="text-right">
                <button class="btn btn-primary">Guardar</button>
            </div>
        {{ Form::close() }}
        @php endif; @endphp
    </div>
@endsection
@section('js')
    <script src="/js/jquery.multi-select.js"></script>
    <script type="text/javascript">
        // run pre selected options
        var selectedCount = 0;
        $('#pre-selected-options').multiSelect(
            {
                keepOrder: true,
                afterSelect: function(values){
                    checkCount();
                },
                afterDeselect: function(values){
                    checkCount();
                }
            }
        );
        function checkCount() {
            selectedCount = $('#pre-selected-options option:selected').length;
            if(selectedCount < {{$limitSucursales}}) {
                $('#pre-selected-options option').prop('disabled',false);
            } else {
                $('#pre-selected-options option').prop('disabled',true);
                $('#pre-selected-options option:selected').prop('disabled',false);
            }
            $('#pre-selected-options').multiSelect('refresh');
        }
        checkCount();
    </script>
@endsection
