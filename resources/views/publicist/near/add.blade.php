@extends('publicist.dashboard')

@section('main')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <h2>Establecimento: {{ $business['name'] }} </h2>
        <hr/>
        <h4>Agregar Sucursal:</h4>
        {{ Form::open(array('url' => '/anunciantes/cerca-mio/nuevo/'.$business['id'],'files' => true ,'id' => 'formNear', 'method' => 'post','onsubmit="return checkForm();"')) }}
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group {{$errors->first('name', ' has-error')}}">
                        <label for="exampleInputEmail1">Nombre</label>
                        {{ Form::text('name', null,['class' => 'form-control','required']) }}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group {{$errors->first('name', ' has-error')}}">
                        <label for="exampleInputEmail1">Dirección:</label>
                        {{ Form::text('direction', null,['class' => 'form-control','required','id'=>'direction'])}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="country">País</label>
                        {{ Form::select('country', $countries, null , ['class'=>'form-control','required','id'=>'country_id']) }}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="state_id">Provincia</label>
                        {{ Form::select('state', $states, null , ['class'=>'form-control','required','id'=>'state_id']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group {{$errors->first('name', ' has-error')}}">
                        <label for="exampleInputEmail1">Imagen:</label>
                        {{ Form::file('image', ['class'=>'form-control']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group {{$errors->first('name', ' has-error')}}">
                        <label for="exampleInputEmail1">Descripción:</label>
                        {{ Form::textarea('description', null,['class' => 'form-control','required']) }}
                    </div>
                </div>
            </div>
            <div class="text-right">
                <button class="btn btn-primary">Guardar</button>
            </div>
            {{ Form::hidden('businesses_id', $business['id']) }}
            {{ Form::hidden('longitude', null,['id'=>'longitude']) }}
            {{ Form::hidden('latitude', null,['id'=>'latitude']) }}
        {{ Form::close() }}
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAAkMcQRH9gn4LhemH5ISNv7uGSh-Y9tWI"></script>
    <script type="text/javascript">
        var latitude = '';
        var longitude = '';
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('direction'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                latitude = place.geometry.location.lat();
                longitude = place.geometry.location.lng();

                $('#longitude').val(longitude);
                $('#latitude').val(latitude);
            });
        });
        function checkForm() {
            $("#formNear").addClass('disabledForm');
            if(!isNumeric($('#longitude').val()) || !isNumeric($('#latitude').val())) {
                alert('La dirección ingresada es incorrecta.');
                $("#formNear").removeClass('disabledForm');
                return false;

            }
            return true;
        }
        function isNumeric(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }

        $('#country_id').change(function(){
            $.get('{{ url('states') }}',
                { country: $(this).val() },
                function(data) {
                    var model = $('#state_id');
                    model.empty();
                    $('#state_id').append("<option>-- Seleccione una provincia --</option>");
                    $.each(data, function(index, element) {
                        $('#state_id').append("<option value='"+ index +"'>" + element + "</option>");
                    });
                });
        });
    </script>
@endsection
