@extends('publicist.dashboard')

@section('main')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <h1 class="page-header">Cerca Mío</h1>
        @if(Session('response'))
            <div class="row alert alert-{{ ((Session('response')['status'] == Config::get('mercadopago.MP_PAYMENT_APPROVED') || Session('response')['status'] == Config::get('mercadopago.MP_PAYMENT_PENDING')) ? 'success' : 'danger') }}">
                <p>{{ Session('response')['message'] }}</p>
            </div>
        @endif
        @if(Session::has('error'))
            <div class="row alert alert-danger">
                <p>{{ Session::get('error') }}</p>
            </div>
        @endif
        @if(!count(Auth::user()->businesses()->whereStatus(Config::get('constants.businessStatus.APPROVED'))->first()))
            <div class="jumbotron">
                <h1>No posee establecimientos regitrados!</h1>
                <p>Debe registrar un establecimniento y esperar que sea validado para poder subscribirse a los servicios de <strong>Cerca mío</strong></p>
                <p><a class="btn btn-primary btn-lg" href="{{ url('/anunciantes/establecimientos') }}" role="button">Establecimientos »</a></p>
            </div>
        @else
            @if(!count($services))
                <div class="jumbotron">
                    <h1>No posee el servicio Cerca mío activo!</h1>
                    <p>Para poder anunciar tu establecimiento dentro de los productos de <strong><a href="www.andresgotta.com.ar">www.andresgotta.com.ar</a></strong>
                    en la sección <strong>Cerca mío</strong> debe primero suscribirse a alguna de las siguientes opciones</p>
                </div>
                <div class="container">
                    @if(count($nearToMeServices))
                        <div class="row">
                            @foreach($nearToMeServices as $nearService)
                                <div class="col-md-4">
                                    <h2>{{ $nearService->name }}</h2>
                                    <p>{{ $nearService->description }}</p>
                                    <p>{{ $nearService->long_description }}</p>
                                    <p><strong>Precio: </strong>$ {{ $nearService->price }}</p>
                                    <p>
                                        <input
                                                type="button"
                                                id="buy-btn"
                                                class="btn btn-success"
                                                data-toggle="modal"
                                                data-target="#payment-modal"
                                                data-service="{{ $nearService->id }}"
                                                data-amount="{{ $nearService->price }}"
                                                data-platform="{{ $nearService->platforms()->first()->id }}"
                                                value="Suscribir">
                                    </p>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            @else

                <p>
                    <b>Establecimientos:</b> {{count($businesss)}} / {{$limitBusiness}}<br/>
                    <b>Sucursales por establecimiento:</b> {{$limitSucursales}}
                </p>
                @php if($limitBusiness > count($businesss)): @endphp
                <p class="text-right">
                    <a href="{{ url('anunciantes/cerca-mio/agregar') }}" class="btn btn-primary" >
                        <i class="fa fa-plus"></i> Agregar Establecimiento
                    </a>
                </p>
                @php endif; @endphp
                @php if(!empty($businesss)): @endphp
                <div class="table-responsive">
                    <table class="table table-striped table-condensed table-bordered">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php foreach($businesss as $business): @endphp
                            <tr>
                                <td>
                                    <b>{{$business->name}}</b><br/><br/>
                                    <h5>Sucursales: {{count($business->near)}}/{{$limitSucursales}}</h5>
                                    @php if(!empty($business->near)): @endphp
                                    <table class="table table-striped table-condensed table-bordered">
                                        <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Dirección</th>
                                                    <th class="text-center">Activo</th>
                                                    <th>Acciones</th>
                                                </tr>
                                        </thead>
                                        <tbody>
                                            @php foreach($business->near as $near): @endphp
                                                <tr>
                                                    <td> {{$near->name}}</td>
                                                    <td> {{$near->direction}}</td>
                                                    <td class="text-center"> {{ (($near->status)?'Si':'No')}}</td>
                                                    <td>
                                                        <a href="{{ url('anunciantes/cerca-mio/editar/'.$near->id) }}"><i class="fa fa-pencil"></i> Editar</a><br />
                                                        <a data-toggle="confirmation"
                                                        data-btn-ok-label="Si" data-btn-ok-class="btn-success"
                                                        data-btn-ok-icon-class="fa fa-check" data-btn-ok-icon-content="as"
                                                        data-btn-cancel-label="No" data-btn-cancel-class="btn-danger"
                                                        data-btn-cancel-icon-class="as" data-btn-cancel-icon-content="as"
                                                        data-title="Eliminar sucursal" data-content="¿Esta seguro que desea eliminar la sucursal?" href="{{ url('anunciantes/cerca-mio/eliminar/'.$near->id) }}"><i class="fa fa-close"></i> Eliminar</a><br />
                                                    </td>
                                                </tr>
                                            @php endforeach; @endphp
                                        </tbody>
                                    </table>
                                    @php endif; @endphp
                                </td>
                                <td style="width:1px;">
                                    <a class="btn btn-primary btn-sm" href="{{ url('anunciantes/cerca-mio/activos/'.$business->id) }}"><i class="fa fa-edit"></i> Activar sucursales</a><br /><br />
                                    @php if($limitSucursales > count($business->near)): @endphp
                                    <a class="btn btn-primary btn-sm" href="{{ url('anunciantes/cerca-mio/nuevo/'.$business->id) }}"><i class="fa fa-plus"></i> Agregar sucursal</a>
                                    @php endif; @endphp
                                </td>
                            </tr>
                            @php endforeach; @endphp
                        </tbody>
                    </table>
                </div>
                @php else: @endphp
                No hay establecimientos creados.
                @php endif; @endphp

            @endif
        @endif
    </div>

    <div class="modal fade bs-example-modal-lg" id="payment-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Realizar Pago</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('services/pay') }}" method="post" id="payForm" name="payForm" >
                        {{ csrf_field() }}
                        <div id="cardFields">
                            <div class="form-group">
                                <label for="cardNumber">Número de tarjeta:</label>
                                <input class="form-control" type="text" id="cardNumber" data-checkout="cardNumber" placeholder="4509 9535 6623 3704" />
                            </div>
                            <div class="form-group col-md-4">
                                <label for="securityCode">Código de seguridad:</label>
                                <input class="form-control" type="text" id="securityCode" data-checkout="securityCode" placeholder="123" />
                            </div>
                            <div class="form-group col-md-4">
                                <label for="cardExpirationMonth">Mes de vencimiento:</label>
                                <input class="form-control" type="text" id="cardExpirationMonth" data-checkout="cardExpirationMonth" placeholder="12" />
                            </div>
                            <div class="form-group col-md-4">
                                <label for="cardExpirationYear">Año de vencimiento:</label>
                                <input class="form-control" type="text" id="cardExpirationYear" data-checkout="cardExpirationYear" placeholder="2015" />
                            </div>
                            <div class="form-group">
                                <label for="cardholderName">Nombre del tarjetabiente:</label>
                                <input class="form-control" type="text" id="cardholderName" data-checkout="cardholderName" placeholder="Pedro Perez" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="docType">Tipo de documento:</label>
                            <select class="form-control" id="docType" data-checkout="docType"></select>
                        </div>
                        <div class="form-group">
                            <label for="docNumber">Número de documento:</label>
                            <input class="form-control" type="text" id="docNumber" data-checkout="docNumber" placeholder="12345678" />
                        </div>
                        <div class="form-group">
                            <label for="promoCode">Código promocional:</label>
                            <input class="form-control" type="text" name="promoCode" id="promoCode" placeholder="PROMO50" />
                        </div>
                        <input type="hidden" id="amount" name="amount" />
                        <input type="hidden" id="platform" name="platform" />
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <div class="form-group">
                                <button type="button" class="form-control btn btn-danger" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3 col-md-offset-6 col-sm-offset-6 col-xs-offset-6">
                            <div class="form-group">
                                <button type="button" id="pay" class="form-control btn btn-success">Pagar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet" type="text/css">
@endsection

@section('js')
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-confirmation2@2.4.1/bootstrap-confirmation.min.js"></script>
    <script src="https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js"></script>
    <script src="https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js"></script>
    <script>
        $(function() {
            $(document).on('click', '#buy-btn', function () {
                $('#payForm').attr('action', '{{ url('services/pay') }}' + '/' + $(this).data('service'));

                $('#amount').val($(this).data('amount'));

                $('#platform').val($(this).data('platform'));
            });

            Mercadopago.setPublishableKey('{{ Config::get('mercadopago.public_key') }}');

            var idTypes = Mercadopago.getIdentificationTypes();

            $.each(idTypes, function (id, name) {
                $('#docType').append('<option value="' + id + '">' + name + '</option>');
            });

            $('#pay').on('click', function(event) {
                event.preventDefault();

                Mercadopago.createToken($('#payForm'), sdkResponseHandler);
            });

            function  sdkResponseHandler(status, response) { console.log('response', response);
                if (status != 200 && status != 201) {
                    alert('revisar campos');
                } else {
                    var form = $('#payForm');
                    form.append('<input type="hidden" name="token" value="'+ response.id +'" />');
                    form.submit();
                }
            }
        });
        $(document).ready(function() {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
            });
        });
    </script>
@endsection
