@extends('publicist.dashboard')

@section('main')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <p>
            <a href="/anunciantes/cerca-mio" role="button" class="btn btn-default btn-sm">« Volver</a>
        </p>
        <h4>Agregar establecimiento:</h4>
        <hr/>
        {{ Form::open(array('url' => '/anunciantes/cerca-mio/agregar', 'method' => 'post')) }}
            {{ csrf_field() }}
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group {{$errors->first('name', ' has-error')}}">
                        <label for="exampleInputEmail1">Establecimiento:</label>
                        {{ Form::select('business_id', $businesses, null , ['class'=>'form-control','required']) }}
                    </div>
                </div>
            </div>
            <div class="text-right">
                <button class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
            </div>
        {{ Form::close() }}
    </div>
@endsection
