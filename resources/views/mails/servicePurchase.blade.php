<!doctype html><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head><title></title><!--[if !mso]><!-- --><meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]--><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta name="viewport" content="width=device-width,initial-scale=1"><style type="text/css">#outlook a{padding:0}.ReadMsgBody{width:100%}.ExternalClass{width:100%}.ExternalClass *{line-height:100%}body{margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}table,td{border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0}img{border:0;height:auto;line-height:100%;outline:0;text-decoration:none;-ms-interpolation-mode:bicubic}p{display:block;margin:13px 0}</style><!--[if !mso]><!--><style type="text/css">@media only screen and (max-width:480px){@-ms-viewport{width:320px}@viewport{width:320px}}</style><!--<![endif]--><!--[if mso]>
<xml>
  <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
  </o:OfficeDocumentSettings>
</xml>
<![endif]--><!--[if lte mso 11]>
<style type="text/css">
  .outlook-group-fix {
    width:100% !important;
  }
</style>
<![endif]--><!--[if !mso]><!--><link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css"><style type="text/css">@import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);</style><!--<![endif]--><style type="text/css">.boton:hover{opacity:.75}@media all and (min-width:601px){.contenido .boton{font-size:24px}}@media all and (max-width:600px){.contenido .boton{font-size:4vmin}}@media all and (max-width:480px){.contenido .boton{font-size:4.8vmin}}</style><style type="text/css">@media only screen and (min-width:480px){.mj-column-per-100{width:100%!important}}</style></head><body style="background:#eee"><div class="mj-container" style="background-color:#eee"><!--[if mso | IE]>
      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;" class="header-outlook">
      <![endif]--><div style="background-color:#fff;padding:0;text-align:center;margin:0 auto;max-width:600px;background:#fff" class="header"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0;width:100%;background:#fff" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0;padding:0"><!--[if mso | IE]>
      <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td style="vertical-align:top;width:600px;" class="img-header-outlook">
      <![endif]--><div class="mj-column-per-100 outlook-group-fix img-header" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%"><table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0"><tbody><tr><td style="word-wrap:break-word;font-size:0;padding:0;width:100%" align="center" width="100%"><table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0" align="center" border="0"><tbody><tr><td style="padding:0;width:100%" width="100%"><img alt="" height="auto" src="http://php71.projectsunderdev.com/gotta-golf-modulo-centralizador/public/img/mails/header.jpg" style="border:none;border-radius:0;display:block;font-size:13px;outline:0;text-decoration:none;width:100%;height:auto" width="550"></td></tr></tbody></table></td></tr></tbody></table></div><!--[if mso | IE]>
      </td></tr></table>
      <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
      </td></tr></table>
      <![endif]--><!--[if mso | IE]>
      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;" class="contenido-outlook">
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      <![endif]--><div style="background-color:#fff;padding:0;text-align:center;color:#000;margin:0 auto;max-width:600px" class="contenido"><table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0;width:100%" align="center" border="0"><tbody><tr><td style="text-align:center;vertical-align:top;direction:ltr;font-size:0;padding:20px 0"><!--[if mso | IE]>
      <table role="presentation" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td style="vertical-align:top;width:600px;">
      <![endif]-->
      <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%;font-family:'Source Sans Pro'">
        <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
          <tbody>
            <tr>
              <td style="word-wrap:break-word;font-size:0;padding:10px 25px" align="center">
                <div style="cursor:auto;color:#000;font-size:30px;font-weight:700;line-height:1.2;text-align:center;font-family:'Source Sans Pro'">
                  <p style="font-family:'Source Sans Pro'">Confirmación de contratación exitosa</p>
                </div>
              </td>
            </tr>
            <tr>
              <td style="word-wrap:break-word;font-size:0;padding:10px 25px" align="center">
                <div style="cursor:auto;color:#000;font-size:18px;font-weight:regular;line-height:1.5;text-align:center;font-family:'Source Sans Pro'">
                  <p style="font-family:'Source Sans Pro'">
                    Hola <span style="font-weight:700">{{ $user->profile->first_name }} {{ $user->profile->last_name }}</span>, 
                    tu contratación de servicios en Andres Gotta Golf, se ha realizado con éxito.
                  </p>
                  <p style="font-family:'Source Sans Pro'">
                    Servicio contratrado: <span style="font-weight:700">{{ $order->services[0]->name }}</span><br>
                    Monto: <span style="font-weight:700">$ {{ $order->services[0]->price }}</span><br>
                    El monto se debitará de tu cuenta todos los meses.
                  </p>
                  <p>
                    En tu perfil podrás ver todos tus servicios contratados <br>
                    <a href="{{ url('/profile/edit/'.$user->id) }}" title="Apple" class="boton" style="border-radius: 3px;background: #FFF;box-shadow: 0px 9px 32px 0px rgba(0, 0, 0, 0.26);font-size: 18px;font-weight: 500;color: #666666;margin: 0.5rem;padding: 0.7rem 1.6rem;line-height: 5.8;position: relative;text-decoration: none;">
                      Ver perfil
                    </a>
                  </p>
                </div>
                @if ($linkAcademia)
                  <div style="cursor:auto;color:#000;font-size:18px;font-weight:regular;line-height:1.5;text-align:center;font-family:'Source Sans Pro'">
                    <p style="font-family:'Source Sans Pro'">
                      Para poder gestionar tu academia, ingresa al siguiente link con tu usuario y contraseña
                    </p>
                    <p>
                      <a href="https://php56.projectsunderdev.com/gotta-golf-site/web/academia" title="Apple" class="boton" style="border-radius: 3px;background: #FFF;box-shadow: 0px 9px 32px 0px rgba(0, 0, 0, 0.26);font-size: 18px;font-weight: 500;color: #666666;margin: 0.5rem;padding: 0.7rem 1.6rem;line-height: 5.8;position: relative;text-decoration: none;">
                        Gestionar academia
                      </a>
                    </p>
                  </div>
                @endif
                
              </td>
            </tr>
              
            </tbody></table></div><!--[if mso | IE]>
      </td></tr></table>
      <![endif]--></td></tr></tbody></table></div><!--[if mso | IE]>
      </td></tr></table>
      <![endif]--><!--[if mso | IE]>
      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;" class="footer-outlook">
        <tr>
          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
      </td></tr></table>
      <![endif]--></div></body></html>