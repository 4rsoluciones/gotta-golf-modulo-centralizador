<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.min.css" rel="stylesheet">
    @yield('css')
</head>
<body style="padding-top: 50px;">
    <div id="app" style="padding-top: 10px">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <!-- {{ config('app.name', 'Laravel') }} -->
                        <img src="{{ url('/') }}/img/logo.png" alt="{{ config('app.name', 'Laravel') }}" />
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Entrar</a></li>
                            <li><a href="{{ route('register') }}">Registro</a></li>
                            <li><a href="{{ route('publicist') }}">Anunciantes</a></li>
                        @else
                            @if(Auth::user()->isPublicist())
                                <li><a href="{{ url('anunciantes') }}">Dashboard Anunciante</a></li>
                            @endif
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->username }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ url('profile/edit/'.Auth::user()->id) }}">Perfil</a>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Salir
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script>
        var statesUrl = '{{ url("/states") }}';
    </script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"
            integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="
            crossorigin="anonymous">
    </script>
    <script>
        var platform = '{{ session()->has('platform') ? session('platform') : '' }}';
        var redirectTo = '{{ session()->has('redirectTo') ? session('redirectTo') : '' }}';

        $(function () {
            if (platform.length && redirectTo.length) { console.log('obtuvo el valor');
                sessionStorage.setItem('platform', platform);
                sessionStorage.setItem('redirectTo', redirectTo);
            }

            if (sessionStorage.getItem('platform') && sessionStorage.getItem('redirectTo')) { console.log('leyo los valores');
                $('#backBtn').prop('href', sessionStorage.getItem('redirectTo'));
                $('#backBtn').text('Volver a '+sessionStorage.getItem('platform'));

                $('#backBtn').show();
            } else {
                $('#backBtn').hide();
            }

            $(document).on('click', '#backBtn', function (e) {
                e.preventDefault();

                sessionStorage.clear();

                location = $('#backBtn').prop('href');
            })
        })
    </script>
    @yield('js')
</body>
</html>
