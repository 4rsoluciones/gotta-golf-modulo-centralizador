<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="{{ asset('css/front.css') }}" rel="stylesheet">

    @yield('css')
</head>
<body>
<div class="flex-center position-ref" style="padding-top: 60px;">
    @if (Route::has('login'))
        <div class="top-right links">
            <a href="#" id="backBtn" style="display: none"></a>
            @auth
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
                    Salir
                </a>
                <a href="{{ route('profile') }}">Perfil</a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            @else
                <a href="{{ route('login') }}">Entrar</a>
                <a href="{{ route('register') }}">Registro</a>
            @endauth
        </div>
    @endif

    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    @yield('content')
</div>
    <script>
        var platform = '{{ session()->has('platform') ? session('platform') : '' }}';
        var redirectTo = '{{ session()->has('redirectTo') ? session('redirectTo') : '' }}';

        $(function () {
            if (platform.length && redirectTo.length) { console.log('obtuvo el valor');
                sessionStorage.setItem('platform', platform);
                sessionStorage.setItem('redirectTo', redirectTo);
            }

            if (sessionStorage.getItem('platform') && sessionStorage.getItem('redirectTo')) { console.log('leyo los valores');
                $('#backBtn').prop('href', sessionStorage.getItem('redirectTo'));
                $('#backBtn').text('Volver a '+sessionStorage.getItem('platform'));

                $('#backBtn').show();
            } else {
                $('#backBtn').hide();
            }

            $(document).on('click', '#backBtn', function (e) {
                e.preventDefault();

                sessionStorage.clear();

                location = $('#backBtn').prop('href');
            })
        })
    </script>
    @yield('js')
</body>
</html>