@extends('adminlte::page')

@section('content_header')
    <h1>Anunciantes <small>Establecimientos - Detalle</small></h1>
@stop

@section('content')
    <div class="col-md-12 main">
        <h1 class="page-header">Datos del Establecimiento</h1>
        <form method="post" id="newUser" action="{{ url('/admin/publicists/places/'.$business->id) }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Nombre del negocio</label>
                        <span name="name" id="name" class="form-control disabled">{{ $business->name }}</span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="business_name">Razon Social</label>
                        <span name="business_name" id="business_name" class="form-control disabled">{{ $business->business_name }}</span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="description">Descripción</label>
                        <span name="description" id="description" class="form-control disabled">{{ $business->description }}</span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="cuit">Cuit</label>
                        <input  name="cuit" id="cuit" value="{{ $business->cuit }}" class="form-control" />
                        <!-- <span name="cuit" id="cuit" class="form-control">{{ $business->cuit }}</span> -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="web">Página web</label>
                        <span name="web" id="web" class="form-control disabled">{{ $business->web }}</span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="business_category">Categoría</label>
                        <span name="business_category" id="business_category" class="form-control disabled">{{ $business->category->name }}</span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label for="business_category_id">Estatus</label>
                        <select class="form-control" name="status" id="status">
                            <option value="">-- Seleccione una estatus --</option>
                            @if(count($statuses))
                                @foreach($statuses as $id => $status)
                                    <option value="{{ $id }}" {{ ($status == old('status') || $status == $business->status) ? 'selected' : '' }}>{{ $status }}</option>
                                @endforeach
                            @endif
                        </select>
                        @if ($errors->has('status'))
                            <span class="help-block">
                                <strong>{{ $errors->first('status') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="logo">Logo</label>
                        <img class="img-responsive" src="{{ Storage::url($business->logo) }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="button" class="form-control btn btn-flat btn-info" value="Volver" onclick="location.href ='{{ url('/admin/publicists/'.$business->owner->id) }}'">
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-6">
                    <div class="form-group">
                        <input type="submit" class="form-control btn btn-flat btn-success" value="Guardar" />
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('css')

    <link rel="stylesheet" href="/css/admin_custom.css">

@stop
