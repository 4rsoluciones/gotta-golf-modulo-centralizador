@extends('adminlte::page')

@section('content_header')
    <h1>Anunciantes <small>Detalle</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if (session('success'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success">{{ session('success') }}</div>
                    </div>
                </div>
            @endif
            @if (session('error'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger">{{ session('error') }}</div>
                    </div>
                </div>
            @endif
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Datos</a></li>
                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="true">Establecimientos</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Nombre</label>
                                <span id="name" class="form-control" >{{ $user->profile->first_name }}</span>
                            </div>
                            <div class="form-group">
                                <label for="description">Apellido</label>
                                <span id="description" class="form-control" >{{ $user->profile->last_name }}</span>
                            </div>
                            <div class="form-group">
                                <label for="long_description">Nombre de usuario</label>
                                <span id="long_description" class="form-control" >{{ $user->username }}</span>
                            </div>
                            <div class="form-group">
                                <label for="price">Email</label>
                                <span id="price" class="form-control" >{{ $user->email }}</span>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                        <div class="box-body">
                            @if(count($user->businesses))
                                <div class="dataTables_wrapper">
                                    <table id="business" class="table table-bordered table-hover dataTable">
                                        <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Razon Social</th>
                                            <th>Cuit</th>
                                            <th>Categoría</th>
                                            <th>Estatus</th>
                                            <th>Acciones</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($user->businesses as $business)
                                            @php
                                                switch($business->status) {
                                                    case config('constants.businessStatus.APPROVED'):
                                                            $class = 'bg-green';
                                                        break;

                                                    case config('constants.businessStatus.DECLINED'):
                                                    case config('constants.businessStatus.CANCELLED'):
                                                            $class = 'bg-red';
                                                        break;

                                                    default:
                                                            $class = 'bg-aqua';
                                                        break;
                                                }
                                            @endphp
                                            <tr>
                                                <td>{{ $business->name }}</td>
                                                <td>{{ $business->business_name ? $business->business_name : '-' }}</td>
                                                <td>{{ $business->cuit }}</td>
                                                <td>{{ $business->category->name }}</td>
                                                <td><span class="label {{ $class }}">{{ $business->status }}</span></td>
                                                <td class="text-center" style="width: 1%;">
                                                    <div class="btn-group">
                                                        <button
                                                                type="button"
                                                                class="btn btn-default btn-flat"
                                                                onclick="location.href ='{{ url('/admin/publicists/places/'.$business->id) }}'"
                                                                data-toggle="tooltip"
                                                                data-placement="top"
                                                                title="Ver"
                                                        >
                                                            <i class="fa fa-search"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @else
                                <div class="row">
                                    <p>No posee establecimientos registrados.</p>
                                </div>
                            @endif
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="button" class="form-control btn btn-flat btn-info" value="Volver" onclick="location.href ='{{ url('admin/services') }}'">
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('#orders').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : false,
                'language': {
                    'search' : 'Buscar',
                    'emptyTable': 'No se encontraron registros.',
                    'zeroRecords': 'No se encontraron registros que concuerden con la busqueda',
                    'paginate': {
                        'first': 'Primero',
                        'last': 'Último',
                        'next': 'Siguiente',
                        'previous': 'Anterior',
                    }
                }
            });
        });
    </script>
@stop