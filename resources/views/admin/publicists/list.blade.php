@extends('adminlte::page')

@section('content_header')
    <h1>Anunciantes <small>Listado</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if (session('success'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success">{{ session('success') }}</div>
                    </div>
                </div>
            @endif
            @if (session('error'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger">{{ session('error') }}</div>
                    </div>
                </div>
            @endif
            <div class="dataTables_wrapper">
                @if (isset($users) && !empty($users))
                    <table id="services" class="table table-bordered table-hover dataTable">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Nombre de usuario</th>
                            <th>Email</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->profile->first_name ? $user->profile->first_name : 'N/A' }}</td>
                                <td>{{ $user->profile->last_name ? $user->profile->last_name : 'N/A' }}</td>
                                <td>{{ $user->username }}</td>
                                <td>{{ $user->email }}</td>
                                <td class="text-center" style="width: 1%;">
                                    <div class="btn-group">
                                        <button
                                                type="button"
                                                class="btn btn-default btn-flat"
                                                onclick="location.href ='{{ url('/admin/publicists/'.$user->id) }}'"
                                                data-toggle="tooltip"
                                                data-placement="top"
                                                title="Ver"
                                        >
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <h2>No hay anunciantes registrados.</h2>
                @endif
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('#services').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : false,
                'language': {
                    'search' : 'Buscar',
                    'emptyTable': 'No se encontraron registros.',
                    'zeroRecords': 'No se encontraron registros que concuerden con la busqueda',
                    'paginate': {
                        'first': 'Primero',
                        'last': 'Último',
                        'next': 'Siguiente',
                        'previous': 'Anterior',
                    }
                }
            })
        })

    </script>
@stop