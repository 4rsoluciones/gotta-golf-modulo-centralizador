@extends('adminlte::page')

@section('content_header')
    <h1>Usuarios <small>Detalle</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Datos Básicos</a></li>
                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Perfil</a></li>
                    <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Plataformas</a></li>
                    {{--@if (in_array('ROLE_ACADEMY', unserialize($user->profile->roles)) ||
                         in_array('ROLE_STUDENT', unserialize($user->profile->roles)) ||
                         in_array('ROLE_TEACHER', unserialize($user->profile->roles))
                         )
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                Configuración <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                @if (in_array('ROLE_ACADEMY', unserialize($user->profile->roles)))
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#" onclick="location.href ='{{ url(Config::get('constants.gpStatsUrl').'/admin/academy/'.$user->email.'/places') }}'">Sedes</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#" onclick="location.href ='{{ url(Config::get('constants.gpStatsUrl').'/admin/academy/'.$user->email.'/teachers-and-students') }}'">Profesores y Alumnos</a></li>
                                @endif
                                @if (in_array('ROLE_STUDENT', unserialize($user->profile->roles)))
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#" onclick="location.href ='{{ url(Config::get('constants.gpStatsUrl').'/admin/students/'.$user->email.'/academies') }}'">Academias Alumno</a></li>
                                @endif
                                @if (in_array('ROLE_TEACHER', unserialize($user->profile->roles)))
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#" onclick="location.href ='{{ url(Config::get('constants.gpStatsUrl').'/admin/teachers/'.$user->email.'/academies') }}'">Academias Profesor</a></li>
                                @endif
                            </ul>
                        </li>
                    @endif--}}
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="username">Nombre de ususario</label>
                                <span id="username" class="form-control" >{{ $user->username }}</span>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <span id="email" class="form-control" >{{ $user->email }}</span>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="first_name">Nombre</label>
                                <span id="first_name" class="form-control" >{{ $user->profile->first_name }}</span>
                            </div>
                            <div class="form-group">
                                <label for="last_name">Apellido</label>
                                <span id="last_name" class="form-control" >{{ $user->profile->last_name }}</span>
                            </div>
                            <div class="form-group">
                                <label for="last_login">Último Login</label>
                                <span id="last_login" class="form-control" >{{ $user->profile->last_login }}</span>
                            </div>
                            <div class="form-group">
                                <label for="country">País</label>
                                <span id="country" class="form-control" >{{ $state ? $state->country->name : '' }}</span>
                            </div>
                            <div class="form-group">
                                <label for="state">Provincia</label>
                                <span id="state" class="form-control" >{{ $state ? $state->name : '' }}</span>
                            </div>
                            <div class="form-group">
                                <label for="city">Localidad</label>
                                <span id="city" class="form-control" >{{ $user->profile->city }}</span>
                            </div>
                            <div class="form-group">
                                <label for="company_name">Razón Social</label>
                                <span id="company_name" class="form-control" >{{ $user->profile->company_name }}</span>
                            </div>
                            <div class="form-group">
                                <label for="birth_date">Fecha de Nacimiento</label>
                                <span id="birth_date" class="form-control" >{{ $user->profile->birth_date }}</span>
                            </div>
                            <div class="form-group">
                                <label for="gender">Sexo</label>
                                <span id="gender" class="form-control" >{{ $user->profile->gender }}</span>
                            </div>
                            <div class="form-group">
                                <label for="mobile_phonenumber">Celular</label>
                                <span id="mobile_phonenumber" class="form-control" >{{ $user->profile->mobile_phonenumber }}</span>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_3">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="platform">Plataformas Registradas</label>
                                <span id="platform" class="form-control" >
                                    @foreach($user->platforms as $plarform)
                                        <span class="label bg-aqua">{{ $plarform->name }}</span>
                                    @endforeach
                                </span>
                            </div>
                            <div class="form-group">
                                <label for="roles">Roles</label>
                                <span id="roles" class="form-control" >
                                    @foreach($roles as $role)
                                        <span class="label bg-aqua">{{ $role }}</span>
                                    @endforeach
                                </span>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="button" class="form-control btn btn-flat btn-info" value="Volver" onclick="location.href ='{{ url('admin/users') }}'">
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-6">
                    <div class="form-group">
                        <input type="button" class="form-control btn btn-flat btn-success" value="Editar" onclick="location.href ='{{ url('admin/users/edit/'.$user->id) }}'">
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop