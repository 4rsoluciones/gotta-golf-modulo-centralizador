@extends('adminlte::page')

@section('content_header')
    <h1>Usuarios <small>Editar</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form method="post" id="editUser" action="{{ url('admin/users/edit/'.$user->id) }}">
                {{ csrf_field() }}
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li id="li-tab1" class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Datos Básicos</a></li>
                        <li id="li-tab2" class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Perfil</a></li>
                        <li id="li-tab2" class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Plataformas</a></li>
                        {{--@if (in_array('ROLE_ACADEMY', unserialize($user->profile->roles)) ||
                         in_array('ROLE_STUDENT', unserialize($user->profile->roles)) ||
                         in_array('ROLE_TEACHER', unserialize($user->profile->roles))
                         )
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                    Configuración <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    @if (in_array('ROLE_ACADEMY', unserialize($user->profile->roles)))
                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#" onclick="location.href ='{{ url(Config::get('constants.gpStatsUrl').'/admin/academy/'.$user->email.'/places') }}'">Sedes</a></li>
                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#" onclick="location.href ='{{ url(Config::get('constants.gpStatsUrl').'/admin/academy/'.$user->email.'/teachers-and-students') }}'">Profesores y Alumnos</a></li>
                                    @endif
                                    @if (in_array('ROLE_STUDENT', unserialize($user->profile->roles)))
                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#" onclick="location.href ='{{ url(Config::get('constants.gpStatsUrl').'/admin/students/'.$user->email.'/academies') }}'">Academias Alumno</a></li>
                                    @endif
                                    @if (in_array('ROLE_TEACHER', unserialize($user->profile->roles)))
                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#" onclick="location.href ='{{ url(Config::get('constants.gpStatsUrl').'/admin/teachers/'.$user->email.'/academies') }}'">Academias Profesor</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif--}}
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="username">Nombre de ususario</label>
                                    <input type="text" name="username" id="username" class="form-control" value="{{ $user->username }}" />
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" id="email" class="form-control" value="{{ $user->email }}" />
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="first_name">Nombre</label>
                                    <input type="text" name="first_name" id="first_name" class="form-control" value="{{ $user->profile->first_name }}" />
                                </div>
                                <div class="form-group">
                                    <label for="last_name">Apellido</label>
                                    <input type="text" name="last_name" id="last_name" class="form-control" value="{{ $user->profile->last_name }}" />
                                </div>
                                <div class="form-group">
                                    <label for="country">País</label>
                                    <select name="country" id="country" class="form-control">
                                        <option>-- Seleccione una país --</option>
                                        @foreach($countries as $k => $country)
                                            <option value="{{ $k }}"  @if(count($state) && $state->country->name == $country) selected @endif>{{ $country }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="state">Provincia</label>
                                    <select class="form-control" id="state" name="state_id">
                                        <option>-- Seleccione una provincia --</option>
                                        @if(count($state))
                                            @foreach($states as $st)
                                                <option value="{{ $st->id }}" @if($st->id == $state->id) selected @endif>{{ $st->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="city">Localidad</label>
                                    <input type="text" name="city" id="city" class="form-control" value="{{ $user->profile->city }}" />
                                </div>
                                <div class="form-group">
                                    <label for="company_name">Razón Social</label>
                                    <input type="text" name="company_name" id="company_name" class="form-control" value="{{ $user->profile->company_name }}" />
                                </div>
                                <div class="form-group">
                                    <label for="birth_date">Fecha de Nacimiento</label>
                                    <input name="birth_date" id="birth_date" type="text" class="form-control datepicker" name="birth_date" value="{{ $user->profile->birth_date }}" />
                                </div>
                                <div class="form-group">
                                    <label for="gender">Sexo</label>
                                    <select class="form-control" id="gender" name="gender">
                                        <option>-- Seleccione un sexo --</option>
                                        @foreach($genders as $id => $gender)
                                            <option value="{{ $id }}" @if($user->profile->gender == $id) selected @endif>{{ $gender }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="mobile_phonenumber">Celular</label>
                                    <input type="text" name="mobile_phonenumber" id="mobile_phonenumber" class="form-control" value="{{ $user->profile->mobile_phonenumber }}" />
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_3">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="platform">Plataformas</label>
                                    <select name="platforms[]" id="platforms" class="form-control select2" multiple autocomplete="off">
                                        @foreach($platforms as $id => $platform)
                                            <option value="{{ $id }}" @if(in_array($id, $user->platforms->pluck('id')->toArray())) selected @endif>{{ $platform }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="roles">Roles</label>
                                    <select name="roles[]" id="roles" class="form-control select2" multiple autocomplete="off">
                                        @foreach($allRoles as $k => $role)
                                            <option value="{{ $k }}" @if(in_array($role, $roles)) selected @endif>{{ $role }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->

                    </div>
                    <!-- /.tab-content -->
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="button" class="form-control btn btn-flat btn-danger" value="Cancelar" onclick="location.href ='{{ url('admin/users') }}'">
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-6">
                        <div class="form-group">
                            <input type="button" id="editBtn" class="form-control btn btn-flat btn-success" value="Guardar y continuar">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('js')
    <script>
        var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
        var eventer = window[eventMethod];
        var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

        // Listen to message from child window
        eventer(messageEvent,function(e) {
            var key = e.message ? "message" : "data";
            var data = e[key];
            console.log(data);

            if (data === 'formCompleted') {
                sessionStorage.setItem('success', 'Usuario editado correctamente');

                location.href = '{{ url("admin/users") }}';
            }
        },false);

        $(function () {
            setIframe();

            $('.select2').select2({
                width: "100%"
            });

            $('#country').change(function(){
                $.get('{{ url('states') }}',
                    { country: $(this).val() },
                    function(data) {
                        var model = $('#state');
                        model.empty();
                        $('#state').append("<option>-- Seleccione una provincia --</option>");
                        $.each(data, function(index, element) {
                            $('#state').append("<option value='"+ index +"'>" + element + "</option>");
                        });
                    });
            });

            $('.datepicker').datepicker({
                dateFormat: 'yy-mm-dd'
            });

            $('#platforms').change(function () {
                setRoles($('#platforms').val());
            });

            $('#editBtn').on('click', function () {
                $.post('{{ url("admin/users/edit/".$user->id) }}',
                    $( "#editUser" ).serialize(),
                    function (data) {
                        if (data == "success") {
                            $('#editBtn').hide();

                            $('#tab_1').removeClass('active');
                            $('#li-tab1').removeClass('active');
                            $('#tab_2').removeClass('active');
                            $('#li-tab2').removeClass('active');
                            $('#tab_3').addClass('active');
                            $('#li-tab3').addClass('active');

                            $('#tab_3 .box-body').html('<iframe name="platforms-data" src="{{ Config::get("constants.gpStatsUrl") }}/admin/user/user@user.com/edit" style="width: 100%; height: 400px;"></iframe>');
                        } else {
                            $('.help-block').remove();

                            $.each(data, function (field, message) {
                                var brace = '';

                                if (field === 'platforms' || field === 'roles') {
                                    brace ='[]';
                                }

                                $('[name="'+ field + brace+'"]').after('<span class="help-block"><strong>'+ message +'</strong></span>').parent().addClass('has-error');
                            });

                            $('#tab_2').removeClass('active');
                            $('#li-tab2').removeClass('active');
                            $('#tab_3').removeClass('active');
                            $('#li-tab3').removeClass('active');
                            $('#tab_1').addClass('active');
                            $('#li-tab1').addClass('active');
                        }
                    }
                );
            });
        });

        function setIframe() {
            $.get('{{ url(Config::get("constants.gpStatsUrl")."/admin/login_check") }}',
                { username: '{{ Auth::user()->email }}',
                    password: '{{ session('pw') }}'
                },
                function(data) {
                    alert('test '+data);
                }
            );
        }

        function setRoles(platforms) {
            var oldRoles = '{{ old("roles") }}';

            $.get('{{ url('roles') }}',
                { platforms: platforms },
                function (data) {
                    var model = $('#roles');
                    var selected = '';

                    model.empty();

                    $.each(data, function (index, element) {
                        selected = ($.inArray(index, oldRoles) !== -1 ? 'selected' : '');
                        $('#roles').append("<option value='"+ index +"' "+ selected +">" + element + "</option>");
                    });

                    $('#roles').select2({
                        width: "100%"
                    });
                }
            );
        }
    </script>
@stop