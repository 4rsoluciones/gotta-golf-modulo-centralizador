@extends('adminlte::page')

@section('content_header')
    <h1>Usuarios <small>Nuevo</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form method="post" id="newUser" action="{{ url('admin/users/new') }}">
                {{ csrf_field() }}
                <div id="rootwizard" class="tabbable tabs-left nav-tabs-custom">
                    <ul>
                        <li id="li-tab1"><a href="#tab1" data-toggle="tab">Datos Básicos</a></li>
                        <li id="li-tab2"><a href="#tab2" data-toggle="tab">Perfil</a></li>
                        <li id="li-tab3"><a href="#tab3" data-toggle="tab">Plataformas</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane" id="tab1">
                            <div class="box-body">
                                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                    <label for="username">Nombre de ususario</label>
                                    <input type="text" name="username" id="username" class="form-control" value="{{ old('username') }}" onkeyup="checkFields();" required/>
                                    @if ($errors->has('username'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" id="email" class="form-control" value="{{ old('email') }}" onkeyup="checkFields();" required/>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                {{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password">Contraseña</label>
                                    <input type="password" name="password" id="password" class="form-control" value="" required/>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password-confirm">Repita Contraseña</label>
                                    <input type="password" name="password_confirmation" id="password-confirm" class="form-control" value="" required/>
                                </div>--}}
                            </div>
                        </div>
                        <div class="tab-pane" id="tab2">
                            <div class="box-body">
                                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                    <label for="first_name">Nombre</label>
                                    <input type="text" name="first_name" id="first_name" class="form-control" value="{{ old('first_name') }}" />
                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    <label for="last_name">Apellido</label>
                                    <input type="text" name="last_name" id="last_name" class="form-control" value="{{ old('last_name') }}" />
                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                    <label for="country">País</label>
                                    <select name="country" id="country" class="form-control">
                                        <option value="">-- Seleccione una país --</option>
                                        @foreach($countries as $k => $country)
                                            <option value="{{ $k }}" {{ old('country') == $k ? "selected" : "" }}>{{ $country }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('country'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('country') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                                    <label for="state">Provincia</label>
                                    <select class="form-control" id="state" name="state_id">
                                        <option value="">-- Seleccione una provincia --</option>
                                    </select>
                                    @if ($errors->has('state'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('state') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                    <label for="city">Localidad</label>
                                    <input type="text" name="city" id="city" class="form-control" value="{{ old('city') }}"/>
                                    @if ($errors->has('city'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('city') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
                                    <label for="company_name">Razón Social</label>
                                    <input type="text" name="company_name" id="company_name" class="form-control" value="{{ old('company_name') }}" />
                                    @if ($errors->has('company_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('company_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('birth_date') ? ' has-error' : '' }}">
                                    <label for="birth_date">Fecha de Nacimiento</label>
                                    <input name="birth_date" id="birth_date" type="text" class="form-control datepicker" name="birth_date" value="{{ old('birth_date') }}"/>
                                    @if ($errors->has('birth_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('birth_date') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                    <label for="gender">Sexo</label>
                                    <select class="form-control" id="gender" name="gender">
                                        <option value="">-- Seleccione un sexo --</option>
                                        @foreach($genders as $id => $gender)
                                            <option value="{{ $id }}" {{ old('gender') == $id ? "selected" : "" }}>{{ $gender }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('gender'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('gender') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('mobile_phonenumber') ? ' has-error' : '' }}">
                                    <label for="mobile_phonenumber">Celular</label>
                                    <input type="text" name="mobile_phonenumber" id="mobile_phonenumber" class="form-control" value="{{ old('mobile_phonenumber') }}" />
                                    @if ($errors->has('mobile_phonenumber'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('mobile_phonenumber') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab3">
                            <div class="box-body">
                                <div class="form-group{{ $errors->has('platforms') ? ' has-error' : '' }}">
                                    <label for="platform">Plataformas</label>
                                    <select name="platforms[]" id="platforms" class="form-control select2" multiple autocomplete="off">
                                        @foreach($platforms as $id => $platform)
                                            <option value="{{ $id }}" {{ collect(old('platforms'))->contains($id) ? "selected" : "" }}>{{ $platform }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('platforms'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('platforms') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('roles') ? ' has-error' : '' }}">
                                    <label for="roles">Roles</label>
                                    <select name="roles[]" id="roles" class="form-control select2" multiple autocomplete="off">
                                    </select>
                                    @if ($errors->has('roles'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('roles') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <ul class="pager wizard">
                                <div class="col-md-3">
                                    <li class="previous"><a href="#" class="form-control btn btn-flat">Anterior</a></li>
                                </div>
                                <div class="col-md-3 col-md-offset-6">
                                    <li class="next"><a href="#" class="form-control btn btn-flat">Siguiente</a></li>
                                    <li class="finish" style="display: none;">
                                        <input type="button" id="saveBtn" class="form-control btn btn-flat btn-success" value="Guardar y continuar" />
                                    </li>
                                </div>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="button" class="form-control btn btn-flat btn-danger" value="Cancelar" onclick="location.href ='{{ url('admin/users') }}'">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('js')
    <script>
        var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
        var eventer = window[eventMethod];
        var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

        // Listen to message from child window
        eventer(messageEvent,function(e) {
            var key = e.message ? "message" : "data";
            var data = e[key];
            console.log(data);

            if (data === 'formCompleted') {
                sessionStorage.setItem('success', 'Usuario creado correctamente');

                location.href = '{{ url("admin/users") }}';
            }
        },false);

        function checkFields() {
            if($('#email').val() && $('#username').val()){
                $('.next').hide();

                $('.finish').show();
            } else {
                $('.finish').hide();

                $('.next').show();
            }
        }

        $(function () {
            setIframe();

            $('.select2').select2({
                width: "100%"
            });

            if ($('#country').val() !== '') {
                setState($('#country'));
            }

            $('#country').change(function(){
                setState($(this));
            });

            $('#platforms').change(function () {
                setRoles($('#platforms').val());
            });

            $('.datepicker').datepicker({
                dateFormat: 'yy-mm-dd'
            });

            $('#rootwizard').formValidation({
                fields: {
                    username: {
                        validators: {
                            required: {
                                message: 'El nombre de usuario es requerido'
                            }
                        }
                    }
                }
            }).bootstrapWizard({
                'tabClass': 'nav nav-tabs',
                onNext: function(tab, navigation, index) {
                    var numTabs    = $('#installationForm').find('.tab-pane').length,
                        isValidTab = validateTab(index - 1);

                    if (!isValidTab) {
                        return false;
                    }
                },
                onTabShow: function(tab, navigation, index) {
                    var $total = navigation.find('li').length;
                    var $current = index+1;

                    if ($total >= $current && $('#email').val() && $('#username').val()) {
                        $('.next').hide();

                        $('.finish').show();
                    } else {
                        $('.finish').hide();

                        $('.next').show();
                    }
                }
            });

            $('#email').on('blur', function () {
                if($(this).val() && $('#username').val()){
                    $('.next').hide();

                    $('.finish').show();
                } else {
                    $('.finish').hide();

                    $('.next').show();
                }
            });

            $('#username').on('blur', function () {
                if($(this).val() && $('#email').val()){
                    $('.next').hide();

                    $('.finish').show();
                } else {
                    $('.finish').hide();

                    $('.next').show();
                }
            });

            $('#saveBtn').on('click', function () {
                $.post('{{ url("admin/users/new") }}',
                    $( "#newUser" ).serialize(),
                    function (data) {
                        if (data.status == "success") {
                            $('#saveBtn').hide();

                            $('#tab2').removeClass('active');
                            $('#li-tab2').removeClass('active');
                            $('#tab1').removeClass('active');
                            $('#li-tab1').removeClass('active');
                            $('#tab3').addClass('active');
                            $('#li-tab3').addClass('active');

                            if (!data.onlyPlayer) {
                                $('#tab3 .box-body').html('<iframe name="platforms-data" src="{{ Config::get("constants.gpStatsUrl") }}/admin/user/'+data.email+'/edit" style="width: 100%; height: 400px;"></iframe>');
                            } else {
                                sessionStorage.setItem('success', 'Usuario creado correctamente');

                                location.href = '{{ url("admin/users") }}';
                            }
                        } else {
                            var tab;

                            $('.help-block').remove();

                            $.each(data.messages, function (field, message) {
                                var brace = '';

                                tab = (field === 'username' || field === 'email') ? 'tab1' : 'tab2';

                                if (field === 'platforms' || field === 'roles') {
                                    brace ='[]';

                                    tab = tab == '' ? 'tab3' : tab;
                                }

                                $('[name="'+ field + brace+'"]').after('<span class="help-block"><strong>'+ message +'</strong></span>').parent().addClass('has-error');
                            });

                            $('#tab2').removeClass('active');
                            $('#li-tab2').removeClass('active');
                            $('#tab3').removeClass('active');
                            $('#li-tab3').removeClass('active');
                            $('#'+tab).addClass('active');
                            $('#li-'+tab).addClass('active');
                        }
                    }
                );
            });
        });

        function setIframe() {
            $.get('{{ url(Config::get("constants.gpStatsUrl")."/admin/login_check") }}',
                { username: '{{ Auth::user()->email }}',
                    password: '{{ session('pw') }}'
                },
                function(data) {
                    alert('test '+data);
                }
            );
        }

        function setState(country) {
            var oldState = '{{ old("state_id") }}';

            $.get('{{ url('states') }}',
                { country: country.val() },
                function(data) {
                    var model = $('#state');
                    var selected = '';

                    model.empty();
                    $('#state').append("<option>-- Seleccione una provincia --</option>");
                    $.each(data, function(index, element) {
                        selected = (oldState === index ? 'selected' : '');
                        $('#state').append("<option value='"+ index +"' "+ selected +">" + element + "</option>");
                    });
                });
        }

        function setRoles(platforms) {
            var oldRoles = '{{ old("roles") }}';

            $.get('{{ url('roles') }}',
                { platforms: platforms },
                function (data) {
                    var model = $('#roles');
                    var selected = '';

                    model.empty();

                    $.each(data, function (index, element) {
                        selected = ($.inArray(index, oldRoles) !== -1 ? 'selected' : '');
                        $('#roles').append("<option value='"+ index +"' "+ selected +">" + element + "</option>");
                    });

                    $('#roles').select2({
                        width: "100%"
                    });
                }
            );
        }

        function validateTab(index) {
            var fv   = $('#rootwizard').data('formValidation'), // FormValidation instance
                // The current tab
                $tab = $('#rootwizard').find('.tab-pane').eq(index);

            // Validate the container
            fv.validateContainer($tab);

            var isValidStep = fv.isValidContainer($tab);
            if (isValidStep === false || isValidStep === null) {
                // Do not jump to the target tab
                return false;
            }

            return true;
        }
    </script>
@stop