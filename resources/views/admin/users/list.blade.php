@extends('adminlte::page')

@section('content_header')
    <h1>Usuarios <small>Listado</small></h1>
@stop

@section('content')
    <div class="row">
        <div id="userList" class="col-md-12">
            @if (session('success'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success">{{ session('success') }}</div>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-3 col-md-offset-9">
                    <div class="form-group">
                        <input type="button" class="form-control btn btn-flat btn-success" value="Nuevo Usuario" onclick="location.href ='{{ url('admin/users/new') }}'">
                    </div>
                </div>
            </div>
            <div class="dataTables_wrapper">
                @if (isset($users) && !empty($users))
                    <table id="users" class="table table-bordered table-hover dataTable">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Nombre de usuario</th>
                                <th>Email</th>
                                <th>Roles</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->profile->first_name ? $user->profile->first_name : 'N/A' }}</td>
                                    <td>{{ $user->profile->last_name ? $user->profile->last_name : 'N/A' }}</td>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        @foreach( $user->roles as $role)
                                            <span class="label bg-aqua">{{ $role }}</span>
                                        @endforeach
                                    </td>
                                    <td class="text-center" style="width: 1%;">
                                        <div class="btn-group" style="display: flex;">
                                            <button
                                                    type="button"
                                                    class="btn btn-default btn-flat"
                                                    onclick="location.href ='{{ url('admin/users/'.$user->id) }}'"
                                                    data-toggle="tooltip"
                                                    data-placement="top"
                                                    title="Ver"
                                            >
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <button
                                                    type="button"
                                                    class="btn btn-default btn-flat"
                                                    onclick="location.href ='{{ url('admin/users/edit/'.$user->id) }}'"
                                                    data-toggle="tooltip"
                                                    data-placement="top"
                                                    title="Editar"
                                            >
                                                <i class="fa fa-edit"></i>
                                            </button>
                                            {{--<button--}}
                                                    {{--type="button"--}}
                                                    {{--class="btn btn-default btn-flat"--}}
                                                    {{--onclick="location.href ='{{ url('admin/users/delete/'.$user->id) }}'"--}}
                                                    {{--data-toggle="tooltip"--}}
                                                    {{--data-placement="top"--}}
                                                    {{--title="Eliminar"--}}
                                            {{-->--}}
                                                {{--<i class="fa fa-trash"></i>--}}
                                            {{--</button>--}}
                                            {{--@if (in_array('ROLE_ACADEMY', unserialize($user->profile->roles)))
                                                <button
                                                        type="button"
                                                        class="btn btn-default btn-flat"
                                                        onclick="location.href ='{{ url(Config::get('constants.gpStatsUrl').'/admin/academy/'.$user->email.'/places') }}'"
                                                        data-toggle="tooltip"
                                                        data-placement="top"
                                                        title="Sedes"
                                                >
                                                    <i class="fa fa-building-o"></i>
                                                </button>
                                                <button
                                                        type="button"
                                                        class="btn btn-default btn-flat"
                                                        onclick="location.href ='{{ url(Config::get('constants.gpStatsUrl').'/admin/academy/'.$user->email.'/teachers-and-students') }}'"
                                                        data-toggle="tooltip"
                                                        data-placement="top"
                                                        title="Profesores y Alumnos"
                                                >
                                                    <i class="fa fa-graduation-cap"></i>
                                                </button>
                                            @endif
                                            @if (in_array('ROLE_STUDENT', unserialize($user->profile->roles)))
                                                <button
                                                        type="button"
                                                        class="btn btn-default btn-flat"
                                                        onclick="location.href ='{{ url(Config::get('constants.gpStatsUrl').'/admin/students/'.$user->email.'/academies') }}'"
                                                        data-toggle="tooltip"
                                                        data-placement="top"
                                                        title="Academias Alumno"
                                                >
                                                    <i class="fa fa-university"></i>
                                                </button>
                                            @endif
                                            @if (in_array('ROLE_TEACHER', unserialize($user->profile->roles)))
                                                <button
                                                        type="button"
                                                        class="btn btn-default btn-flat"
                                                        onclick="location.href ='{{ url(Config::get('constants.gpStatsUrl').'/admin/teachers/'.$user->email.'/academies') }}'"
                                                        data-toggle="tooltip"
                                                        data-placement="top"
                                                        title="Academias Profesor">
                                                    <i class="fa fa-university"></i>
                                                </button>
                                            @endif--}}
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @else
                    <h2>No hay usuarios registrados.</h2>
                @endif
            </div>
        </div>
    </div>
    <img src="{{ url(Config::get('constants.gpStatsUrl').'/admin/login_check').'?username='.Auth::user()->email.'&password='.session('pw') }}" style="display: none;" >
@stop

@section('js')
    <script>
        $(function () {
            //setIframe();

            if (sessionStorage.getItem('success')) {
                $('#userList').prepend('<div class="row"><div class="col-md-12"><div class="alert alert-success">'+ sessionStorage.getItem('success') +'</div></div></div>');
                sessionStorage.removeItem('success');
            }

            $('[data-toggle="tooltip"]').tooltip();

            $('#users').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : false,
                'language': {
                    'search' : 'Buscar',
                    'emptyTable': 'No se encontraron registros.',
                    'zeroRecords': 'No se encontraron registros que concuerden con la busqueda',
                    'paginate': {
                        'first': 'Primero',
                        'last': 'Último',
                        'next': 'Siguiente',
                        'previous': 'Anterior',
                    }
                }
            });
        });

        function setIframe() {
            $.get('{{ url(Config::get("constants.gpStatsUrl")."/admin/login_check") }}',
                { username: '{{ Auth::user()->email }}',
                    password: '{{ session('pw') }}'
                },
                function(data) {

                }
            );
        }
    </script>
@stop