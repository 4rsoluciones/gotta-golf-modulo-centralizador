@extends('adminlte::page')

@section('content_header')
    <h1>Usuarios <small>Importar</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if (session('success'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success">{{ session('success') }}</div>
                    </div>
                </div>
            @endif
            @if (session('error'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger">{{ session('error') }}</div>
                    </div>
                </div>
            @endif
            <form method="post" id="editUser" action="{{ route('users.import.excel') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box box-default">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="platform">Plataforma</label>
                                <select name="platform" id="platform" class="form-control select2" autocomplete="off">
                                    <option value="" disabled selected>Seleccione una plataforma...</option>
                                    @foreach($platforms as $id => $platform)
                                        <option value="{{ $id }}">{{ $platform }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="sewrvice">Servicio</label>
                                <select name="service" id="service" class="form-control select2" autocomplete="off">
                                    <option value="" disabled selected>Seleccione un servicio...</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="sewrvice">Roles</label>
                                <select name="roles[]" id="roles" class="form-control select2" multiple autocomplete="off">
                                    <option value="" disabled>Seleccione al menos un rol...</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="file">Subir archivo de Excel</label>
                                <input type="file", name="file" class="form-control" accept=".xls, .xlsx">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="button" class="form-control btn btn-flat btn-info" value="Cancelar" onclick="location.href ='{{ url('admin/users') }}'">
                        </div>
                    </div>
                    <div class="col-md-3 pull-right">
                        <div class="form-group">
                            <input type="submit" class="form-control btn btn-flat btn-success" value="Importar">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(function () {
            $('.select2').select2({
                width: "100%"
            });

            $('#platform').change(function () {
                setServices($(this).val());
                setRoles($(this).val());
            });

            function setServices(platform) {
                $.get('{{ route('services.list') }}',
                    { platform: platform },
                    function (data) {
                        var model = $('#service');

                        model.empty();

                        $.each(data, function (index, element) {
                            $('#service').append('<option value="'+ index +'">' + element + '</option>');
                        });

                        $('#service').select2({
                            width: "100%"
                        });
                    }
                );
            }

            function setRoles(platform) {
                $.get('{{ url('roles') }}',
                    { platforms: platform },
                    function (data) {
                        var model = $('#roles');

                        model.empty();

                        model.append('<option value="" disabled>Seleccione al menos un rol...</option>');

                        $.each(data, function (index, element) {
                            model.append('<option value="'+ index +'" >' + element + '</option>');
                        });

                        $('#roles').select2({
                            width: "100%"
                        });
                    }
                );
            }
        });
    </script>
@endsection