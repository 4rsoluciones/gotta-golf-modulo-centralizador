@extends('adminlte::page')

@section('content_header')
    <h1>Usuarios <small>Eliminar</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form method="post" action="{{ url('admin/users/delete/'.$user->id) }}">
                {{ csrf_field() }}
                <div class="box">
                    <div class="box-header">
                        <h3>¿Seguro que desea eliminar al siguiente usuario?</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="first_name">Nombre</label>
                            <span id="first_name" class="form-control" >{{ $user->profile->first_name }}</span>
                        </div>
                        <div class="form-group">
                            <label for="last_name">Apellido</label>
                            <span id="last_name" class="form-control" >{{ $user->profile->last_name }}</span>
                        </div>
                        <div class="form-group">
                            <label for="username">Nombre de ususario</label>
                            <span id="username" class="form-control" >{{ $user->username }}</span>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <span id="email" class="form-control" >{{ $user->email }}</span>
                        </div>
                        <div class="form-group">
                            <label for="roles">Roles</label>
                            <span id="roles" class="form-control" >
                                @foreach($roles as $role)
                                    <span class="label bg-aqua">{{ $role }}</span>
                                @endforeach
                                    </span>
                        </div>
                        <div class="form-group">
                            <label for="platform">Plataformas Registradas</label>
                            <span id="platform" class="form-control" >
                                @foreach($user->platforms as $plarform)
                                    <span class="label bg-aqua">{{ $plarform->name }}</span>
                                @endforeach
                                    </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="button" class="form-control btn btn-flat btn-info" value="Volver" onclick="location.href ='{{ url('admin/users') }}'">
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-6">
                        <div class="form-group">
                            <input type="submit" class="form-control btn btn-flat btn-danger" value="Eliminar">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop