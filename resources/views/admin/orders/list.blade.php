@extends('adminlte::page')

@section('content_header')
    <h1>Ordenes <small>Listado</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if (session('success'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success">{{ session('success') }}</div>
                    </div>
                </div>
            @endif
            <div class="dataTables_wrapper">
                @if (isset($orders) && !empty($orders))
                    <table id="orders" class="table table-bordered table-hover dataTable">
                        <thead>
                        <tr>
                            <th>Estatus</th>
                            <th>¿Pago en Efectivo?</th>
                            <th>Código Promocional</th>
                            <th>Monto Pagado</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td>{{ $order->status->name  }}</td>
                                <td>{{ $order->pay_cash == 1 ? 'Si' : 'No' }}</td>
                                <td>{{ (!empty($order->promoCode) ? $order->promoCode->code : 'N/A') }}</td>
                                <td>{{ $order->payments->sum('amount') }}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-flat" onclick="location.href ='{{ url('admin/orders/'.$order->id) }}'">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button type="button" class="btn btn-default btn-flat" onclick="location.href ='{{ url('admin/orders/edit/'.$order->id) }}'">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        {{--<button type="button" class="btn btn-default btn-flat" onclick="location.href ='{{ url('admin/orders/delete/'.$order->id) }}'">
                                            <i class="fa fa-trash"></i>
                                        </button>--}}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <h2>No hay ordenes registradas.</h2>
                @endif
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('#orders').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : false,
                'language': {
                    'search' : 'Buscar',
                    'emptyTable': 'No se encontraron registros.',
                    'zeroRecords': 'No se encontraron registros que concuerden con la busqueda',
                    'paginate': {
                        'first': 'Primero',
                        'last': 'Último',
                        'next': 'Siguiente',
                        'previous': 'Anterior',
                    }
                }
            })
        })

    </script>
@stop