@extends('adminlte::page')

@section('content_header')
    <h1>Ordenes <small>Editar</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12" id="order-details">
            <form method="post" action="{{ url('admin/orders/edit/'.$order->id) }}">
                {{ csrf_field() }}
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Datos</a></li>
                        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="true">Pagos</a></li>
                        {{-- <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                Actiones <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Aciones</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">1</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">2</a></li>
                            </ul>
                        </li>--}}
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="box-body">
                                <div class="form-group{{ $errors->has('order_status_id') ? ' has-error' : '' }}">
                                    <label for="name">Estatus</label>
                                    <select class="form-control" name="order_status_id" id="order_status_id">
                                        <option value="">-- Seleccione un estatus --</option>
                                        @if(count($statuses))
                                            @foreach($statuses as $id => $status)
                                                <option value="{{ $id }}" {{ ($id == $order->status->id && !$errors->has('order_status_id')) ? 'selected' : '' }}>{{ $status }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if ($errors->has('order_status_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('order_status_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class="checkbox-inline">
                                        <label>
                                            <input type="checkbox" name="pay_cash" id="pay_cash" value="1" {{ ($order->pay_cash == 1 || $errors->has('cash_amount')) ? 'checked' : '' }}>
                                            <strong>¿Pago en Efectivo?</strong>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('cash_amount') ? '' : 'hidden'}} {{ $errors->has('cash_amount') ? ' has-error' : '' }}" id="cash_div">
                                    <label for="cash_amount">Monto</label>
                                    <input type="text" id="cash_amount" name="cash_amount" class="form-control numeric" />
                                    @if ($errors->has('cash_amount'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('cash_amount') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="long_description">Código Promocional</label>
                                    <span id="long_description" class="form-control" >{{ empty($order->promoCode) ? 'N/A' : $order->promoCode->code }}</span>
                                </div>
                                <div class="form-group">
                                    <label for="type">Descuento del Código Promocional</label>
                                    <span id="type" class="form-control" >{{ empty($order->promoCode) ? 'N/A' : $order->promoCode->discount }}</span>
                                </div>
                                <div class="form-group">
                                    <label for="platform">Servicios Relacionados</label>
                                    <span id="platform" class="form-control" >
                                        @foreach($order->services as $service)
                                            <span class="label bg-aqua">{{ $service->name }}</span>
                                        @endforeach
                                    </span>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-3 col-md-offset-9">
                                        <div class="form-group">
                                            <input type="button" class="form-control btn btn-flat btn-success" data-toggle="modal" data-target="#paymentModal" value="Registrar Pago">
                                        </div>
                                    </div>
                                </div>
                                <div class="dataTables_wrapper">
                                    @if(count($order->payments))
                                        <table id="users" class="table table-bordered table-hover dataTable">
                                            <thead>
                                            <tr>
                                                <th>Monto</th>
                                                <th>Tipo de Pago</th>
                                                <th>Fecha de Pago</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($order->payments as $payment)
                                                <tr>
                                                    <td>{{ $payment->amount }}</td>
                                                    <td>{{ $payment->type->name }}</td>
                                                    <td>{{ Carbon\Carbon::parse($payment->created_at)->format('d-m-Y') }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <h3>No hay pagos registrados para esta orden</h3>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="button" class="form-control btn btn-flat btn-info" value="Volver" onclick="location.href ='{{ url('admin/orders') }}'">
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-6">
                        <div class="form-group">
                            <input type="submit" class="form-control btn btn-flat btn-success" value="Guardar">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade bs-example-modal-lg" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Agregar Pago</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group" id="payment-div">
                        <label for="name">Tipo de Pago</label>
                        <select class="form-control" name="payment_type_id" id="payment_type_id">
                            <option value="">-- Seleccione un tipo --</option>
                            @if(count($paymentTypes))
                                @foreach($paymentTypes as $id => $type)
                                    <option value="{{ $id }}">{{ $type }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group" id="amount-div">
                        <label for="amount">Monto</label>
                        <input type="text" id="amount" name="amount" class="form-control numeric" />
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <div class="form-group">
                                <button type="button" class="form-control btn btn-flat btn-danger" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3 col-md-offset-6 col-sm-offset-6 col-xs-offset-6">
                            <div class="form-group">
                                <button type="button" id="save-payment" class="form-control btn btn-flat btn-success">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            if (sessionStorage.getItem('success')) {
                $('#order-details').prepend('<div class="row"><div class="col-md-12"><div class="alert alert-success">'+ sessionStorage.getItem('success') +'</div></div></div>').show();

                sessionStorage.removeItem('success');
            }

            $('#pay_cash').on('change', function() {
               if( $(this).is(':checked')) {
                   $('#cash_div').removeClass('hidden');
               } else {
                   $('#cash_div').addClass('hidden');
               }
            });

            $('.numeric').inputmask("numeric", {
                radixPoint: ",",
                groupSeparator:'.',
                digits: 2,
                autoGroup: true,
                rightAlign: true,
                allowMinus: false,
                oncleared: function () { self.Value(''); }
            });

            $('#save-payment').on('click', function() {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="_token"]').val()
                    },
                    url: "{{ url('admin/orders/add-payment') }}",
                    method: "POST",
                    data: {
                        payment_type_id: $('#payment_type_id').val(),
                        amount: $('#amount').val(),
                        order_id: {{ $order->id }}
                    }
                }).done(function (data) {
                    sessionStorage.setItem('success', data);

                    $('#paymentModal').modal('hide');

                    location.reload();
                }).fail(function (data) {
                    var response = data.responseJSON;
                    console.log('data', response);
                    $('.help-block').remove();

                    $.each(response, function(key, element) {
                        $('[name="'+ key +'"]').parent().addClass('has-error').append('<span class="help-block"><strong>'+ element +'</strong></span>');
                    });
                });
            })
        });
    </script>
@stop