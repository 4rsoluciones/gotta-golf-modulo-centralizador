@extends('adminlte::page')

@section('content_header')
    <h1>Ordenes <small>Detalle</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Datos</a></li>
                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="true">Pagos</a></li>
                    {{-- <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                            Actiones <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Aciones</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">1</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">2</a></li>
                        </ul>
                    </li>--}}
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Estatus</label>
                                <span id="name" class="form-control" >{{ $order->status->name }}</span>
                            </div>
                            <div class="form-group">
                                <label for="description">¿Pago en Efectivo?</label>
                                <span id="description" class="form-control" >{{ $order->pay_cash == 1 ? 'Si' : 'No' }}</span>
                            </div>
                            <div class="form-group">
                                <label for="long_description">Código Promocional</label>
                                <span id="long_description" class="form-control" >{{ empty($order->promoCode) ? 'N/A' : $order->promoCode->code }}</span>
                            </div>
                            <div class="form-group">
                                <label for="type">Descuento del Código Promocional</label>
                                <span id="type" class="form-control" >{{ empty($order->promoCode) ? 'N/A' : $order->promoCode->discount }}</span>
                            </div>
                            <div class="form-group">
                                <label for="platform">Servicios Relacionados</label>
                                <span id="platform" class="form-control" >
                                    @foreach($order->services as $service)
                                        <span class="label bg-aqua">{{ $service->name }}</span>
                                    @endforeach
                                </span>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                        <div class="box-body">
                            <div class="dataTables_wrapper">
                                @if(count($order->payments))
                                    <table id="users" class="table table-bordered table-hover dataTable">
                                        <thead>
                                        <tr>
                                            <th>Monto</th>
                                            <th>Tipo de Pago</th>
                                            <th>Fecha de Pago</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($order->payments as $payment)
                                            <tr>
                                                <td>{{ $payment->amount }}</td>
                                                <td>{{ $payment->type->name }}</td>
                                                <td>{{ Carbon\Carbon::parse($payment->created_at)->format('d-m-Y') }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <h3>No hay pagos registrados para esta orden</h3>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="button" class="form-control btn btn-flat btn-info" value="Volver" onclick="location.href ='{{ url('admin/orders') }}'">
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-6">
                    <div class="form-group">
                        <input type="button" class="form-control btn btn-flat btn-success" value="Editar" onclick="location.href ='{{ url('admin/orders/edit/'.$order->id) }}'">
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop