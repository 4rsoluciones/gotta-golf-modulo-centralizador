 @extends('adminlte::page')

@section('content_header')
    <h1>Servicios <small>Editar</small></h1>
@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sortable.css') }}">
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div id="div-message" class="col-md-12" style="display: none">
                    <div id="message" class="alert alert-danger"></div>
                </div>
                <div id="error" class="alert alert-danger" role="alert" style="display: none;"></div>
            </div>
            <form id="service-form" method="post" action="{{ url('admin/services/edit/'.$service->id) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li id="first-tab" class="active"><a class="tab-click" href="#tab_1" data-order="1" data-toggle="tab" aria-expanded="true">Datos</a></li>
                        <li id="second-tab" class=""><a class="tab-click" href="#tab_2" data-order="2" data-toggle="tab" aria-expanded="true">Plataformas</a></li>
                        {{-- <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                Actiones <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Aciones</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">1</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">2</a></li>
                            </ul>
                        </li>--}}
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="box-body">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name">Nombre</label>
                                    <input type="text" name="name" id="name" class="form-control" value="{{ $service->name }}" required/>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                    <label for="description">Descripción</label>
                                    <input type="text" name="description" id="description" class="form-control" value="{{ $service->description }}" required/>
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('long_description') ? ' has-error' : '' }}">
                                    <label for="long_description">Detalle</label>
                                    <textarea class="form-control" rows="5" id="long_description" name="long_description" style="resize: none;" required>{{ $service->long_description }}</textarea>
                                    @if ($errors->has('long_description'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('long_description') }}</strong>
                                            </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                                    <label for="image">Imagen</label>
                                    @php if(!empty($service->image)): @endphp
                                    <img src="{{ Storage::url($service->image) }}" class="img-responsive" style="width: 30%;">
                                    @php endif; @endphp
                                    <input type="file" name="image" id="image" value="{{ old('image') }}" />
                                    <p class="help-block">Imagen referencia del servicio</p>
                                    @if ($errors->has('image'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('image') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                    <label for="price">Precio</label>
                                    <input type="text" name="price" id="price" class="form-control" value="{{ $service->price }}" required/>
                                    @if ($errors->has('price'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('price') }}</strong>
                                            </span>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('trial_days') ? ' has-error' : '' }}">
                                    <label for="trial_days">Días de prueba</label>
                                    <div class="input-group trial-days-number-spinner">
                                        <span class="input-group-btn data-dwn">
                                            <button type="button" class="btn btn-default btn-info" data-dir="dwn" id="trial-days-dwn">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </span>
                                        <input type="text" class="form-control text-center" name="trial_days" id="trial_days" value="{{ $service->trial_days }}" min="0" max="360">
                                        <span class="input-group-btn data-up">
                                            <button type="button" class="btn btn-default btn-info" data-dir="up" id="trial-days-up">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </span>
                                    </div>
                                    @if ($errors->has('trial_days'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('trial_days') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('area_id') ? ' has-error' : '' }}">
                                    <label for="area_id">Area</label>
                                    <select class="form-control" name="area_id" id="area_id">
                                        <option value="">-- Seleccione un área --</option>
                                        @if(count($areas))
                                            @foreach($areas as $id => $area)
                                                <option value="{{ $id }}" {{ ($id == $service->area_id && !$errors->has('area_id')) ? 'selected' : '' }}>{{ $area }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if ($errors->has('area_id'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('area_id') }}</strong>
                                            </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class="checkbox-inline">
                                        <label>
                                            <input type="checkbox" name="visible" value="{{ $service->visible }}" {{ $service->visible == 1 ? 'checked' : '' }}>
                                            <strong>¿Habilitado?</strong>
                                        </label>
                                    </div>
                                    <div class="checkbox-inline">
                                        <label>
                                            <input type="checkbox" name="highlighted" value="{{ $service->highlighted }}" {{ $service->highlighted == 1 ? 'checked' : '' }}>
                                            <strong>¿Destacado?</strong>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <div class="box-body">

                                <div id="packsBlock">
                                    @php foreach($service->platforms as $platform): @endphp
                                    <div id="@php echo "platform".$platform->id @endphp" class="panel panel-default">
                                        <div class="panel-heading">
                                            <strong>Pack de @php echo $platform->name @endphp</strong>
                                            <span class="pull-right"><input type="checkbox" @php echo (($platform->pivot->active == 1)?'checked="checked"':''); @endphp name="platformActive[{{$platform->id}}]"  value="1" /> ¿Habilitado?</span>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="panel-body">
                                            @php
                                                if(!empty($platform->pivot->pack_id) && !empty($packsFields[$platform->pivot->pack_id])):
                                                    foreach($packsFields[$platform->pivot->pack_id] as $field => $value):
                                                    @endphp
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label >{{ $field }}</label>
                                                            <p>{{ $value }}</p>
                                                        </div>
                                                    </div>
                                                    @php
                                                    endforeach;
                                                endif;
                                            @endphp
                                        </div>
                                    </div>
                                    @php endforeach @endphp
                                </div>

                                <div class="form-group{{ $errors->has('area_id') ? ' has-error' : '' }}">
                                    <label for="parent_id">Orden</label>
                                    @if(count($services))
                                        <ul id="sortable" style="list-style-type: none; margin: 0; padding: 0; width: 60%;">
                                            @foreach($services as $serv)
                                                <li class="ui-state-default" style="margin: 0 5px 5px 5px; padding: 5px; font-size: 1.2em;" id="order-{{ $serv->id }}" data-order="{{ $serv->order }}">{{$serv->area->name}} - {{ $serv->name }}</<li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                                <hr/>
                                <h3>Agregar plataformas:</h3>
                                <div class="form-group">
                                    <label for="platform">Plataformas Registradas</label>
                                    <select name="platforms[]" id="platforms" class="form-control select2" multiple autocomplete="off">
                                        @foreach($platforms as $platform)
                                            @php if(!in_array($platform->id, $service->platforms->pluck('id')->toArray())): @endphp
                                            <option value="{{ $platform->id }}">{{ $platform->name }}</option>
                                            @php endif; @endphp
                                        @endforeach
                                    </select>
                                </div>
                                <div id="packs">
                                    @php foreach($platforms as $platform): @endphp
                                        @php if(!in_array($platform->id, $service->platforms->pluck('id')->toArray())): @endphp
                                        <div id="@php echo "platform".$platform->id @endphp" class="panel panel-default hide">
                                            <div class="panel-heading">
                                                <strong>Pack de @php echo $platform->name @endphp</strong>
                                            </div>
                                            <div class="panel-body">
                                                @php foreach($platform->platformPacks as $pack): @endphp
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label >@php echo $pack->name; @endphp </label>
                                                            <input value="" type="text" name="@php echo "platform[".$platform->id."][".$pack->name."]" @endphp" disabled class="form-control" required />
                                                        </div>
                                                    </div>
                                                @php endforeach @endphp
                                            </div>
                                        </div>
                                        @php endif; @endphp
                                    @php endforeach @endphp
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="button" class="form-control btn btn-flat btn-info" value="Volver" onclick="location.href ='{{ url('admin/services') }}'">
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-6">
                        <div class="form-group">
                            <input type="submit" class="form-control btn btn-flat btn-success" value="Enviar">
                        </div>
                    </div>
                </div>
                <input type="hidden" id="changedOrder" name="changedOrder" value="0" />
                @if(count($services))
                    @foreach($services as $serv)
                        <input type="hidden" name="serviceOrder[{{ $serv->id }}]" value="{{ $serv->order }}" />
                    @endforeach
                @endif
            </form>
        </div>
    </div>
@stop

@section('js')
    <script src="{{ asset('js/jquery-ui.js') }}"></script>
    <script>
        $(function () {
            var order = 1;

            $( "#sortable" ).sortable({
                update: function( event, ui ) {
                    setOrder();
                }
            });


            $( "#sortable" ).disableSelection();

            $(document).on('show.bs.tab', 'a[data-toggle="tab"]', function (e) {
                $('#error').hide();

                if ($(e.relatedTarget).parent().prop('id') == 'first-tab') {
                    if (checkFields() == false) {
                        e.preventDefault();
                        $('#error').html('Debe completar todos los campos obligatrios para acceder a la otra pestaña.');
                        $('#error').fadeToggle();
                    } else {
                        $('#save-btn').prop('value', 'Guardar');
                    }
                } else {
                    $('#save-btn').prop('value', 'Siguiente');
                }
            });

            $('.select2').select2({
                width: "100%"
            });

            $('#price').inputmask("numeric", {
                radixPoint: ",",
                digits: 2,
                autoGroup: true,
                rightAlign: false,
                oncleared: function () { self.Value(''); }
            });

            var action;
            $(".days-number-spinner button").mousedown(function () {
                btn = $(this);
                input = btn.closest('.days-number-spinner').find('input');
                btn.closest('.days-number-spinner').find('button').prop("disabled", false);

                if (btn.attr('data-dir') == 'up') {
                    action = setInterval(function(){
                        if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
                            input.val(parseInt(input.val())+1);
                        }else{
                            btn.prop("disabled", true);
                            clearInterval(action);
                        }
                    }, 50);
                } else {
                    action = setInterval(function(){
                        if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
                            input.val(parseInt(input.val())-1);
                        }else{
                            btn.prop("disabled", true);
                            clearInterval(action);
                        }
                    }, 50);
                }
            }).mouseup(function(){
                clearInterval(action);
            });

            $(".trial-days-number-spinner button").mousedown(function () {
                btn = $(this);
                input = btn.closest('.trial-days-number-spinner').find('input');
                btn.closest('.trial-days-number-spinner').find('button').prop("disabled", false);

                if (btn.attr('data-dir') == 'up') {
                    action = setInterval(function(){
                        if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
                            input.val(parseInt(input.val())+1);
                        }else{
                            btn.prop("disabled", true);
                            clearInterval(action);
                        }
                    }, 50);
                } else {
                    action = setInterval(function(){
                        if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
                            input.val(parseInt(input.val())-1);
                        }else{
                            btn.prop("disabled", true);
                            clearInterval(action);
                        }
                    }, 50);
                }
            }).mouseup(function(){
                clearInterval(action);
            });

            $('#is_lifetime').on('change', function () {
               if($('#is_lifetime').is(':checked')) {
                   $('#days-dwn').prop('disabled', true);
                   $('#days').prop('disabled', true);
                   $('#days-up').prop('disabled', true);
                   $('#days').val(0);
               } else {
                   $('#days-dwn').prop('disabled', false);
                   $('#days').prop('disabled', false);
                   $('#days-up').prop('disabled', false);
               }
            });
        });
        function checkFields() {
            var isValid = true;

            $('#tab_1 input[type="text"], #tab_1 textarea').each(function (e) {
                if ($(this).val() == '') {
                    isValid = false;
                }
            });

            return isValid;
        }

        //Add platforms
        $(document).on('change', '#platforms', function () {
            var ids = $(this).val();
            showPlatforms(ids);
        });
        function showPlatforms(ids) {
            $('#packs div[id^="platform"]').addClass('hide');
            $('#packs div[id^="platform"] input').prop('disabled',true);
            for (i = 0; i < ids.length; i++) {
                $('#packs #platform'+ids[i]).removeClass('hide');
                $('#packs #platform'+ids[i]+' input').prop('disabled',false);
            }
        }
        function setOrder() {
            $('#changedOrder').val(1);
            var itemOrder = $('#sortable').sortable("toArray");
            for (var i = 0; i < itemOrder.length; i++) {
                var values = '';
                values = itemOrder[i].split("-");
                $('input[name="serviceOrder['+values[1]+']"]').val(i);
            }
        }
    </script>
@stop
