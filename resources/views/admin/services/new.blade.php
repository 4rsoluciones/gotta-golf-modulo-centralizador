@extends('adminlte::page')

@section('content_header')
    <h1>Servicios <small>Nuevo</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if (session('error'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger">{{ session('error') }}</div>
                    </div>
                </div>
            @endif
            <div id="error" class="alert alert-danger" role="alert" style="display: none;"></div>
            <form method="post" action="{{ url('admin/services/new') }}" enctype="multipart/form-data" novalidate>
                {{ csrf_field() }}
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li id="first-tab" class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Datos</a></li>
                        <li id="second-tab" class=""><a href="#tab_2" data-toggle="tab" aria-expanded="true">Plataformas</a></li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="box-body">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name">Nombre</label>
                                    <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}" required/>
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                    <label for="description">Descripción</label>
                                    <input type="text" name="description" id="description" class="form-control" value="{{ old('description') }}" required/>
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('long_description') ? ' has-error' : '' }}">
                                    <label for="long_description">Detalle</label>
                                    <textarea class="form-control" rows="5" id="long_description" name="long_description" style="resize: none;" required>{{ old('long_description') }}</textarea>
                                    @if ($errors->has('long_description'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('long_description') }}</strong>
                                            </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                                    <label for="image">Imagen</label>
                                    <input type="file" name="image" id="image" value="{{ old('image') }}" />
                                    <p class="help-block">Imagen referencia del servicio</p>
                                    @if ($errors->has('image'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('image') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                    <label for="price">Precio</label>
                                    <input type="text" name="price" id="price" class="form-control" value="{{ old('price') }}" required/>
                                    @if ($errors->has('price'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('price') }}</strong>
                                            </span>
                                    @endif
                                </div>

                                <input type="hidden" name="service_type_id" value="{{ array_keys($serviceTypes->toArray())[0] }}">

                                <div class="form-group{{ $errors->has('trial_days') ? ' has-error' : '' }}">
                                    <label for="trial_days">Días de prueba</label>
                                    <div class="input-group trial-days-number-spinner">
                                        <span class="input-group-btn data-dwn">
                                            <button type="button" class="btn btn-default btn-info" data-dir="dwn" id="trial-days-dwn">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </span>
                                        <input type="text" class="form-control text-center" name="trial_days" id="trial_days" value="{{ old('trial_days') ? old('trial_days') : 0 }}" min="0" max="360">
                                        <span class="input-group-btn data-up">
                                            <button type="button" class="btn btn-default btn-info" data-dir="up" id="trial-days-up">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </span>
                                    </div>
                                    @if ($errors->has('trial_days'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('trial_days') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <div class="checkbox-inline">
                                        <label>
                                            <input type="checkbox" name="visible" value="1" {{ old('visible') == 1 ? 'checked' : '' }}>
                                            <strong>¿Habilitado?</strong>
                                        </label>
                                    </div>
                                    <div class="checkbox-inline">
                                        <label>
                                            <input type="checkbox" name="highlighted" value="1" {{ old('highlighted') == 1 ? 'checked' : '' }}>
                                            <strong>¿Destacado?</strong>
                                        </label>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="tab_2">
                            <div class="box-body">
                                <div class="form-group{{ $errors->has('platforms') ? ' has-error' : '' }}">
                                    <label for="platform">Plataformas Registradas</label>
                                    <select name="platforms[]" id="platforms" class="form-control select2" multiple autocomplete="off">
                                        @php
                                            $platformsSelected = (empty(old('platforms'))?[]:old('platforms'));
                                        @endphp
                                        @foreach($platforms as $id => $platform)
                                            <option value="{{ $platform->id }}" {{(in_array($platform->id, $platformsSelected)) ? 'selected' : '' }}>{{ $platform->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('platforms'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('platforms') }}</strong>
                                            </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('area_id') ? ' has-error' : '' }}">
                                    <label for="area_id">Area</label>
                                    <select class="form-control" name="area_id" id="area_id">
                                        <option value="">-- Seleccione un área --</option>
                                        @if(count($areas))
                                            @foreach($areas as $id => $area)
                                                <option value="{{ $id }}" {{ $id == old('area_id') ? 'selected' : '' }}>{{ $area }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @if ($errors->has('area_id'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('area_id') }}</strong>
                                            </span>
                                    @endif
                                </div>
                                <div id="packs">
                                    @php $oldPlatform = old('platform'); @endphp
                                    @php foreach($platforms as $platform): @endphp
                                    <div id="@php echo "platform".$platform->id @endphp" class="panel panel-default hide">
                                        <div class="panel-heading">
                                            <strong>Pack de @php echo $platform->name @endphp</strong>
                                        </div>
                                        <div class="panel-body">
                                            @php foreach($platform->platformPacks as $pack): @endphp
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label >@php echo $pack->name; @endphp </label>
                                                        <input value="{{ (isset($oldPlatform[$platform->id][$pack->name])?$oldPlatform[$platform->id][$pack->name]:'') }}" type="text" name="@php echo "platform[".$platform->id."][".$pack->name."]" @endphp" class="form-control" required disabled />
                                                    </div>
                                                </div>
                                            @php endforeach @endphp
                                        </div>
                                    </div>
                                    @php endforeach @endphp
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="button" class="form-control btn btn-flat btn-info" value="Volver" onclick="location.href ='{{ url('admin/services') }}'">
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-6">
                        <div class="form-group">
                            <input type="submit" id="save-btn" class="form-control btn btn-flat btn-success" value="Siguiente">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            if ($('.has-error').first().length) {
                if ($('.has-error').first().closest('.tab-pane').prop('id') != 'tab_1') {
                    $('#first-tab, #tab_1').removeClass('active');

                    $('#second-tab, #tab_2').addClass('active');
                }
            }

            $('.select2').select2({
                width: "100%"
            });

            var oldPlatforms = $('#platforms').val();

            if (!$.isEmptyObject(oldPlatforms)) {
                //setPacks(oldPlatforms);
                showPlatforms(oldPlatforms);
            }

            $(document).on('show.bs.tab', 'a[data-toggle="tab"]', function (e) {
                $('#error').hide();

                if ($(e.relatedTarget).parent().prop('id') == 'first-tab') {
                    if (checkFields() == false) {
                        e.preventDefault();
                        $('#error').html('Debe completar todos los campos obligatrios para acceder a la otra pestaña.');
                        $('#error').fadeToggle();
                    } else {
                        $('#save-btn').prop('value', 'Guardar');
                    }
                } else {
                    $('#save-btn').prop('value', 'Siguiente');
                }
            });

            $(document).on('click', '#save-btn', function(e) {
                $('#error').hide();

                if ($('#tab_1').hasClass('active')) {
                    e.preventDefault();

                    if (checkFields() == true) {
                        $('#first-tab, #tab_1').removeClass('active');

                        $('#second-tab').addClass('active').children('a').attr('data-toogle', 'tab');
                        $('#tab_2').addClass('active');

                        $('#save-btn').prop('value', 'Guardar');
                    } else {
                        $('#error').html('Debe completar todos los campos obligatrios para acceder a la otra pestaña.');
                        $('#error').fadeToggle();
                    }
                }
            });

            $(document).on('change', '#platforms', function () {
                var ids = $(this).val();
                showPlatforms(ids);
            });
            function showPlatforms(ids) {
                $('#packs div[id^="platform"]').addClass('hide');
                $('#packs div[id^="platform"] input').prop('disabled',true);
                for (i = 0; i < ids.length; i++) {
                    $('#packs #platform'+ids[i]).removeClass('hide');
                    $('#packs #platform'+ids[i]+' input').prop('disabled',false);
                }
            }
            $('#price').inputmask("numeric", {
                radixPoint: ",",
                digits: 2,
                autoGroup: true,
                rightAlign: false,
                oncleared: function () { self.Value(''); }
            });

            var action;
            $(".days-number-spinner button").mousedown(function () {
                btn = $(this);
                input = btn.closest('.days-number-spinner').find('input');
                btn.closest('.days-number-spinner').find('button').prop("disabled", false);

                if (btn.attr('data-dir') == 'up') {
                    action = setInterval(function(){
                        if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
                            input.val(parseInt(input.val())+1);
                        }else{
                            btn.prop("disabled", true);
                            clearInterval(action);
                        }
                    }, 50);
                } else {
                    action = setInterval(function(){
                        if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
                            input.val(parseInt(input.val())-1);
                        }else{
                            btn.prop("disabled", true);
                            clearInterval(action);
                        }
                    }, 50);
                }
            }).mouseup(function(){
                clearInterval(action);
            });

            $(".trial-days-number-spinner button").mousedown(function () {
                btn = $(this);
                input = btn.closest('.trial-days-number-spinner').find('input');
                btn.closest('.trial-days-number-spinner').find('button').prop("disabled", false);

                if (btn.attr('data-dir') == 'up') {
                    action = setInterval(function(){
                        if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
                            input.val(parseInt(input.val())+1);
                        }else{
                            btn.prop("disabled", true);
                            clearInterval(action);
                        }
                    }, 50);
                } else {
                    action = setInterval(function(){
                        if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
                            input.val(parseInt(input.val())-1);
                        }else{
                            btn.prop("disabled", true);
                            clearInterval(action);
                        }
                    }, 50);
                }
            }).mouseup(function(){
                clearInterval(action);
            });

            $('#is_lifetime').on('change', function () {
                if($('#is_lifetime').is(':checked')) {
                    $('#days-dwn').prop('disabled', true);
                    $('#days').prop('disabled', true);
                    $('#days-up').prop('disabled', true);
                    $('#days').val(0);
                } else {
                    $('#days-dwn').prop('disabled', false);
                    $('#days').prop('disabled', false);
                    $('#days-up').prop('disabled', false);
                }
            });
        });
        function checkFields() {
            var isValid = true;

            $('#tab_1 input[type="text"], #tab_1 textarea').each(function (e) {
                if ($(this).val() === '') {
                    isValid = false;
                }
            });

            return isValid;
        }
    </script>
@stop
