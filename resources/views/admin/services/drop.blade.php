@extends('adminlte::page')

@section('content_header')
    <h1>Servicios <small>Eliminar</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form method="post" action="{{ url('admin/services/delete/'.$service->id) }}">
                {{ csrf_field() }}
                <div class="box">
                    <div class="box-header">
                        <h3>¿Seguro que desea eliminar al siguiente servicio?</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <span id="name" class="form-control" >{{ $service->name }}</span>
                        </div>
                        <div class="form-group">
                            <label for="description">Descripción</label>
                            <span id="description" class="form-control" >{{ $service->description }}</span>
                        </div>
                        <div class="form-group">
                            <label for="long_description"Detalle</label>
                            <span id="long_description" class="form-control" >{{ $service->long_description }}</span>
                        </div>
                        <div class="form-group">
                            <label for="platform">Plataformas Registradas</label>
                            <span id="platform" class="form-control" >
                                @foreach($service->platforms as $plarform)
                                    <span class="label bg-aqua">{{ $plarform->name }}</span>
                                @endforeach
                                    </span>
                        </div>
                        <div class="form-group">
                            <label for="orders">Ordenes</label>
                            <span id="orders" class="form-control" >
                                {{ count($orders) ? 'Posee '.count($orders).' ordenes registradas.' : 'No poseer ordenes registradas' }}
                            </span>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="button" class="form-control btn btn-flat btn-info" value="Volver" onclick="location.href ='{{ url('admin/services') }}'">
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-6">
                        <div class="form-group">
                            <input type="submit" class="form-control btn btn-flat btn-danger" value="Eliminar">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop