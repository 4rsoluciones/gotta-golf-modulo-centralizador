@extends('adminlte::page')

@section('content_header')
    <h1>Servicios <small>Listado</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if (session('success'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success">{{ session('success') }}</div>
                    </div>
                </div>
            @endif
            @if (session('error'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger">{{ session('error') }}</div>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-3 col-md-offset-9">
                    <div class="form-group">
                        <input type="button" class="form-control btn btn-flat btn-success" value="Nuevo Servicio" onclick="location.href ='{{ url('admin/services/new') }}'">
                    </div>
                </div>
            </div>
            <div class="dataTables_wrapper">
                @if (isset($services) && !empty($services))
                    <table id="services" class="table table-bordered table-hover dataTable">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Area</th>
                            <th>Descripción</th>
                            <th>precio</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($services as $service)
                            <tr>
                                <td>{{ $service->name  }}</td>
                                <td>{{ $service->area->name }}</td>
                                <td>{{ $service->description }}</td>
                                <td>{{ $service->price }}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-flat" onclick="location.href ='{{ url('admin/services/'.$service->id) }}'">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button type="button" class="btn btn-default btn-flat" onclick="location.href ='{{ url('admin/services/edit/'.$service->id) }}'">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        {{--<button type="button" class="btn btn-default btn-flat" onclick="location.href ='{{ url('admin/services/delete/'.$service->id) }}'">--}}
                                            {{--<i class="fa fa-trash"></i>--}}
                                        {{--</button>--}}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <h2>No hay servicios registrados.</h2>
                @endif
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('#services').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : false,
                'language': {
                    'search' : 'Buscar',
                    'emptyTable': 'No se encontraron registros.',
                    'zeroRecords': 'No se encontraron registros que concuerden con la busqueda',
                    'paginate': {
                        'first': 'Primero',
                        'last': 'Último',
                        'next': 'Siguiente',
                        'previous': 'Anterior',
                    }
                }
            })
        })

    </script>
@stop