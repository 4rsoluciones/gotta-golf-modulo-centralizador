@extends('adminlte::page')

@section('content_header')
    <h1>Servicios <small>Detalle</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Datos</a></li>
                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="true">Plataformas</a></li>
                    <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="true">Ordenes</a></li>
                    {{-- <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                            Actiones <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Aciones</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">1</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">2</a></li>
                        </ul>
                    </li>--}}
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Nombre</label>
                                <span id="name" class="form-control" >{{ $service->name }}</span>
                            </div>
                            <div class="form-group">
                                <label for="description">Descripción</label>
                                <span id="description" class="form-control" >{{ $service->description }}</span>
                            </div>
                            <div class="form-group">
                                <label for="long_description">Detalle</label>
                                <span id="long_description" class="form-control" >{{ $service->long_description }}</span>
                            </div>
                            <div class="form-group">
                                <label for="image">Imagen</label>
                                <img src="{{ Storage::url($service->image) }}" class="img-responsive" style="width: 30%;">
                            </div>
                            <div class="form-group">
                                <label for="price">Precio</label>
                                <span id="price" class="form-control" >{{ $service->price }}</span>
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label for="type">Tipo</label>--}}
                                {{--<span id="type" class="form-control" >{{ $service->type }}</span>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="days">Días</label>--}}
                                {{--<span id="days" class="form-control">{{ $service->days }}</span>--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <label for="trial_days">Días de prueba</label>
                                <span id="trial_days" class="form-control">{{ $service->trial_days }}</span>
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label for="debitDues">Número de cuotas</label>--}}
                                {{--<span id="debitDues" class="form-control">{{ $service->debitDues }}</span>--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <label for="area_id">Area</label>
                                <span id="area_id" class="form-control">{{ $area }}</span>
                            </div>
                            <div class="form-group">
                                <div class="checkbox-inline disabled">
                                    <label>
                                        <input type="checkbox" name="visible" value="{{ $service->visible }}" {{ $service->visible == 1 ? 'checked' : '' }} disabled>
                                        <strong>¿Habilitado?</strong>
                                    </label>
                                </div>
                                <div class="checkbox-inline disabled">
                                    <label>
                                        <input type="checkbox" name="highlighted" value="{{ $service->highlighted }}" {{ $service->highlighted == 1 ? 'checked' : '' }} disabled>
                                        <strong>¿Destacado?</strong>
                                    </label>
                                </div>
                                {{--<div class="checkbox-inline disabled">--}}
                                    {{--<label>--}}
                                        {{--<input type="checkbox" name="important" value="{{ $service->important }}" {{ $service->important == 1 ? 'checked' : '' }} disabled>--}}
                                        {{--<strong>¿Importante?</strong>--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                                {{--<div class="checkbox-inline disabled">--}}
                                    {{--<label>--}}
                                        {{--<input type="checkbox" name="is_lifetime" id="is_lifetime" value="{{ $service->is_lifetime }}" {{ $service->is_lifetime == 1 ? 'checked' : '' }} disabled>--}}
                                        {{--<strong>¿De por vida?</strong>--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="platform">Plataformas Registradas</label>
                                <span id="platform" class="form-control" >
                                    @foreach($service->platforms as $plarform)
                                        <span class="label bg-aqua">{{ $plarform->name }}</span>
                                    @endforeach
                                </span>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_3">
                        <div class="box-body">
                            <div class="dataTables_wrapper">
                                {{--<div class="dataTables_filter pull-left" style="position: absolute; z-index: 1000;">--}}
                                    {{--<input type="button" class="btn btn-flat btn-info" style="margin-bottom: 5px;" value="Exportar Excel" />--}}
                                    {{--<label for="fecha"></label>--}}
                                    {{--<input type="text" id="fecha" value=""/>--}}
                                {{--</div>--}}
                                @if(count($orders))
                                    <table id="orders" class="table table-bordered table-hover dataTable">
                                        <thead>
                                        <tr>
                                            <th>Estatus</th>
                                            <th>Código Promocional</th>
                                            <th>Pago en Efectivo</th>
                                            <th>Monto Pagado</th>
                                            <th>Fecha de Pago</th>
                                            <th>Usuario</th>
                                            <th>Provincia</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($orders as $order)
                                                <tr>
                                                    <td>{{ $order->status->name }}</td>
                                                    <td>{{ (!empty($order->promoCode) ? $order->promoCode->code : 'N/A') }}</td>
                                                    <td>{{ $order->cash_payment == 1 ? 'Si' : 'No' }}</td>
                                                    <td>{{ $order->payments->sum('amount') }}</td>
                                                    <td>{{ $order->created_at->format('Y-m-d') }}</td>
                                                    <td>{{ (isset($order->services[0]) && isset($order->services[0]->users[0])) ? ucfirst($order->services[0]->users[0]->profile->first_name.' '.$order->services[0]->users[0]->profile->last_name) : '-' }}</td>
                                                    <td>{{ (isset($order->services[0]) && isset($order->services[0]->users[0])) ? $order->services[0]->users[0]->profile->state->name : '-' }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <h3>No hay ordenes creadas para este servicio</h3>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="button" class="form-control btn btn-flat btn-info" value="Volver" onclick="location.href ='{{ url('admin/services') }}'">
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-6">
                    <div class="form-group">
                        <input type="button" class="form-control btn btn-flat btn-success" value="Editar" onclick="location.href ='{{ url('admin/services/edit/'.$service->id) }}'">
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('#orders').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : false,
                'language': {
                    'search' : 'Buscar',
                    'emptyTable': 'No se encontraron registros.',
                    'zeroRecords': 'No se encontraron registros que concuerden con la busqueda',
                    'paginate': {
                        'first': 'Primero',
                        'last': 'Último',
                        'next': 'Siguiente',
                        'previous': 'Anterior',
                    }
                }
            });
        });
    </script>
@stop