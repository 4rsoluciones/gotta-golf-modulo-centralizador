@extends('adminlte::page')

@section('content_header')
    <h1>Servicios <small>Area - Detalle</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Datos</a></li>
                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="true">Servicios</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Nombre</label>
                                <span id="name" class="form-control" >{{ $area->name }}</span>
                            </div>
                            <div class="form-group">
                                <label for="slug">Precio</label>
                                <span id="slug" class="form-control" >{{ $area->slug }}</span>
                            </div>
                            <div class="form-group">
                                <label for="description">Detalle</label>
                                <span id="description" class="form-control" >{{ $area->description }}</span>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="platform">Servicios Asociados</label>
                                <span id="platform" class="form-control" >
                                    @foreach($area->services as $service)
                                        <span class="label bg-aqua">{{ $service->name }}</span>
                                    @endforeach
                                </span>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="button" class="form-control btn btn-flat btn-info" value="Volver" onclick="location.href ='{{ url('admin/areas') }}'">
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-6">
                    <div class="form-group">
                        <input type="button" class="form-control btn btn-flat btn-success" value="Editar" onclick="location.href ='{{ url('admin/areas/edit/'.$area->id) }}'">
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop