@extends('adminlte::page')

@section('content_header')
    <h1>Servicios <small>Areas - listado</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if (session('success'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success">{{ session('success') }}</div>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-3 col-md-offset-9">
                    <div class="form-group">
                        <input type="button" class="form-control btn btn-flat btn-success" value="Nueva Area" onclick="location.href ='{{ url('admin/areas/new') }}'">
                    </div>
                </div>
            </div>
            <div class="dataTables_wrapper">
                @if (isset($areas) && !empty($areas))
                    <table id="areas" class="table table-bordered table-hover dataTable">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($areas as $area)
                            <tr>
                                <td>{{ $area->name  }}</td>
                                <td>{{ $area->description }}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-flat" onclick="location.href ='{{ url('admin/areas/'.$area->id) }}'">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button type="button" class="btn btn-default btn-flat" onclick="location.href ='{{ url('admin/areas/edit/'.$area->id) }}'">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-default btn-flat" onclick="location.href ='{{ url('admin/areas/delete/'.$area->id) }}'">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <h2>No hay areas registradas.</h2>
                @endif
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('#areas').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : false,
                'language': {
                    'search' : 'Buscar',
                    'emptyTable': 'No se encontraron registros.',
                    'zeroRecords': 'No se encontraron registros que concuerden con la busqueda',
                    'paginate': {
                        'first': 'Primero',
                        'last': 'Último',
                        'next': 'Siguiente',
                        'previous': 'Anterior',
                    }
                }
            })
        })

    </script>
@stop