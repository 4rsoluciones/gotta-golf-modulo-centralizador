@extends('adminlte::page')

@section('content_header')
    <h1>Servicios <small>Areas - Eliminar</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form method="post" action="{{ url('admin/areas/delete/'.$area->id) }}">
                {{ csrf_field() }}
                <div class="box">
                    <div class="box-header">
                        <h3>¿Seguro que desea eliminar la siguiente Area?</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <span id="name" class="form-control" >{{ $area->name }}</span>
                        </div>
                        <div class="form-group">
                            <label for="description"Descripción</label>
                            <span id="description" class="form-control" >{{ $area->description }}</span>
                        </div>
                        <div class="form-group">
                            <label for="platform">Servicios Asociados</label>
                            <span id="platform" class="form-control" >
                                @foreach($area->services as $service)
                                    <span class="label bg-aqua">{{ $service->name }}</span>
                                @endforeach
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="button" class="form-control btn btn-flat btn-info" value="Volver" onclick="location.href ='{{ url('admin/areas') }}'">
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-6">
                        <div class="form-group">
                            <input type="submit" class="form-control btn btn-flat btn-danger" value="Eliminar">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop