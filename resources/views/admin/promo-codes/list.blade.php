@extends('adminlte::page')

@section('content_header')
    <h1>Códigos Promocionales <small>listado</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if (session('success'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success">{{ session('success') }}</div>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-3 col-md-offset-9">
                    <div class="form-group">
                        <input type="button" class="form-control btn btn-flat btn-success" value="Nuevo Código" onclick="location.href ='{{ url('admin/promo-codes/new') }}'">
                    </div>
                </div>
            </div>
            <div class="dataTables_wrapper">
                @if (isset($promoCodes) && !empty($promoCodes))
                    <table id="codes" class="table table-bordered table-hover dataTable">
                        <thead>
                        <tr>
                            <th>Código</th>
                            <th>Descuento</th>
                            <th>Fecha de Expiración</th>
                            <th>Usos Permitidos</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($promoCodes as $code)
                            <tr>
                                <td>{{ $code->code  }}</td>
                                <td>{{ $code->discount }}</td>
                                <td>{{ $code->valid_until }}</td>
                                <td>{{ (!empty($code->allowed_uses) ? $code->allowed_uses : 'N/A') }}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-flat" onclick="location.href ='{{ url('admin/promo-codes/'.$code->id) }}'">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button type="button" class="btn btn-default btn-flat" onclick="location.href ='{{ url('admin/promo-codes/edit/'.$code->id) }}'">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-default btn-flat" onclick="location.href ='{{ url('admin/promo-codes/delete/'.$code->id) }}'">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <h2>No hay códigos registrados.</h2>
                @endif
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('#codes').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : false,
                'language': {
                    'search' : 'Buscar',
                    'emptyTable': 'No se encontraron registros.',
                    'zeroRecords': 'No se encontraron registros que concuerden con la busqueda',
                    'paginate': {
                        'first': 'Primero',
                        'last': 'Último',
                        'next': 'Siguiente',
                        'previous': 'Anterior',
                    }
                }
            })
        })

    </script>
@stop