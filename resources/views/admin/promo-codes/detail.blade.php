@extends('adminlte::page')

@section('content_header')
    <h1>Códigos Promocionales <small>Detalle</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form method="post" action="{{ url('admin/promo-codes/edit/'.$promoCode->id) }}">
                {{ csrf_field() }}
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Datos</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="code">Código</label>
                                    <span name="code" id="code" class="form-control">{{ $promoCode->code }}</span>
                                </div>
                                <div class="form-group">
                                    <label for="discount">Descuento</label>
                                    <span class="form-control" id="discount">{{ $promoCode->discount }}</span>
                                </div>
                                <div class="form-group">
                                    <label for="valid_until">Fecha de expiración</label>
                                    <span name="valid_until" id="valid_until" class="form-control">{{ $promoCode->valid_until }}</span>
                                </div>
                                <div class="form-group{{ $errors->has('allowed_uses') ? ' has-error' : '' }}">
                                    <label for="allowed_uses">Número de usos permitidos</label>
                                    <span class="form-control" name="allowed_uses" id="allowed_uses">{{ $promoCode->allowed_uses < 0 ? $promoCode->allowed_uses : 'N/A' }}</span>
                                </div>
                                <div class="form-group">
                                    <label for="services">Servicios Permitidos</label>
                                    <span id="platform" class="form-control" >
                                        @foreach($promoCode->services as $service)
                                            <span class="label bg-aqua">{{ $service->name }}</span>
                                        @endforeach
                                    </span>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="button" class="form-control btn btn-flat btn-info" value="Volver" onclick="location.href ='{{ url('admin/promo-codes') }}'">
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-6">
                        <div class="form-group">
                            <input type="button" class="form-control btn btn-flat btn-success" value="Editar" onclick="location.href = '{{ url('admin/promo-codes/edit/'.$promoCode->id) }}'">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop