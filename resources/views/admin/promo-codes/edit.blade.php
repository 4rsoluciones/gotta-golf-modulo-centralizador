@extends('adminlte::page')

@section('content_header')
    <h1>Códigos Promocionales <small>Editar</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form method="post" action="{{ url('admin/promo-codes/edit/'.$promoCode->id) }}">
                {{ csrf_field() }}
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Datos</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="box-body">
                                <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                                    <label for="code">Código</label>
                                    <input type="text" name="show-code" id="show-code" class="form-control" value="{{ $promoCode->code }}" disabled/>
                                    <input type="hidden" name="code" id="code" value="{{ $promoCode->code }}">
                                    @if ($errors->has('code'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('code') }}</strong>
                                            </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('discount') ? ' has-error' : '' }}">
                                    <label for="discount">Descuento</label>
                                    <div class="input-group discount-number-spinner">
                                        <span class="input-group-btn data-dwn">
                                            <button type="button" class="btn btn-default btn-info" data-dir="dwn" id="discount-dwn">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </span>
                                        <input type="text" class="form-control text-center" name="discount" id="discount" value="{{ $promoCode->discount }}" min="0" max="100" style="z-index: auto">
                                        <span class="input-group-btn data-up">
                                            <button type="button" class="btn btn-default btn-info" data-dir="up" id="discount-up">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </span>
                                    </div>
                                    @if ($errors->has('discount'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('discount') }}</strong>
                                            </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('valid_until') ? ' has-error' : '' }}">
                                    <label for="valid_until">Fecha de expiración</label>
                                    <input name="valid_until" id="valid_until" type="text" class="form-control datepicker" value="{{ $promoCode->valid_until }}" required/>
                                    @if ($errors->has('valid_until'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('valid_until') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('allowed_uses') ? ' has-error' : '' }}">
                                    <label for="allowed_uses">Número de usos permitidos (opcional)</label>
                                    <div class="input-group uses-number-spinner">
                                        <span class="input-group-btn data-dwn">
                                            <button type="button" class="btn btn-default btn-info" data-dir="dwn" id="uses-dwn">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </span>
                                        <input type="text" class="form-control text-center" name="allowed_uses" id="allowed_uses" value="{{ $promoCode->allowed_uses }}" min="0" max="360" style="z-index: auto">
                                        <span class="input-group-btn data-up">
                                            <button type="button" class="btn btn-default btn-info" data-dir="up" id="uses-up">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </span>
                                    </div>
                                    @if ($errors->has('allowed_uses'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('allowed_uses') }}</strong>
                                            </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="services">Servicios Permitidos (opcional)</label>
                                    <select name="services[]" id="services" class="form-control select2" multiple autocomplete="off">
                                        @foreach($services as $id => $service)
                                            <option value="{{ $id }}" {{(in_array($id, $promoCode->services->pluck('id')->toArray())) ? 'selected' : '' }}>{{ $service }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="button" class="form-control btn btn-flat btn-info" value="Volver" onclick="location.href ='{{ url('admin/promo-codes') }}'">
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-6">
                        <div class="form-group">
                            <input type="submit" class="form-control btn btn-flat btn-success" value="Enviar">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('.select2').select2({
                width: "100%"
            });

            $('.datepicker').datepicker({
                dateFormat: 'yy-mm-dd'
            });

            var action;
            $(".uses-number-spinner button").mousedown(function () {
                btn = $(this);
                input = btn.closest('.uses-number-spinner').find('input');
                btn.closest('.uses-number-spinner').find('button').prop("disabled", false);

                if (btn.attr('data-dir') == 'up') {
                    action = setInterval(function(){
                        if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
                            input.val(parseInt(input.val())+1);
                        }else{
                            btn.prop("disabled", true);
                            clearInterval(action);
                        }
                    }, 50);
                } else {
                    action = setInterval(function(){
                        if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
                            input.val(parseInt(input.val())-1);
                        }else{
                            btn.prop("disabled", true);
                            clearInterval(action);
                        }
                    }, 50);
                }
            }).mouseup(function(){
                clearInterval(action);
            });

            $(".discount-number-spinner button").mousedown(function () {
                btn = $(this);
                input = btn.closest('.discount-number-spinner').find('input');
                btn.closest('.discount-number-spinner').find('button').prop("disabled", false);

                if (btn.attr('data-dir') == 'up') {
                    action = setInterval(function(){
                        if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
                            input.val(parseInt(input.val())+1);
                        }else{
                            btn.prop("disabled", true);
                            clearInterval(action);
                        }
                    }, 50);
                } else {
                    action = setInterval(function(){
                        if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
                            input.val(parseInt(input.val())-1);
                        }else{
                            btn.prop("disabled", true);
                            clearInterval(action);
                        }
                    }, 50);
                }
            }).mouseup(function(){
                clearInterval(action);
            });
        });
    </script>
@stop