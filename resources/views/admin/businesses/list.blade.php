@extends('adminlte::page')

@section('content_header')
    <h1>Establecimientos - Categorias<small>Listado</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if (session('success'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success">{{ session('success') }}</div>
                    </div>
                </div>
            @endif
            @if (session('error'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger">{{ session('error') }}</div>
                    </div>
                </div>
            @endif
            <div class="dataTables_wrapper">
                @if (isset($businesses) && !empty($businesses))
                    <table id="services" class="table table-bordered table-hover dataTable">
                        <thead>
                        <tr>
                            <th style="width:1px;">Id</th>
                            <th>Nombre</th>
                            <th>Categoria</th>
                            <th>Razon Social</th>
                            <th>Cuit</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($businesses as $business)
                            <tr>
                                <td style="width:1px;">{{ $business->id }}</td>
                                <td>{{ $business->name }}</td>
                                <td>{{ $business->category->name }}</td>
                                <td>{{ $business->business_name }}</td>
                                <td>{{ $business->cuit }}</td>
                                <td class="text-center" style="width: 1%;">
                                    <div class="btn-group">
                                        <button
                                                type="button"
                                                class="btn btn-default btn-flat"
                                                onclick="location.href ='{{ url('admin/publicists/places/'.$business->id) }}'"
                                                data-toggle="tooltip"
                                                data-placement="top"
                                                title="Ver"
                                        >
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <h2>No hay categorias creadas.</h2>
                @endif
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('#services').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : false,
                'language': {
                    'search' : 'Buscar',
                    'emptyTable': 'No se encontraron registros.',
                    'zeroRecords': 'No se encontraron registros que concuerden con la busqueda',
                    'paginate': {
                        'first': 'Primero',
                        'last': 'Último',
                        'next': 'Siguiente',
                        'previous': 'Anterior',
                    }
                }
            })
        })

    </script>
@stop
