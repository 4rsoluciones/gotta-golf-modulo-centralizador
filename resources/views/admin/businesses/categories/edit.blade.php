@extends('adminlte::page')

@section('content_header')
    <h1>Anunciantes <small>Listado</small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if (session('success'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success">{{ session('success') }}</div>
                    </div>
                </div>
            @endif
            @if (session('error'))
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger">{{ session('error') }}</div>
                    </div>
                </div>
            @endif
            <h3>Editar categoria</h3>
            {{ Form::model($category, ['method' => 'post' , 'action' => ['Admin\BusinessController@categoriesUpdate',$category->id]]) }}
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group {{$errors->first('name', ' has-error')}}">
                            <label for="exampleInputEmail1">Nombre</label>
                            {{ Form::text('name', null,['class' => 'form-control','required']) }}
                        </div>

                    </div>
                </div>
                <div class="text-right">
                    <button class="btn btn-primary">Guardar</button>
                </div>
            {{ Form::close() }}
        </div>
    </div>
@stop

@section('js')
    <script>


    </script>
@stop
