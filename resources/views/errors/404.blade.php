
<!doctype html>
<html lang="en">
<head>
    <title>Gotta Center - by Andrés Gotta Golf</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Font -->
    <link rel="dns-prefetch" href="//fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css?family=Chivo:400,700,900" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('404/css/bootstrap.min.css') }}">
    <!-- Themify Icons -->
    <link rel="stylesheet" href="{{ asset('404/css/themify-icons.css') }}">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="{{ asset('404/css/owl.carousel.min.css') }}">
    <!-- Main css -->
    <link href="{{ asset('404/css/style.css') }}" rel="stylesheet">
</head>

<body data-spy="scroll" data-target="#navbar" data-offset="30">
<div class="section bg-gradient" id="download">
    <div class="container">
        <div class="call-to-action">
            <div class="box-icon"><span class="ti-settings gradient-fill ti-3x"></span></div>
            <h2>Página no encontrada</h2>
        </div>
        <div class="call-to-action">
            <div class="my-4">
                <a href="{{ url('/') }}" class="btn btn-light">Volver</a>
            </div>
        </div>
    </div>
</div>
<div class="client-logos my-5">
    <div class="container text-center">
        <a href="http://www.sportsintouch.com.ar" target="_blank" class="logo-cliente-1">
            <img src="{{ asset('404/images/sportsintouch_gris.png') }}" alt="client logos" class="img-fluid logo-gris-1">
            <img src="{{ asset('404/images/sportsintouch.png') }}" alt="client logos" class="img-fluid logo-color-1">
        </a>
        <a href="http://www.andresgotta.com.ar" target="_blank" class="logo-cliente-2">
            <img src="{{ asset('404/images/andresgotta_gris.png') }}" alt="client logos" class="img-fluid logo-gris-2">
            <img src="{{ asset('404/images/andresgotta.png') }}" alt="client logos" class="img-fluid logo-color-2">
        </a>
        <a href="http://www.gpstats.com.ar" target="_blank" class="logo-cliente-1">
            <img src="{{ asset('404/images/gpstats_gris.png') }}" alt="client logos" class="img-fluid logo-gris-1">
            <img src="{{ asset('404/images/gpstats.png') }}" alt="client logos" class="img-fluid logo-color-1">
        </a>
    </div>
</div>
</body>
</html>