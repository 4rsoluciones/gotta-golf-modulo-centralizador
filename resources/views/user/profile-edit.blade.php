@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form method="post" id="editUser" action="{{ url('profile/edit/'.$profile->id) }}">
                            {{ csrf_field() }}
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Datos</a></li>
                                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Servicios</a></li>
                                </ul>
                                <br>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label for="first_name">Nombre</label>
                                                <input type="text" name="first_name" id="first_name" class="form-control" value="{{ $profile->profile->first_name}}" >
                                            </div>
                                            <div class="form-group">
                                                <label for="last_name">Apellido</label>
                                                <input type="text" name="last_name" id="last_name" class="form-control" value="{{ $profile->profile->last_name }}" >
                                            </div>
                                            <div class="form-group">
                                                <label for="username">Nombre de ususario</label>
                                                <span id="username" class="form-control" >{{ $profile->username }}</span>
                                            </div>
                                            <div class="form-group{{ $errors->has('old_password') ? ' has-error' : '' }}">
                                                <label for="last_name">Contraseña actual</label>
                                                <input type="password" name="old_password" id="old_password" class="form-control" value="" >
                                                @if ($errors->has('old_password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('old_password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
                                                <label for="last_name">Contraseña nueva</label>
                                                <input type="password" name="new_password" id="new_password" class="form-control" value="" >
                                                @if ($errors->has('new_password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('new_password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group{{ $errors->has('new_password_confirmation') ? ' has-error' : '' }}">
                                                <label for="last_name">Confirme la nueva contraseña</label>
                                                <input type="password" name="new_password_confirmation" id="new_password_confirmation" class="form-control" value="" >
                                                @if ($errors->has('new_password_confirmation'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('new_password_confirmation') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label for="email">Email</label>
                                                <input type="email" name="email" id="email" class="form-control" value="{{ $profile->email }}" >
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label for="country">País</label>
                                                <select name="country" id="country" class="form-control" autocomplete="off">
                                                    <option>-- Seleccione una país --</option>
                                                    @foreach($countries as $k => $country)
                                                        <option value="{{ $k }}"  @if($profile->profile->state && $profile->profile->state->country->name == $country) selected @endif>{{ $country }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="state_id">Provincia</label>
                                                <select class="form-control" id="state" name="state_id" autocomplete="off">
                                                    <option value="">-- Seleccione una provincia --</option>
                                                    @if(!empty($states))
                                                        @foreach($states as $k => $state)
                                                            <option value="{{ $k }}" @if($k == $profile->profile->state->id) selected @endif>{{ $state }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="city">Localidad</label>
                                                <input type="text" name="city" id="city" class="form-control" value="{{ $profile->profile->city }}" >
                                            </div>
                                            <div class="form-group">
                                                <label for="birth_date">Fecha de Nacimiento</label>
                                                <input type="date" name="birth_date" id="birth_date" class="form-control" value="{{ $profile->profile->birth_date }}" >
                                            </div>
                                            <div class="form-group">
                                                <label for="gender">Género</label>
                                                <select class="form-control" id="gender" name="gender" autocomplete="off">
                                                    <option value="">-- Seleccione un sexo --</option>
                                                    @foreach($genders as $id => $gender)
                                                        <option value="{{ $id }}" @if($profile->profile->gender == $id) selected @endif>{{ $gender }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="mobile_phonenumber">Teléfono Celular</label>
                                                <input type="text" id="mobile_phonenumber" class="form-control" value="{{ $profile->profile->mobile_phonenumber }}" >
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.tab-pane -->
                                    <div class="tab-pane" id="tab_2">
                                        <div class="box-body">
                                            @if (count($profile->services))
                                                <div class="dataTables_wrapper">
                                                    <table id="services" class="table table-bordered table-hover dataTable">
                                                        <thead>
                                                        <tr>
                                                            <th>Plataforma</th>
                                                            <th>Nombre</th>
                                                            <th>Precio</th>
                                                            <th>Status</th>
                                                            <th>Acciones</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($profile->services as $service)
                                                            <tr>
                                                                <td>{{ $service->platforms()->first()->name }}</td>
                                                                <td>{{ $service->area->name . ' - ' . $service->name }}</td>
                                                                <td>{{ $service->price }}</td>
                                                                <td>{{ $service->pivot->active ? 'Activo' : 'Inactivo' }}</td>
                                                                <td>
                                                                    <input
                                                                            type="button"
                                                                            class="form-control btn btn-flat btn-success service-action"
                                                                            data-toggle="modal"
                                                                            data-target="#myModal"
                                                                            data-action="{{ $service->pivot->active }}"
                                                                            data-id="{{ $service->id }}"
                                                                            data-price="{{ $service->price }}"
                                                                            value="{{ $service->pivot->active ? 'Desafiliar' : 'Afiliar' }}">
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            @else
                                                <h3>Ningún servicio ha sido contratado</h3>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- /.tab-pane -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="button" class="form-control btn btn-flat btn-info" value="Volver" onclick="location.href ='{{ url('/') }}'">
                                    </div>
                                </div>
                                <div class="col-md-3 col-md-offset-6">
                                    <div class="form-group">
                                        <input type="submit" class="form-control btn btn-flat btn-success" value="Guardar">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <form id="modal-form" method="post">
                {{ csrf_field() }}
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"></h4>
                    </div>
                    <div class="modal-body" id="myModalBody">
                    </div>
                    <div class="modal-footer">
                        <input type="hidden"  id="service-id" value="">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success" id="send-btn"></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(function (){
            $('#country').change(function(){
                $.get('{{ url('states') }}',
                    { country: $(this).val() },
                    function(data) {
                        var model = $('#state');
                        model.empty();
                        $('#state').append("<option>-- Seleccione una provincia --</option>");
                        $.each(data, function(index, element) {
                            $('#state').append("<option value='"+ index +"'>" + element + "</option>");
                        });
                    });
            });

            $(document).on('click', '.service-action', function () {
               if ($(this).data('action') == 1){
                   $('#myModalLabel').html('Desafiliación de servicio');
                   $('#myModalBody').html('<p>¿Está seguro que desea desafiliarse del servicio?</p>\n' +
                       '                   <p>Recuerde que al desafiliarce pierde todo los días pagos no utilizados.</p>');
                   $('#send-btn').html('Desafiliar');
                   $('#modal-form').prop('action', '{{ url('/services/unsubscribe') }}/'+$(this).data('id'));
               } else {
                   $('#myModalLabel').html('Afiliación de servicio');
                   $('#myModalBody').html('<p>Se le empezará a debitar mensualmente nuevamente este servicio a la tarjeta ingresada originalmente.</p>');
                   $('#send-btn').html('Afiliar');
                   $('#modal-form').prop('action', '{{ url('/services/resubscribe') }}/'+$(this).data('id'));
               }

               $('#service-id').val($(this).data('id'));
            });
        });
    </script>
@stop