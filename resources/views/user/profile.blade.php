@extends('layouts.app')

@section('content')
    <div class="container">
        @if(session('success'))
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="alert alert-success">{{ session('success') }}</div>
                </div>
            </div>
        @endif
            @if(session('error'))
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="alert alert-danger">{{ session('error') }}</div>
                    </div>
                </div>
            @endif
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Datos</a></li>
                                <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Servicios</a></li>
                            </ul>
                            <br>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="first_name">Nombre</label>
                                            <span id="first_name" class="form-control" >{{ $profile->profile->first_name}}</span>
                                        </div>
                                        <div class="form-group">
                                            <label for="last_name">Apellido</label>
                                            <span id="last_name" class="form-control" >{{ $profile->profile->last_name }}</span>
                                        </div>
                                        <div class="form-group">
                                            <label for="username">Nombre de ususario</label>
                                            <span id="username" class="form-control" >{{ $profile->username }}</span>
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <span id="email" class="form-control" >{{ $profile->email }}</span>
                                        </div>
                                        <div class="form-group">
                                            <label for="country">País</label>
                                            <span id="country" class="form-control" >{{ $profile->profile->state ? $profile->profile->state->country->name : '' }}</span>
                                        </div>
                                        <div class="form-group">
                                            <label for="state_id">Provincia</label>
                                            <span id="state_id" class="form-control" >{{ $profile->profile->state ? $profile->profile->state->name : '' }}</span>
                                        </div>
                                        <div class="form-group">
                                            <label for="city">Localidad</label>
                                            <span id="city" class="form-control" >{{ $profile->profile->city }}</span>
                                        </div>
                                        <div class="form-group">
                                            <label for="birth_date">Fecha de Nacimiento</label>
                                            <span id="birth_date" class="form-control" >{{ $profile->profile->birth_date }}</span>
                                        </div>
                                        <div class="form-group">
                                            <label for="gender">Género</label>
                                            <span id="gender" class="form-control" >{{ $profile->profile->gender == 'male' ? 'Masculino' : 'Femenino' }}</span>
                                        </div>
                                        <div class="form-group">
                                            <label for="mobile_phonenumber">Teléfono Celular</label>
                                            <span id="mobile_phonenumber" class="form-control" >{{ $profile->profile->mobile_phonenumber }}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_2">
                                    <div class="box-body">
                                        @if (count($profile->services))
                                            <div class="dataTables_wrapper">
                                                <table id="services" class="table table-bordered table-hover dataTable">
                                                    <thead>
                                                    <tr>
                                                        <th>Plataforma</th>
                                                        <th>Nombre</th>
                                                        <th>Precio</th>
                                                        <th>Status</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($profile->services as $service)
                                                        <tr>
                                                            <td>{{ $service->platforms()->first()->name }}</td>
                                                            <td>{{ $service->area->name . ' - ' . $service->name }}</td>
                                                            <td>{{ $service->price }}</td>
                                                            <td>{{ $service->pivot->active ? 'Activo' : 'Inactivo' }}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        @else
                                            <h3>Ningún servicio ha sido contratado</h3>
                                        @endif
                                    </div>
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="button" class="form-control btn btn-flat btn-info" value="Volver" onclick="location.href ='{{ url('/') }}'">
                                </div>
                            </div>
                            <div class="col-md-3 col-md-offset-6">
                                <div class="form-group">
                                    <input type="button" class="form-control btn btn-flat btn-success" value="Editar" onclick="location.href ='{{ url('profile/edit/'.$profile->id) }}'">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection