<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->text('image')->nullable();
            $table->integer('stock')->unsigned()->default(0);
            $table->integer('status')->unsigned()->default(0);
            $table->string('price')->nullable();
            $table->string('price_old')->nullable();
            $table->string('discount')->nullable();
            $table->integer('business_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->date('date_active')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
