<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->integer('state_id')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('city')->nullable();
            $table->string('company_name')->nullable();
            $table->date('birth_date')->default(date('Y-m-d'));
            $table->string('gender')->default('male');
            $table->string('mobile_phonenumber')->nullable();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->dropForeign([
                'user_id'
            ]);

            $table->dropColumn([
                'state_id',
                'city',
                'company_name',
                'birth_date',
                'gender',
                'mobile_phonenumber'
            ]);
        });
    }
}
