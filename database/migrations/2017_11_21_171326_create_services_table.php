<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->collation('utf8_unicode_ci')->unique();
            $table->string('description')->collation('utf8_unicode_ci');
            $table->text('long_description')->collation('utf8_unicode_ci');
            $table->decimal('price')->unsigned();
            $table->boolean('highlighted');
            $table->boolean('important');
            $table->boolean('visible');
            $table->text('type')->collation('utf8_unicode_ci');
            $table->integer('days')->unsigned();
            $table->integer('trial_days')->unsigned();
            $table->boolean('is_lifetime');
            $table->integer('debitDues')->unsigned();
            $table->integer('service_type_id')->unsigned();
            $table->integer('area_id')->unsigned();
            $table->timestamps();

            $table->foreign('service_type_id')
                ->references('id')
                ->on('service_types')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('area_id')
                ->references('id')
                ->on('areas')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->dropForeign(['service_type_id']);
            $table->dropForeign(['area_id']);
        });

        Schema::dropIfExists('services');
    }
}
