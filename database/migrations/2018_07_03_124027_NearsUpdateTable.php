<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NearsUpdateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nears', function (Blueprint $table) {
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->text('image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nears', function (Blueprint $table) {
            $table->dropColumn('state');
            $table->dropColumn('country');
            $table->dropColumn('image');
        });
    }
}
