<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTearsFieldToServicesPlatformsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services_platforms', function (Blueprint $table) {
            $table->integer('pack_id')->unsigned()->nullable();

            $table->foreign('pack_id')
                ->references('id')
                ->on('packs')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services_platforms', function (Blueprint $table) {
            $table->dropForeign(['pack_id']);

            $table->dropColumn('pack_id');
        });
    }
}
