<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActiveFieldToSerivicesPlatformsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('services_platforms', 'active')) {
            Schema::table('services_platforms', function (Blueprint $table) {
                $table->boolean('active')->unsigned()->nullable()->default(1);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services_platforms', function (Blueprint $table) {
            $table->dropColumn('active');
        });
    }
}
