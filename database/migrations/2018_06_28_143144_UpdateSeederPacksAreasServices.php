<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSeederPacksAreasServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Artisan::call('db:seed', array('--class' => 'PlatformPacksSeeder'));
        Artisan::call('db:seed', array('--class' => 'AreasSeeder'));
        Artisan::call('db:seed', array('--class' => 'PacksSeeder'));
        Artisan::call('db:seed', array('--class' => 'UpdateServicesPlatformsSeed'));
        Artisan::call('db:seed', array('--class' => 'UpdateServicesAreasSeed'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
