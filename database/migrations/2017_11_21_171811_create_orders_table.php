<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_status_id')->unsigned();
            $table->integer('promo_code_id')->unsigned();
            $table->boolean('pay_cash');
            $table->timestamps();

            $table->foreign('order_status_id')
                ->references('id')
                ->on('order_status')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('promo_code_id')
                ->references('id')
                ->on('promo_codes')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['order_status_id']);
            $table->dropForeign(['promo_code_id']);
        });

        Schema::dropIfExists('orders');
    }
}
