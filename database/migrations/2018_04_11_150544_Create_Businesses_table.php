<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->string('business_name')->nullable();
            $table->string('description');
            $table->string('cuit')->unique();
            $table->string('web')->nullable();
            $table->string('logo')->nullable();
            $table->string('status')->nullable();
            $table->integer('business_category_id')->unsigned();
            $table->integer('user_id')->unsigned()->index();
            $table->timestamps();

            $table->foreign('business_category_id')
                ->references('id')
                ->on('business_categories')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('businesses', function (Blueprint $table) {
            $table->dropForeign(['business_category_id']);

            $table->dropForeign(['user_id']);
        });

        Schema::dropIfExists('businesses');
    }
}
