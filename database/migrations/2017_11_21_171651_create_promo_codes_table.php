<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromoCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->integer('discount')->unsigned();
            $table->date('valid_until');
            $table->integer('promo_code_type_id')->unsigned();

            $table->foreign('promo_code_type_id')
                ->references('id')
                ->on('promo_code_types')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promo_codes', function (Blueprint $table) {
            $table->dropForeign([
                'promo_code_type_id',
            ]);
        });

        Schema::dropIfExists('promo_codes');
    }
}
