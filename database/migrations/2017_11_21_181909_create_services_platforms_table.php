<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesPlatformsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services_platforms', function (Blueprint $table) {
            $table->integer('service_id')->unsigned();
            $table->integer('platform_id')->unsigned();

            $table->foreign('service_id')
                ->references('id')
                ->on('services')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('platform_id')
                ->references('id')
                ->on('platforms')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services_platforms', function (Blueprint $table) {
            $table->dropForeign(['service_id']);
            $table->dropForeign(['platform_id']);
        });

        Schema::dropIfExists('services_platforms');
    }
}
