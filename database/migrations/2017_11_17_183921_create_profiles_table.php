<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->boolean('enabled');
            $table->dateTime('last_login')->nullable()->default(null);
            $table->boolean('locked');
            $table->boolean('expired');
            $table->dateTime('expires_at')->nullable()->default(null);
            $table->string('confirmation_token')
                ->collation('utf8_unicode_ci')
                ->nullable()
                ->default(null);
            $table->dateTime('password_requested_at')->nullable()->default(null);
            $table->text('roles')->collation('utf8_unicode_ci');
            $table->boolean('credentials_expired');
            $table->dateTime('credentials_expire_at')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
