<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromoCodesServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo_codes_services', function (Blueprint $table) {
            $table->integer('promo_code_id')->unsigned();
            $table->integer('service_id')->unsigned();

            $table->foreign('promo_code_id')
                ->references('id')
                ->on('promo_codes')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('service_id')
                ->references('id')
                ->on('services')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promo_codes_services', function (Blueprint $table) {
            $table->dropForeign(['promo_code_id']);
            $table->dropForeign(['service_id']);
        });

        Schema::dropIfExists('promo_codes_services');
    }
}
