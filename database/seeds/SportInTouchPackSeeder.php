<?php

use Illuminate\Database\Seeder;

class SportInTouchPackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $platform = \App\Platform::where('name', 'Sport in Touch')->first();

        if ($platform) {
            \App\PlatformPack::create([
                'name' => 'Sedes',
                'type' => 'integer',
                'platform_id' => $platform->id
            ]);

            \App\PlatformPack::create([
                'name' => 'Profesores',
                'type' => 'integer',
                'platform_id' => $platform->id
            ]);

            \App\PlatformPack::create([
                'name' => 'Alumnos',
                'type' => 'integer',
                'platform_id' => $platform->id
            ]);
        }
    }
}
