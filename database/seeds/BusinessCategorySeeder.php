<?php

use Illuminate\Database\Seeder;

class BusinessCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\BusinessCategory::firstOrCreate([
            'name' => 'Accesorios de Golf',
        ]);

        \App\BusinessCategory::firstOrCreate([
            'name' => 'Indumentaria Deportiva',
        ]);

        \App\BusinessCategory::firstOrCreate([
            'name' => 'Campos',
        ]);

        \App\BusinessCategory::firstOrCreate([
            'name' => 'Bares',
        ]);

        \App\BusinessCategory::firstOrCreate([
            'name' => 'Restaurantes',
        ]);

        \App\BusinessCategory::firstOrCreate([
            'name' => 'Hoteles',
        ]);

        \App\BusinessCategory::firstOrCreate([
            'name' => 'Otros',
        ]);
    }
}
