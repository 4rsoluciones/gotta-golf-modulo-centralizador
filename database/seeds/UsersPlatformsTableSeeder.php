<?php

use Illuminate\Database\Seeder;

class UsersPlatformsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_platforms')->insert([
            'user_id' => 1,
            'platform_id' => 1
        ]);
    }
}
