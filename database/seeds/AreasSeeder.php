<?php

use Illuminate\Database\Seeder;

class AreasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('areas')->insert([
            'name' => 'Personal',
            'slug' => 'personal',
            'description' => '',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('areas')->insert([
            'name' => 'Academia Individual',
            'slug' => 'academia_individual ',
            'description' => '',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('areas')->insert([
            'name' => 'Academia Equipos',
            'slug' => 'academia_equipos',
            'description' => '',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
