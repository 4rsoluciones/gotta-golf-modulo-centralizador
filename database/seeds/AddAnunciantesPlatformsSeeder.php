<?php

use Illuminate\Database\Seeder;

class AddAnunciantesPlatformsSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        //Platform
        $idPlatform = DB::table('platforms')->insertGetId([
            'name' => 'Anunciantes'
        ]);
        //Platform fields
        DB::table('platform_packs')->insert([
            'name' => 'Negocios',
            'type' => 'number',
            'platform_id' => $idPlatform
        ]);
        DB::table('platform_packs')->insert([
            'name' => 'Cupones',
            'type' => 'number',
            'platform_id' => $idPlatform
        ]);
        DB::table('platform_packs')->insert([
            'name' => 'Sucursales',
            'type' => 'number',
            'platform_id' => $idPlatform
        ]);
        //Areas
        $idAreaBeneficios = DB::table('areas')->insertGetId([
            'name' => 'Beneficios',
            'slug' => 'beneficios',
            'description' => '',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        $idAreaCercaMio = DB::table('areas')->insertGetId([
            'name' => 'Cerca mio',
            'slug' => 'cerca_mio',
            'description' => '',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        //Packs
        DB::table('packs')->insertGetId([
            'name' => 'Beneficios - Basico',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('packs')->insertGetId([
            'name' => 'Cerca Mio - Basico',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
