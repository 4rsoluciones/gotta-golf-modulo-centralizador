<?php

use Illuminate\Database\Seeder;

class PaymentTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_types')->insert([
            [
                'id' => 1,
                'name' => 'Tarjeta de Crédito'
            ],
            [
                'id' => 2,
                'name' => 'Visa Credito'
            ],
            [
                'id' => 3,
                'name' => 'Visa Debito'
            ],
            [
                'id' => 4,
                'name' => 'Tarjeta de Débito'
            ],
            [
                'id' => 5,
                'name' => 'Efectivo'
            ],
            [
                'id' => 6,
                'name' => 'Depósito Bancario'
            ]
        ]);
    }
}
