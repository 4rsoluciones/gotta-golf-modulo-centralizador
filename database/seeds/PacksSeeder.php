<?php

use Illuminate\Database\Seeder;

class PacksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('packs')->insert([
            'id' => 1,
            'name' => 'Personal - Básico',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Sedes',
            'value' => 1,
            'pack_id' => 1,
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Profesor',
            'value' => 1,
            'pack_id' => 1,
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Alumnos',
            'value' => 20,
            'pack_id' => 1,
        ]);
        //-----------
        DB::table('packs')->insert([
            'id' => 2,
            'name' => 'Personal - Medio',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Sedes',
            'value' => 1,
            'pack_id' => 2,
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Profesor',
            'value' => 1,
            'pack_id' => 2,
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Alumnos',
            'value' => 40,
            'pack_id' => 2,
        ]);
        //-----------
        DB::table('packs')->insert([
            'id' => 3,
            'name' => 'Personal - Profesional',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Sedes',
            'value' => 1,
            'pack_id' => 3,
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Profesor',
            'value' => 1,
            'pack_id' => 3,
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Alumnos',
            'value' => 0,
            'pack_id' => 3,
        ]);
        //-----------
        DB::table('packs')->insert([
            'id' => 4,
            'name' => 'Academia Individual - Básico',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Sedes',
            'value' => 1,
            'pack_id' => 4,
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Profesor',
            'value' => 2,
            'pack_id' => 4,
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Alumnos',
            'value' => 30,
            'pack_id' => 4,
        ]);
        //-----------
        DB::table('packs')->insert([
            'id' => 5,
            'name' => 'Academia Individual - Medio',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Sedes',
            'value' => 1,
            'pack_id' => 5,
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Profesor',
            'value' => 5,
            'pack_id' => 1,
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Alumnos',
            'value' => 50,
            'pack_id' => 5,
        ]);
        //-----------
        DB::table('packs')->insert([
            'id' => 6,
            'name' => 'Academia Individual - Profesional',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Sedes',
            'value' => 1,
            'pack_id' => 6,
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Profesor',
            'value' => 10,
            'pack_id' => 6,
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Alumnos',
            'value' => 0,
            'pack_id' => 6,
        ]);
        //-----------
        DB::table('packs')->insert([
            'id' => 7,
            'name' => 'Academia Equipos - Básico',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Sedes',
            'value' => 2,
            'pack_id' => 7,
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Profesor',
            'value' => 2,
            'pack_id' => 7,
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Alumnos',
            'value' => 30,
            'pack_id' => 7,
        ]);
        //-----------
        DB::table('packs')->insert([
            'id' => 8,
            'name' => 'Academia Equipos - Medio',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Sedes',
            'value' => 4,
            'pack_id' => 8,
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Profesor',
            'value' => 5,
            'pack_id' => 8,
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Alumnos',
            'value' => 75,
            'pack_id' => 8,
        ]);
        //-----------
        DB::table('packs')->insert([
            'id' => 9,
            'name' => 'Academia Equipos - Profesional',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Sedes',
            'value' => 10,
            'pack_id' => 9,
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Profesor',
            'value' => 15,
            'pack_id' => 9,
        ]);
        DB::table('pack_fields')->insert([
            'name' => 'Alumnos',
            'value' => 0,
            'pack_id' => 9,
        ]);
    }
}
