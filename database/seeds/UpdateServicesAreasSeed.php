<?php

use Illuminate\Database\Seeder;

class UpdateServicesAreasSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->where('id', 1)->update(['area_id' => 2,'order'=>1]);
        DB::table('services')->where('id', 2)->update(['area_id' => 2,'order'=>2]);
        DB::table('services')->where('id', 3)->update(['area_id' => 2,'order'=>3]);

        DB::table('services')->where('id', 4)->update(['area_id' => 3,'order'=>4]);
        DB::table('services')->where('id', 5)->update(['area_id' => 3,'order'=>5]);
        DB::table('services')->where('id', 6)->update(['area_id' => 3,'order'=>6]);

        DB::table('services')->where('id', 7)->update(['area_id' => 4,'order'=>7]);
        DB::table('services')->where('id', 8)->update(['area_id' => 4,'order'=>8]);
        DB::table('services')->where('id', 9)->update(['area_id' => 4,'order'=>9]);
    }
}
