<?php

use Illuminate\Database\Seeder;

class OldUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $oldUsers = DB::connection('old_gotta')
            ->table('users')
            ->join('profiles', 'users.id', '=', 'profiles.user_id')
            ->select('users.*', 'profiles.*')
            ->get();


        if ($oldUsers) {
            foreach ($oldUsers as $user) {
                $profile = \App\Profile::firstOrCreate([
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'enabled' => $user->enabled,
                    'locked' => $user->locked,
                    'expired' => $user->expired,
                    'confirmation_token' => $user->confirmation_token,
                    'roles' => $user->roles,
                    'credentials_expired' => $user->credentials_expired,
                    'state_id' => $user->state_id,
                    'city' => $user->city,
                    'company_name' => $user->company_name,
                    'birth_date' => $user->birth_date,
                    'gender' => $user->gender,
                    'mobile_phonenumber' => $user->mobile
                ]);

                \App\User::firstOrCreate([
                    'email' => $user->email
                ],
                [
                    'email' => $user->email,
                    'password' => $user->password,
                    'created_at' => $user->created_at,
                    'updated_at' => $user->updated_at,
                    'username' => $user->username,
                    'salt' => $user->salt,
                    'profile_id' => $profile->id
                ]);
            }
        }
    }
}
