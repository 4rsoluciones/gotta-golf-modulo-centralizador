<?php

use Illuminate\Database\Seeder;

class GpStatsPackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $platform = \App\Platform::where('name', 'GP Stats')->first();

        if ($platform) {
            \App\PlatformPack::create([
                'name' => 'Comparativas',
                'type' => 'integer',
                'platform_id' => $platform->id
            ]);

            \App\PlatformPack::create([
                'name' => 'Vueltas por mes',
                'type' => 'integer',
                'platform_id' => $platform->id
            ]);

            \App\PlatformPack::create([
                'name' => 'Reportes personales',
                'type' => 'integer',
                'platform_id' => $platform->id
            ]);

            \App\PlatformPack::create([
                'name' => 'Grupo cerrado',
                'type' => 'integer',
                'platform_id' => $platform->id
            ]);
        }
    }
}
