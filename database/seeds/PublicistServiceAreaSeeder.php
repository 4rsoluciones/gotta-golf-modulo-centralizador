<?php

use Illuminate\Database\Seeder;

class PublicistServiceAreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Area::firstOrCreate([
            'name' => 'Anunciantes',
            'slug' => 'anunciantes',
            'description' => 'Servicios para anunciar negocios dentro de las aplicaciones',
        ]);
    }
}
