<?php

use Illuminate\Database\Seeder;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert([
            'id' => 1,
            'first_name' => 'admin',
            'last_name' => 'admin',
            'enabled' => 1,
            'last_login' => date('Y-m-d H:i:s'),
            'locked' => 0,
            'expired' => 0,
            'expires_at' => null,
            'confirmation_token' => null,
            'password_requested_at' => null,
            'roles' => 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}',
            'credentials_expired' => 0,
            'credentials_expire_at' => null,
            'state_id' => 1,
        ]);

        DB::table('profiles')->insert([
            'id' => 2,
            'first_name' => 'teacher',
            'last_name' => 'teacher',
            'enabled' => 1,
            'last_login' => date('Y-m-d H:i:s'),
            'locked' => 0,
            'expired' => 0,
            'expires_at' => null,
            'confirmation_token' => null,
            'password_requested_at' => null,
            'roles' => 'a:1:{i:0;s:12:"ROLE_TEACHER";}',
            'credentials_expired' => 0,
            'credentials_expire_at' => null,
            'state_id' => 1,
        ]);


        DB::table('profiles')->insert([
            'id' => 3,
            'first_name' => 'student',
            'last_name' => 'student',
            'enabled' => 1,
            'last_login' => date('Y-m-d H:i:s'),
            'locked' => 0,
            'expired' => 0,
            'expires_at' => null,
            'confirmation_token' => null,
            'password_requested_at' => null,
            'roles' => 'a:1:{i:0;s:12:"ROLE_STUDENT";}',
            'credentials_expired' => 0,
            'credentials_expire_at' => null,
            'state_id' => 1,
        ]);

        DB::table('profiles')->insert([
            'id' => 4,
            'first_name' => 'player',
            'last_name' => 'player',
            'enabled' => 1,
            'last_login' => date('Y-m-d H:i:s'),
            'locked' => 0,
            'expired' => 0,
            'expires_at' => null,
            'confirmation_token' => null,
            'password_requested_at' => null,
            'roles' => 'a:1:{i:0;s:11:"ROLE_PLAYER";}',
            'credentials_expired' => 0,
            'credentials_expire_at' => null,
            'state_id' => 1,
        ]);

        DB::table('profiles')->insert([
            'id' => 5,
            'first_name' => 'academy',
            'last_name' => 'academy',
            'enabled' => 1,
            'last_login' => date('Y-m-d H:i:s'),
            'locked' => 0,
            'expired' => 0,
            'expires_at' => null,
            'confirmation_token' => null,
            'password_requested_at' => null,
            'roles' => 'a:1:{i:0;s:12:"ROLE_ACADEMY";}',
            'credentials_expired' => 0,
            'credentials_expire_at' => null,
            'state_id' => 1,
        ]);
    }
}
