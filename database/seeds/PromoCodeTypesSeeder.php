<?php

use Illuminate\Database\Seeder;

class PromoCodeTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('promo_code_types')->insert([
            [
                'id' => 1,
                'name' => 'Descuento'
            ],
            [
                'id' => 2,
                'name' => 'Bono'
            ],
        ]);
    }
}
