<?php

use Illuminate\Database\Seeder;

class UpdateServicesPlatformsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services_platforms')->where('service_id', 1)->update(['pack_id' => 1]);
        DB::table('services_platforms')->where('service_id', 2)->update(['pack_id' => 2]);
        DB::table('services_platforms')->where('service_id', 3)->update(['pack_id' => 3]);
        DB::table('services_platforms')->where('service_id', 4)->update(['pack_id' => 4]);
        DB::table('services_platforms')->where('service_id', 5)->update(['pack_id' => 5]);
        DB::table('services_platforms')->where('service_id', 6)->update(['pack_id' => 6]);
        DB::table('services_platforms')->where('service_id', 7)->update(['pack_id' => 7]);
        DB::table('services_platforms')->where('service_id', 8)->update(['pack_id' => 8]);
        DB::table('services_platforms')->where('service_id', 9)->update(['pack_id' => 9]);
    }
}
