<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlatformPacksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('platform_packs')->insert([
            'name' => 'Sedes',
            'type' => 'number',
            'platform_id' => 2,
        ]);
        DB::table('platform_packs')->insert([
            'name' => 'Profesores',
            'type' => 'number',
            'platform_id' => 2,
        ]);
        DB::table('platform_packs')->insert([
            'name' => 'Alumnos',
            'type' => 'number',
            'platform_id' => 2,
        ]);
    }
}
