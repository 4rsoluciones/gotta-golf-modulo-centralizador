<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProfilesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PlatformsTableSeeder::class);
        $this->call(UsersPlatformsTableSeeder::class);
        $this->call(ServicesTypeSeeder::class);
        $this->call(OrderStatusSeeder::class);
        $this->call(PaymentTypesSeeder::class);
        $this->call(PromoCodeTypesSeeder::class);
        $this->call(OldUserSeeder::class);
        $this->call(BusinessCategorySeeder::class);
        $this->call(PublicistServiceAreaSeeder::class);
    }
}
