<?php

use Illuminate\Database\Seeder;

class SetServiceToAcademySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = \App\Service::get();

        $user = \App\User::where('email', 'academy@example.com')->first();

        $activeServices = DB::table('users_services')->where('user_id', $user->id)->pluck('service_id')->toArray();

        if ($services && $user) {
            foreach ($services as $service) {
                if (!in_array($service->id, $activeServices)) {
                    DB::table('users_services')
                        ->insert([
                            'user_id' => $user->id,
                            'service_id' => $service->id,
                            'active' => true
                        ]);
                }
            }
        }
    }
}
