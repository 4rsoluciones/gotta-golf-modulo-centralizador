<?php

use Illuminate\Database\Seeder;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_status')->insert([
            [
                'id' => 1,
                'name' => 'Pagado'
            ],
            [
                'id' => 2,
                'name' => 'Pendiente'
            ],
            [
                'id' => 3,
                'name' => 'Devuelto'
            ],
            [
                'id' => 4,
                'name' => 'Rechazado'
            ]
        ]);
    }
}
