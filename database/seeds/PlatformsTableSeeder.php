<?php

use Illuminate\Database\Seeder;

class PlatformsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('platforms')->insert([
            [
                'id' => 1,
                'name' => 'GP Stats'
            ],
            [
                'id' => 2,
                'name' => 'Sport in Touch'
            ]
        ]);
    }
}
