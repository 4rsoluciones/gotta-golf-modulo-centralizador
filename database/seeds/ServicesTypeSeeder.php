<?php

use Illuminate\Database\Seeder;

class ServicesTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('service_types')->insert([
            [
                'id' => 1,
                'name' => 'Estadístico'
            ],
            [
                'id' => 2,
                'name' => 'Clases'
            ]
        ]);
    }
}
