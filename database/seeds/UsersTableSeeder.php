<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $encoder = new \App\SymfonyDigestPasswordEncoder();
        $salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);

        DB::table('users')->insert([
            'id' => 1,
            'email' => 'superadmin@gotta.com',
            'salt' => $salt,
            'password' => $encoder->encodePassword('123456', $salt),
            'username' => 'admin',
            'profile_id' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);


        DB::table('users')->insert([
            'id' => 2,
            'email' => 'teacher@example.com',
            'salt' => $salt,
            'password' => $encoder->encodePassword('teacher', $salt),
            'username' => 'teacher',
            'profile_id' => 2,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'id' => 3,
            'email' => 'student@example.com',
            'salt' => $salt,
            'password' => $encoder->encodePassword('student', $salt),
            'username' => 'student',
            'profile_id' => 3,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'id' => 4,
            'email' => 'player@example.com',
            'salt' => $salt,
            'password' => $encoder->encodePassword('player', $salt),
            'username' => 'player-center',
            'profile_id' => 4,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'id' => 5,
            'email' => 'academy@example.com',
            'salt' => $salt,
            'password' => $encoder->encodePassword('academy', $salt),
            'username' => 'academy',
            'profile_id' => 5,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
