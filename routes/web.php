<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'FrontController@index');

Route::get('/states', 'Auth\RegisterController@getStates');
Route::get('/roles', 'Admin\UserController@getRolesAjax');

Route::get('/packs', 'Admin\ServiceController@getPacks');

Route::get('sha512', 'FrontController@sha512Password');

/**
 * Platforms
 */
Route::get('platform/{id}', 'ServiceController@platform');
Route::get('platform/{idPlatform}/area/{idArea}', 'ServiceController@platformsArea');

/**
 * Users
 */
Route::get('/profile', 'ProfileController@index')->name('profile');
Route::get('/profile/edit/{id}', 'ProfileController@edit')->name('profile.edit')->where('id', '[0-9]+');
Route::post('/profile/edit/{id}', 'ProfileController@update')->name('profile.update')->where('id', '[0-9]+');

/**
 * Services
 */
Route::get('service', 'FrontController@services');
Route::get('services', 'ServiceController@index')->name('services.list');
Route::get('subscription', 'FrontController@services');
Route::get('cart', 'FrontController@services');
Route::get('order', 'FrontController@services');
Route::get('areas/services/{id}', 'ServiceController@area')->where('id', '[0-9]+');
Route::get('services/{id}/{platform?}', 'ServiceController@show')->where('id', '[0-9]+');
Route::post('services/pay/{id}', 'ServiceController@pay')->where('id', '[0-9]+');
Route::post('services/unsubscribe/{id}', 'ServiceController@unsubscribe')->where('id', '[0-9]+');
Route::post('services/resubscribe/{id}', 'ServiceController@resubscribe')->where('id', '[0-9]+');
Route::get('serviceslist/{email}', 'ServiceController@listServices');

/**
 * Admin
 */
Route::prefix('admin')->group(function () {
    Route::get('/', 'Admin\HomeController@index')->name('admin');

    /**
     * Login
     */
    Route::get('login', 'Admin\LoginController@showLoginForm');
    Route::post('logout', 'Admin\LoginController@logout');
    Route::get('/test', 'Admin\HomeController@index');

    /**
     * Users
     */
    Route::get('users', 'Admin\UserController@index')->name('admin.users');
    Route::get('users/{id}', 'Admin\UserController@show')->where('id', '[0-9]+');
    Route::get('users/edit/{id}', 'Admin\UserController@edit')->where('id', '[0-9]+');
    Route::post('users/edit/{id}', 'Admin\UserController@update')->where('id', '[0-9]+');
    Route::get('users/new', 'Admin\UserController@create');
    Route::post('users/new', 'Admin\UserController@store');
    Route::get('users/import', 'Admin\UserController@showImportForm')->name('users.import');
    Route::post('users/import', 'Admin\UserController@import')->name('users.import.excel');
   // Route::get('users/delete/{id}', 'Admin\UserController@drop')->where('id', '[0-9]+');
   // Route::post('users/delete/{id}', 'Admin\UserController@remove')->where('id', '[0-9]+');

    /**
     * Services
     */
    Route::get('services', 'Admin\ServiceController@index');
    Route::get('services/{id}', 'Admin\ServiceController@show')->where('id', '[0-9]+');
    Route::get('services/edit/{id}', 'Admin\ServiceController@edit')->where('id', '[0-9]+');
    Route::post('services/edit/{id}', 'Admin\ServiceController@update')->where('id', '[0-9]+');
    Route::get('services/new', 'Admin\ServiceController@create');
    Route::post('services/new', 'Admin\ServiceController@store');
//    Route::get('services/delete/{id}', 'Admin\ServiceController@drop')->where('id', '[0-9]+');
//    Route::post('services/delete/{id}', 'Admin\ServiceController@remove')->where('id', '[0-9]+');

    /**
     * Services Areas
     */
    Route::get('areas', 'Admin\AreaController@index');
    Route::get('areas/{id}', 'Admin\AreaController@show')->where('id', '[0-9]+');
    Route::get('areas/edit/{id}', 'Admin\AreaController@edit')->where('id', '[0-9]+');
    Route::post('areas/edit/{id}', 'Admin\AreaController@update')->where('id', '[0-9]+');
    Route::get('areas/new', 'Admin\AreaController@create');
    Route::post('areas/new', 'Admin\AreaController@store');
    Route::get('areas/delete/{id}', 'Admin\AreaController@drop')->where('id', '[0-9]+');
    Route::post('areas/delete/{id}', 'Admin\AreaController@destroy')->where('id', '[0-9]+');

    /**
     * Promo Codes
     */
    Route::get('promo-codes', 'Admin\PromoCodeController@index');
    Route::get('promo-codes/{id}', 'Admin\PromoCodeController@show')->where('id', '[0-9]+');
    Route::get('promo-codes/edit/{id}', 'Admin\PromoCodeController@edit')->where('id', '[0-9]+');
    Route::post('promo-codes/edit/{id}', 'Admin\PromoCodeController@update')->where('id', '[0-9]+');
    Route::get('promo-codes/new', 'Admin\PromoCodeController@create');
    Route::post('promo-codes/new', 'Admin\PromoCodeController@store');
    Route::get('promo-codes/delete/{id}', 'Admin\PromoCodeController@drop')->where('id', '[0-9]+');
    Route::post('promo-codes/delete/{id}', 'Admin\PromoCodeController@destroy')->where('id', '[0-9]+');

    /**
     * Orders
     */
    Route::get('orders', 'Admin\OrderController@index');
    Route::get('orders/{id}', 'Admin\OrderController@show')->where('id', '[0-9]+');
    Route::get('orders/edit/{id}', 'Admin\OrderController@edit')->where('id', '[0-9]+');
    Route::post('orders/edit/{id}', 'Admin\OrderController@update')->where('id', '[0-9]+');
    Route::post('orders/add-payment', 'Admin\OrderController@addPayment');

    /**
     * Publicists
     */
    Route::get('publicists/users', 'Admin\BusinessController@index');
    Route::get('publicists/{id}', 'Admin\BusinessController@show')->where('id', '[0-9]+');
    Route::get('publicists/places/{id}', 'Admin\BusinessController@businessDetail')->where('id', '[0-9]+');
    Route::post('publicists/places/{id}', 'Admin\BusinessController@update')->where('id', '[0-9]+');

    /**
     * businesses
     */
    Route::get('businesses', 'Admin\BusinessController@getList');
    Route::get('businesses/categories', 'Admin\BusinessController@categoriesList');
    Route::get('businesses/categories/new', 'Admin\BusinessController@categoriesNew');
    Route::post('businesses/categories/new', 'Admin\BusinessController@categoriesCreate');
    Route::get('businesses/categories/{id}/edit', 'Admin\BusinessController@categoriesEdit');
    Route::post('businesses/categories/{id}/update', 'Admin\BusinessController@categoriesUpdate');
});

/**
 * Publicists
 */

Route::prefix('anunciantes')->group(function () {
   Route::get('/', 'Publicist\HomeController@index')->name('publicist');

    /**
     * Login
     */
    Route::get('login', 'Publicist\LoginController@showLoginForm');
    Route::post('login', 'Publicist\LoginController@login');
    Route::post('logout', 'Publicist\LoginController@logout');
    Route::get('registro', 'Publicist\RegisterController@showRegistrationForm');
    Route::post('registro', 'Publicist\RegisterController@register');
    Route::get('confirmar', 'Publicist\RegisterController@confirm');
    Route::post('confirmar', 'Publicist\RegisterController@setPublicist');
    Route::get('inscribir', 'Publicist\RegisterController@inscription');

    /**
     * Business
     */

    Route::get('establecimientos', 'Publicist\BusinessController@index');
    Route::get('establecimientos/registro', 'Publicist\BusinessController@create');
    Route::post('establecimientos/registro', 'Publicist\BusinessController@store');
    Route::get('establecimientos/editar/{id}', 'Publicist\BusinessController@edit')->where('id', '[0-9]+');
    Route::post('establecimientos/editar/{id}', 'Publicist\BusinessController@update')->where('id', '[0-9]+');

    /**
     * Near to me
     */

    Route::get('cerca-mio', 'Publicist\NearToMeController@index');
    Route::get('cerca-mio/nuevo/{id}', 'Publicist\NearToMeController@add');
    Route::post('cerca-mio/nuevo/{id}', 'Publicist\NearToMeController@create');

    Route::get('cerca-mio/editar/{id}', 'Publicist\NearToMeController@edit');
    Route::post('cerca-mio/editar/{id}', 'Publicist\NearToMeController@update');

    Route::get('cerca-mio/eliminar/{id}', 'Publicist\NearToMeController@delete');

    Route::get('cerca-mio/agregar', 'Publicist\NearToMeController@assign');
    Route::post('cerca-mio/agregar', 'Publicist\NearToMeController@assignSave');

    Route::get('cerca-mio/activos/{id}', 'Publicist\NearToMeController@actives');
    Route::post('cerca-mio/activos/{id}', 'Publicist\NearToMeController@activesSave');
    /*
     * Beneficios
     */
    Route::get('beneficios', 'Publicist\BenefitsController@index');

    Route::get('beneficios/agregar', 'Publicist\BenefitsController@assign');
    Route::post('beneficios/agregar', 'Publicist\BenefitsController@assignSave');

    Route::get('beneficios/nuevo/{id}', 'Publicist\BenefitsController@add');
    Route::post('beneficios/nuevo/{id}', 'Publicist\BenefitsController@create');

    Route::get('beneficios/editar/{id}', 'Publicist\BenefitsController@edit');
    Route::post('beneficios/editar/{id}', 'Publicist\BenefitsController@update');

    Route::get('beneficios/eliminar/{id}', 'Publicist\BenefitsController@delete');

    Route::get('beneficios/activos/{id}', 'Publicist\BenefitsController@actives');
    Route::post('beneficios/activos/{id}', 'Publicist\BenefitsController@activesSave');
});
