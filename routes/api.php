<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::post('oauth/token', 'API\AccessTokenController@issueToken');
Route::post('user/change-password/{email}', 'API\UserController@changePassword');

/**
 * Services
 */
Route::get('services/for-user/{user}', 'API\ServiceController@servicesByUserWithoutToken');
Route::get('services/by-platform', 'API\ServiceController@servicesGroupByPlatforms');

Route::group(['middleware' => 'auth:api'], function(){
    /**
     * Users
     */
    Route::get('user', 'API\UserController@details');
    Route::get('users', 'API\UserController@lists');
    Route::put('user', 'API\UserController@edit');
    Route::put('user/{email}', 'API\UserController@editUser');
    Route::delete('user', 'API\UserController@remove');
    Route::get('user/{email}', 'API\UserController@getUser');
    Route::post('user', 'API\UserController@create');

    /**
     * Services
     */
    Route::get('services', 'API\ServiceController@index');
    Route::get('service/{id}', 'API\ServiceController@details');
    Route::post('service', 'API\ServiceController@create');
    Route::put('service/{id}', 'API\ServiceController@edit');
    Route::get('services/{platform}', 'API\ServiceController@servicesByPlatform')->where('platform', '[0-9]+');
    Route::delete('service/{id}', 'API\ServiceController@remove');
    Route::get('services/user/{user}', 'API\ServiceController@servicesByUser');
    Route::get('services/{user}/platform/{platform}','API\ServiceController@servicesUserByPlatform');

    /**
     * Platforms
     */
    Route::get('platforms/{platform}/services', 'API\ServiceController@servicesByPlatformName');

    Route::post('nearMe', 'API\NearToMeController@getNearMe');
    Route::get('business/categories', 'API\BusinessController@getCategories');
    Route::post('benefits', 'API\BenefitsController@getAll');
    Route::get('exchang/coupon/{id}', 'API\BenefitsController@exchangeCoupon');
});



/**
 * Webhooks
 */
Route::post('webhook/mercadopago', 'MercadoPagoWebhookController@index');
