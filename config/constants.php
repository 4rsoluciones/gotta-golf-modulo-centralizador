<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Gotta GpStats URL
    |--------------------------------------------------------------------------
    |
    | This option controls the url to point to GpStats application
    | on integrations
    |
    */

    'gpStatsUrl' => 'https://www.andresgotta.com.ar',

    /*
    |--------------------------------------------------------------------------
    | Gotta Sport in Touch URL
    |--------------------------------------------------------------------------
    |
    | This option controls the url to point to Sport in Touch application
    | on integrations
    |
    */

    'sportInTouchUrl' => 'https://www.andresgotta.com.ar/academia',

    /*
    |--------------------------------------------------------------------------
    | System Roles
    |--------------------------------------------------------------------------
    |
    | This option store all roles accepted by the system
    |
    */

    'roles' => [
        'ROLE_SUPER_ADMIN' => 'Super Administrador',
        'ROLE_GROUP_OWNER' => 'Dueño de Grupo',
        'ROLE_GROUP_ADMIN' => 'Administrador de Grupo',
        'ROLE_SECRETARY' => 'Secretaria',
        'ROLE_CUSTOMER' => 'Cliente',
        'ROLE_STUDENT' => 'Estudiante',
        'ROLE_ACADEMY' => 'Academia',
        'ROLE_TEACHER' => 'Profesor',
        'ROLE_PLAYER' => 'Jugador',
        'ROLE_USER'   => 'usuario',
        'ROLE_PUBLICIST' => 'Anunciante',
    ],

    /*
    |--------------------------------------------------------------------------
    | Gotta GpStats Roles
    |--------------------------------------------------------------------------
    |
    | This option store roles accepted by GpStats platform
    |
    */

    'gpStatsRoles' => [
        'ROLE_SUPER_ADMIN',
        'ROLE_GROUP_OWNER',
        'ROLE_GROUP_ADMIN',
        'ROLE_SECRETARY',
        'ROLE_CUSTOMER',
        'ROLE_PLAYER',
    ],

    /*
    |--------------------------------------------------------------------------
    | Gotta Sport in Touch Roles
    |--------------------------------------------------------------------------
    |
    | This option store roles accepted by Sport in Touch Platform
    |
    */

    'sportInTouchRoles' => [
        'ROLE_ACADEMY',
        'ROLE_STUDENT',
        'ROLE_TEACHER',
    ],

    /*
    |--------------------------------------------------------------------------
    | Gotta Publicist Roles
    |--------------------------------------------------------------------------
    |
    | This option store roles accepted by Publicist Platform
    |
    */

    'publicistRoles' => [
        'ROLE_PUBLICIST',
    ],


    /*
    |--------------------------------------------------------------------------
    | Gotta Sport in Touch Roles Names
    |--------------------------------------------------------------------------
    |
    | This option store roles accepted by Sport in Touch Platform
    |
    */

    'sportInTouchRolesNames' => [
        'ACADEMY' => 'ROLE_ACADEMY',
        'STUDENT' => 'ROLE_STUDENT',
        'TEACHER' => 'ROLE_TEACHER',
    ],

    /*
    |--------------------------------------------------------------------------
    | Gotta debit dues
    |--------------------------------------------------------------------------
    |
    | This option store a option of debit dues accepted for an service
    |
    */

    'debitDues' => [
        '1' => 1,
        '3' => 3,
        '6' => 6,
        '12' => 12,
        '18' => 18,
        '36' => 36,
    ],

    /*
    |--------------------------------------------------------------------------
    | Gotta payment status
    |--------------------------------------------------------------------------
    |
    | This option store a payment statuses name
    |
    */

    'payments' => [
        'PAYMENT_APPROVED' => 'Pagado',
        'PAYMENT_PENDING' => 'Pendiente',
        'PAYMENT_DECLIDED' => 'Rechazado',
        'PAYMENT_REFUND' => 'Devuelto',
    ],

    /*
    |--------------------------------------------------------------------------
    | Gotta Platform names
    |--------------------------------------------------------------------------
    |
    | This option store each platform names
    |
    */

    'platformNames' => [
        'GPSTATS_NAME' => 'GP Stats',
        'SPORT_IN_TOUCH_NAME' => 'Sport in Touch',
        'PUBLICIST_NAME' => 'Anunciantes',
    ],

    /*
    |--------------------------------------------------------------------------
    | Gotta Platform names
    |--------------------------------------------------------------------------
    |
    | Internal name
    |
    */

    'platformInternalNames' => [
        'gpstats' => 'GP Stats',
        'sportsintouch' => 'Sports in Touch',
        'anunciantes' => 'Anunciantes',
    ],

    /*
    |--------------------------------------------------------------------------
    | Publicists business status
    |--------------------------------------------------------------------------
    |
    | This option store each business inscription status names
    |
    */

    'businessStatus' => [
        'EVALUATION' => 'En Evaluación',
        'APPROVED' => 'Aprobada',
        'DECLINED' => 'Rechazada',
        'CANCELLED' => 'Cancelada',
    ],

    /*
    |--------------------------------------------------------------------------
    | Publicists Service Area
    |--------------------------------------------------------------------------
    |
    | The publicists service area name
    |
    */

    'PUBLICISTS_SERVICE_AREA' => 'Anunciantes',
];