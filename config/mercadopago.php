<?php

return [
    /**
     * Basic Checkout Credentials
     */

	'app_id'     => env('MP_APP_ID', ''),
	'app_secret' => env('MP_APP_SECRET', ''),

    /**
     * Custom Checkout Credentials
     */

    'public_key' => env('MP_PUBLIC_KEY'),
    'access_token' => env('MP_ACCESS_TOKEN'),

    /**
     * Payment Status
     */

    'MP_PAYMENT_APPROVED' => 'approved',
    'MP_PAYMENT_PENDING' => 'pending',

    /**
     * Subscription Status
     */

    'MP_SUBSCRIPTION_AUTHORIZED' => 'authorized',
    'MP_SUBSCRIPTION_PAUSED' => 'paused',
    'MP_SUBSCRIPTION_FINISHED' => 'finished',
    'MP_SUBSCRIPTION_CANCELED' => 'cancelled',

    /**
     * Payment Types
     */

    'MP_PAYMENT_CREDIT_CARD' => 'Tarjeta de Crédito',
    'MP_PAYMENT_DEBIT_CARD' => 'Tarjeta de Débito',
    'MP_PAYMENT_TICKET' => 'Efectivo',
    'MP_PAYMENT_ATM' => 'Depósito Bancario',
];